import * as fs from 'fs';

class Utils {
  static supprAccent(s:string):string {
    let accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    let noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
    for(var i = 0; i < accent.length; i++){
        s = s.replace(accent[i], noaccent[i]);
    }
    return s;
  }
}

class Replacer {
  private isPrevLetterSpace:boolean;
  private isPrevLetterDot:boolean;
  private currLetter:string;
  private nbReturned:number;
  private space = [" ","\n",",","'","’","»","«","”","\"","-",")","(","[","]","–","°","1","2","3","4","5","6","7","8","9","0"];
  private dot = [".","!","?",":",";"];
  private known = ["A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","T","V","W","X","Y","Z"," ","."]

  constructor(){
    this.isPrevLetterSpace=true;
    this.isPrevLetterDot=true;
    this.nbReturned = 0;
  }

  private isSpace(letter:string):boolean {
    return this.space.indexOf(letter) != -1
  }

  private isDot(letter:string):boolean {
    return this.dot.indexOf(letter) != -1
  }

  private isKnown(letter:string):boolean {
    return this.known.indexOf(letter) != -1
  }

  private replace(letter:string) {
    if (this.isSpace(letter)) return " ";
    if (this.isDot(letter)) return ".";
    letter = Utils.supprAccent(letter).toUpperCase();
    if (letter == "U") return "V";
    if (letter == "Œ") return "O";
    if (! this.isKnown(letter)) console.log(letter);
    return letter;
  }

  public add(prevLetter:string, currLetter:string, nextLetter:string):string {
    if (nextLetter.length != 1 || currLetter.length != 1 || nextLetter.length != 1) throw new Error("Length of letters must be one caracter");
    this.currLetter = this.replace(currLetter);
    if (this.isSpace(this.currLetter) && this.isSpace(prevLetter)) this.currLetter = "";
    if (this.isSpace(this.currLetter) && this.isDot(nextLetter)) this.currLetter = "";
    if (this.isSpace(this.currLetter) && this.isNewLine()) this.currLetter = ""
    if (this.isDot(this.currLetter) && this.isDot(prevLetter)) this.currLetter = "";
    if (this.currLetter != "") this.nbReturned++;
    //console.log("#" + this.currLetter + ":" + this.nbReturned + ":" + this.isNewLine());
    return this.currLetter;
  }

  public isNewLine() {
    return ((this.nbReturned > 1) && (this.nbReturned ) % 27 == 0);
  }

  public isNewPage() {
    return (this.nbReturned % (27*27) == 0);
  }

}


class Parser {
  private inputContent:string;
  private outputContent:string;
  private outputDir:string;
  private outputFile:string;
  private outputFileNumber:number;
  private replacer:Replacer;

  constructor(inputPath:string, outputDir:string) {
    this.inputContent = fs.readFileSync(inputPath,'utf8');
    this.outputContent = "";
    this.outputFileNumber = 1;
    this.outputDir = outputDir;
    this.setOutputFile();
    this.replacer = new Replacer();
  }

  private setOutputFile() {
    this.outputFile = this.outputDir + "/" + this.outputFileNumber + ".txt";
  }

  private savePage() {
    this.outputContent += "\n\nTitre : CRIME ET CHATIMENT LIBRE VN\n"
    this.outputContent += "Autre : FRIEDRICH WILHELM NIETZSCHE\n"
    this.outputContent += "Cité : ROMAN KACEW\n"
    this.outputContent += "Salle : 6657\n"
    this.outputContent += "Étagère : 9\n"
    this.outputContent += "Niveau : 27\n"
    this.outputContent += "Libre : 27 \n"
    this.outputContent += "Page : 1\n"
    fs.writeFileSync(this.outputFile, this.outputContent);
    this.outputContent = "";
    this.outputFileNumber++;
    this.setOutputFile();
  }

  print() {
    let i:number=0;
    let currLetter:string;
    let prevLetter:string;
    let nextLetter:string;
    while (currLetter = this.inputContent[i]) {
      if (i == 0) prevLetter = " "; else prevLetter = this.inputContent[i-1];
      if (i == this.inputContent.length-1) nextLetter = " "; else nextLetter = this.inputContent[i+1];
      let letter:string = this.replacer.add(prevLetter, currLetter, nextLetter)
      this.outputContent += letter;
      if (letter != "") {
        if (this.replacer.isNewPage()) {
          this.savePage();
        }
        else if (this.replacer.isNewLine()) {
          this.outputContent += "\n";
        }
      }
      i++;
    }
    fs.writeFileSync(this.outputFile, this.outputContent);
  }
}

let p:Parser = new Parser("in/c_et_m_chap1.txt", "out");
p.print();

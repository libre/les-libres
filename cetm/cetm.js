"use strict";
exports.__esModule = true;
var fs = require("fs");
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.supprAccent = function (s) {
        var accent = [
            /[\300-\306]/g, /[\340-\346]/g,
            /[\310-\313]/g, /[\350-\353]/g,
            /[\314-\317]/g, /[\354-\357]/g,
            /[\322-\330]/g, /[\362-\370]/g,
            /[\331-\334]/g, /[\371-\374]/g,
            /[\321]/g, /[\361]/g,
            /[\307]/g, /[\347]/g,
        ];
        var noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
        for (var i = 0; i < accent.length; i++) {
            s = s.replace(accent[i], noaccent[i]);
        }
        return s;
    };
    return Utils;
}());
var Replacer = /** @class */ (function () {
    function Replacer() {
        this.space = [" ", "\n", ",", "'", "’", "»", "«", "”", "\"", "-", ")", "(", "[", "]", "–", "°", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
        this.dot = [".", "!", "?", ":", ";"];
        this.known = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "N", "M", "O", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z", " ", "."];
        this.isPrevLetterSpace = true;
        this.isPrevLetterDot = true;
        this.nbReturned = 0;
    }
    Replacer.prototype.isSpace = function (letter) {
        return this.space.indexOf(letter) != -1;
    };
    Replacer.prototype.isDot = function (letter) {
        return this.dot.indexOf(letter) != -1;
    };
    Replacer.prototype.isKnown = function (letter) {
        return this.known.indexOf(letter) != -1;
    };
    Replacer.prototype.replace = function (letter) {
        if (this.isSpace(letter))
            return " ";
        if (this.isDot(letter))
            return ".";
        letter = Utils.supprAccent(letter).toUpperCase();
        if (letter == "U")
            return "V";
        if (letter == "Œ")
            return "O";
        if (!this.isKnown(letter))
            console.log(letter);
        return letter;
    };
    Replacer.prototype.add = function (prevLetter, currLetter, nextLetter) {
        if (nextLetter.length != 1 || currLetter.length != 1 || nextLetter.length != 1)
            throw new Error("Length of letters must be one caracter");
        this.currLetter = this.replace(currLetter);
        if (this.isSpace(this.currLetter) && this.isSpace(prevLetter))
            this.currLetter = "";
        if (this.isSpace(this.currLetter) && this.isDot(nextLetter))
            this.currLetter = "";
        if (this.isSpace(this.currLetter) && this.isNewLine())
            this.currLetter = "";
        if (this.isDot(this.currLetter) && this.isDot(prevLetter))
            this.currLetter = "";
        if (this.currLetter != "")
            this.nbReturned++;
        //console.log("#" + this.currLetter + ":" + this.nbReturned + ":" + this.isNewLine());
        return this.currLetter;
    };
    Replacer.prototype.isNewLine = function () {
        return ((this.nbReturned > 1) && (this.nbReturned) % 27 == 0);
    };
    Replacer.prototype.isNewPage = function () {
        return (this.nbReturned % (27 * 27) == 0);
    };
    return Replacer;
}());
var Parser = /** @class */ (function () {
    function Parser(inputPath, outputDir) {
        this.inputContent = fs.readFileSync(inputPath, 'utf8');
        this.outputContent = "";
        this.outputFileNumber = 1;
        this.outputDir = outputDir;
        this.setOutputFile();
        this.replacer = new Replacer();
    }
    Parser.prototype.setOutputFile = function () {
        this.outputFile = this.outputDir + "/" + this.outputFileNumber + ".txt";
    };
    Parser.prototype.savePage = function () {
        this.outputContent += "\n\nTitre : CRIME ET CHATIMENT LIBRE VN\n";
        this.outputContent += "Autre : FRIEDRICH WILHELM NIETZSCHE\n";
        this.outputContent += "Cité : ROMAN KACEW\n";
        this.outputContent += "Salle : 6657\n";
        this.outputContent += "Étagère : 9\n";
        this.outputContent += "Niveau : 27\n";
        this.outputContent += "Libre : 27 \n";
        this.outputContent += "Page : " + this.outputFileNumber + "\n";
        fs.writeFileSync(this.outputFile, this.outputContent);
        this.outputContent = "";
        this.outputFileNumber++;
        this.setOutputFile();
    };
    Parser.prototype.print = function () {
        var i = 0;
        var currLetter;
        var prevLetter;
        var nextLetter;
        while (currLetter = this.inputContent[i]) {
            if (i == 0)
                prevLetter = " ";
            else
                prevLetter = this.inputContent[i - 1];
            if (i == this.inputContent.length - 1)
                nextLetter = " ";
            else
                nextLetter = this.inputContent[i + 1];
            var letter = this.replacer.add(prevLetter, currLetter, nextLetter);
            this.outputContent += letter;
            if (letter != "") {
                if (this.replacer.isNewPage()) {
                    this.savePage();
                }
                else if (this.replacer.isNewLine()) {
                    this.outputContent += "\n";
                }
            }
            i++;
        }
        fs.writeFileSync(this.outputFile, this.outputContent);
    };
    return Parser;
}());
var p = new Parser("in/c_et_m_chap1.txt", "out");
p.print();

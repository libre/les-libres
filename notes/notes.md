
# Histoire
- XXIe : siècle des machines et de la consommation
- XXIIe : siècle de la nature : après s'être focalisé pendant 100 ans sur la dépendance aux machines, il est advenu le temps de s'occuper de la terre. Les hommes en avait finit de cette lutte (ils l'avaient perdue, mais ne le savait pas encore). Les EUTAN ont annexé l'Amérique du Sud, l'Europe l'Afrique, la Russie et la Chine le reste du monde, et ont décrété que ces zones seraient des poumons, que les hommes n'y mettrait plus les pieds que pour planter des arbres. Ce sont en fait les machines qui ont décidé les hommes à ce changement (on l'apprendra dans la troisième partie).
En fait c'est plus la lassitude la consommation (des objets numériques) et du rapport aux machines qui avait créé le changement que le bouleversement écologique.

- 2080 Naissance de Richard
- 2099 Basculement (avancés vie longue, manifestation pour la préservation de la planète, change or perish...)
- 2100 Pacte mondiale de la nouvelle ère (optimisation des négociations par les IA, accord de dénatalité massif, abolition des nations, automatisation des états déjà en place depuis 2 ou 3 ans sans difficultés dans quelques pays, arrêt du développement), année 0 (ou date UNIX ou Fourrier)
- 2812 Début de la troisième partie de Creativity
- 3007 Fin de la troisième partie de Creativity
      Pourtant Richard n'était pas un gagneur. Dans le monde d'avant, il était même plutôt apathique.

# Dimensionnement
On est en 3000 environ
La partie a commencé en 2800
200 ans, 8 générations
2000 habitants, 1000 mineurs (50% de la population environ)
1 salle contient 6651 libres
1 mineur clôt 3 libres par jour
1 mineur clôt 1000 libres par an
1 mineure clôt en moyenne 1 salle par vie (tous les mineurs ne font pas ça toute leur vie)
8000 salles explorées en fin de partie
Carole a 18 ans, elle a commencé il y à 5 ans, elle traite à peine deux libres par jour, elle a clôt environ 3000 libres, elle en est à la moitié de sa salle : Étagère 4/9, niveau 7/27, libre 12/27

# Vocabulaire
cité
monde
âme artificielle

# La communauté
Mairie de Mareuil = Conseil, y entre et gère la cité qui veut. Bob, il pourrait bien aller au conseil, mais ça aussi ça le gonfle.
Monolithe à la place de l'église (panoptique) ; IAM
Jardin des libertaires sur le plateau

nom de la cité hérité de l'autre du libre emblème de la cité ROMAN

Pas d’animaux. Reste quelques insectes. Végétariens. Agriculteurs. Pas d’armes. Pas de violence.
La plupart des objets manufacturés viennent de l'IAM. Quelques créations artisanales mineures.

# Les enfants
Comment se reproduisent les clones. Introduire le thème de l'enfant dans la partie 1, ou au contraire évoquer un autre mode de reproduction. Demander un enfant à la machine, par exemple.

# IAM
IAM
I am
I âme

# Personnages

Description Carole, peau marron

Il n'existe que 3 postures pour chaque être humain dans son rapport au monde, avec pour chacune 2 niveaux, passif et actif.

C'est une théorie de la fin du XXIè siècle, qui sera pré-redécouverte par le Philosophe et ensuite exposée par Richard, car à la base de sa stratégie pour jouer.

Physique des moeurs

Ce sera aussi étudié par les machines, car elle ne parviennent pas à créer (stade Créatif actif).

Les 6 premiers personnages des 6 premiers chapitres sont chacun l'incarnation d'une figure.

I. Être au monde (état naturel) (cf Henri Laborit)
- Naturel Passif : Asthénie : Subir le monde, s'en rendre malade (Bob)
- Naturel Actif : Violence : Subir le monde, retourner sa douleur contre autrui (violence) (Alexander)
- Ces états sont stables

II . Accepter le monde (état tragique, cf stoïcisme et épicurisme, puis Amor Fati de Nietzsche)
- Tragique Passif : Divertissement (Pascal) : refus du monde, détournement, religion, arrière-monde (Philosophe : renommer en Joueurs ?) (Revel)
- Tragique Actif : Ataraxie : Accepter le monde tel qu'il l'est, impassibilité, ataraxie, (voire bouddhisme), Ce n'est pas mauvais de mal vivre (Ada)
- Ces états sont instables, le monde se rappelle régulièrement aux tragiques

III . Changer le monde (état créatif)
- Créatif Passif : Harmonie : Aimer ce qui nous arrive (Amor Fati), il faut faire l'amour à la vie (Carole)
- Créatif Actif : Liberté : Changer le monde, principe de la créativité, faire le monde à son image, change ce que tu peux (Gabriel)

- C'est état sont hautement instable, ils demandent une grande énergie en permanence.

Pour être libre, il faut aimer et créer son monde.

## Tout ce qu'on ne comprend pas dans C&M
l’argent
tuer
étudiant
misère difficile à comprendre dans un monde ou tout est à disposait
St Pétersbourg : sorte de cité, mais qui parait beaucoup plus grande, aucune cité n'est aussi grande, il me semble

## TODO

- Carole : Affirmer plus fortement son "amor fati" dès le début (chap1) ; refaire le paragraphe (chap4) où elle prends conscience du changement (passif vers actif) :  il ne te suffit plus d'aimer le jeu, tu envisage à présente d'en réécrire les règles ? Chapitre 4 : évoquer les douleurs physique, régression en naturel passif.

Carole n'était pas très belle. ses cheveux n'était pas bien soignés, son corps n'était pas laid, mais dépourvu de grâce, elle paraissait un peu trop forte, mais sans en avoir le charme, son visage un peu trop rond, son nez légèrement épaté, ses yeux un peu trop rapprochés. Seule elle dégageait quelque chose tout de même, on l'aurait peut-être trouvé jolie, intéressante pour le moins, intrigante, mais à côté d'Ada, elle disparaissait totalement.

- Ada : aménager en partie le je m'en fout, en ça ne me touche pas, je peux y résister, je n'ai pas besoin de cela

- Bob : insister sur les douleurs, le corps de Bob qui va mal

- Inventeur : Refaire le chapitre en le rendant agressif (enlever la boisson, on laisse ça à Bob). Explique qu'il ennuie les inventeurs, changer l'anecdote où il est mis en défaut, par un autre, où lui met en défaut, un mineur ? un autre inventeur ? Laisse en tous cas apparaître sa frustration, elle est constitutive de sa violence. À la fin du chapitre : mélanger les livres et tout refait ; déchirer les pages des libres et les mélanger, il resterait encore tout, n'est ce pas ? déchirer chaque page en petit morceau, qu'est ce que ça changerait, tout y serait, n'est ce pas ? Une idée insensée effrayante. Une provocation ? Il sait, pense Carole...

- Renommer Philosophes en Joueurs, ils se retirent du monde et joue à inventer des idées

- Pouvoir tout calculer, c'est cela détruire le monde, c'est l'incertitude, le danger, la finitude, le hasard qui créent les conditions de possibilité de la créativité. Le calcul, l'automatisation, la routine, la mécanisation, détruise le monde, le rigidifie, le fixe, le fige.

# Vocabulaire
- libre-ouvert : libre dans la bibliothèque qui n'a pas encore été trouvé et lu
- libre-enclos : libre qui a été totalement lu par un mineur et qui contient au moins un mot
- clôturer un libre (par un mineur) -> terminer la lecture et classer le libre en libre-libre (marqué par une pierre noir) ou libre-enclos (marqué par une pierre blanche)
- écrit-vain désigne les non mots

- libre lettre ? #TODO

```
- libre-libre : libre qui ne contient aucun mot, il ne contient que des écrits-vains
- livre-mâle, ne contient qu'un seul mot
- libre-d'âme, contient une ou plusieurs locution, qui ne forment pas une phrase
- libre-île, contient une seule phrase
- libre-ailes, contient au moins deux phrases isolées
- libre-mère, contient au moins un paragraphe, c-a-d deux phrases qui s'enchaînent.
- libre-plage, contient au moins une pleine page de phrases
- libre-plaint, ne contient que des mots (aucun écrit-vain), mais qui ne s'enchaînent jamais
- libre-ivre, ne contient que des phrases qui peuvent former un sens.
```

Les livres ne bougent pas, les inventeur se souviennent de leurs positions, les libérateur les apprennent et décident des mots.
Les éleveurs enseigne la lecture.

- Intégriste : pensent qu'il n'existent pas deux libres avec le même titre et le même autre (?)
- Finitistes : pensent qu'il n'existent pas deux libres identiques
- Infinitistes : pense qu'il existe une infinité de copies de chaque libre

# Présentation

Extrait libre
Préambule
Extrait C&M1

# Plan
Partie I
  1. Préambule [jour 1]
  2. Ada, dilettante [jour 2 & 3]
  3. Robertlovis, épouvantail [Jour 4, 5?, 6?]
  4. Carole
  5. Alexanderpouchkine
  6. Revel
  7. Celine
Enlumineur
Libertaire - Libre-penseur

Partie II
  1. Préambule. Richard mise, Carole page blanche
  2. Richard., Retour sur l'histoire qui a conduit à ce monde, Carole écrit le titre et l'autre
  3. William explique les clones de Richard, Carole écrit au hasard. ABC discutent du libre. Bob et AP.
  4. Richard. Ada. Petit cheval. Carole écrit une première page. Ada copie à l'encre
  5a. William. Meurtre jour 31. All in.
  5b. Richard. Meurtre jour 31. All in. Carole reprend écriture
  6. Wasiam. réaction de la cité, chantage, suspicion, fuite. Carole, s'exile dans le désert, belle page, va visiter d'autres cités, finit son libre
  7. IAM. Victoire de Richard, Richard rencontre Carole. Tu vas supprimer mon monde. Décision des IA. On apprend que Richard est un des derniers humains. On le garde pour une autre partie, pour comprendre.

Pas d'IA que des Interfaces (IAM)
Elles jouent, ne cherchent rien en particulier, elles se contentent de faire ce qu'elles avaient à faire quand l'âme qu'elle interfaçait à disparue. les Interfaces sans corps sont vides
À la fin on découvre beaucoup d'âme on simplement arrêté de vivre, comme Richard va le faire en cherchant à profiter es 3 années qu'il lui reste avec Carole. Carole refusera l'option de vivre comme Richard car elle comprends que ce monde n'est pas intéressant.

La poïétique (du grec ancien ποίησις, « oeuvre, création, fabrication ») a pour objet l'étude des potentialités inscrites dans une situation donnée qui débouche sur une création nouvelle. Chez Platon la poïèsis se définit comme « La cause qui, quelle que soit la chose considérée, fait passer celle-ci du non-être à l'être » (Le Banquet, 205 b).
Poète : font-être

William est une interface qui a perdu son corps. Elle ne sait que faire. Elle étudie des combinatoires. Mais ça ne la même nulle part.

Ce qui caractérise l'humain c'est cette volonté de ne pas vivre directement au monde, de se couper du monde, de construire des interfaces qui change sa représentation du monde. L'âme n'est pas le dans monde, elle s'entoure pour être à côté du monde.

# Personnages
- Alexanderpouchkine : parler précieux, style russe

# Noms (réserve)
Marcus Alius Aurelius Verus
RobertlovisBalfovrStevenson
Adavgvasta lovelace kingbyr
LeontolstoilevNikolaeïvitch
Carole lewisLvtwidgeDodgson
Celine Ferdinand Destouches
JEANBAPTISTE POCLAINMOLIERE
Nicolas Vassiliévitch Gogol
Michel AleksandrovitchBakou
IvanSer	gïevitchTourgveniev
Gabriel gabo García Márquez
Revel JRRRonaldJohn Tolkien
Philip Kindred Dick
Roman ajar emile Kacew gary
Gabriel garcia marquez
Céline Destouches Ferdinand

# titres

la vie devant soi



Rapport à la technique : besoin de symboles (diabole) propre de l'intelligence
Plaisir technique doit être modéré ; déjà donné
danger, le nouveau, aplatir la courbe
satasifaction des plaisirs, tendance à la boulimie, regul ataraxie

contrer la boulimie, lenteur, ataraxie

si je le laisse vivre ce monde, il risque de devenir comme le mien, un jour, la boulimie risque de le reprendre, il va tout détruire à nouveau, et peut etre se déttruire, ou tout détruire...

-- mais ce n'est pas certain ? il y a une chance ?

-- je ne sais pas si c'est certain... donc, non, ce n'est pas certain.

-- c'est différent, les machines, elles sont là, on n'aura peut être pas autant envie d'elles ? Quand on a déjà quelque chose, on ne le désire plus. On désirera autre chose, peut être que ce sera mieux.




code sources publics : ne pas tricher
trop vite : personne ne mise, partie sans intérêt
trop expliqite, on le voit dans le code, idem
créer de l'imprévisible puis chercher à la prévoir

brider la volonté de puissance, la laisser réélerger
concomitance néessaire de créativité, art, et mal
l'exploration n'a pas de morale
Carole n'est pas moralemet atteinte car il n' a pas la chape morale de Rodion, revoir cette aspect


*

­— Est-ce que tu m'en veux Carole ?

-- Je ne crois pas. De quoi t'en voudrais-je ?

-- De t'avoir créée. Je veux dire, de l'avoir fait te que je l'ai fait, pas jeu. D'avoir déterminé ta vie...

-- Comment sais-tu qu'elle l'est moins ta vie à toi, déterminée ? C'est plutôt chouette d'avoir été créée par jeu, c'est gai. Quest ce que ça changerait, si je t'en voulais ? Je serai plus lourde de cette pensée. Je serais plus malheureuse. Tu serais pllus malheureux aussi, n'est ce pas ? Je n'ai pas le pouvoir d'avoir été créée différement, mais j'ai ce pouvoir de nous rendre moins malheureux, tu vois. Alors, le je l'aime, moi, la façon dont tu m'a créée, ce monde que tu as fait. Je l'aime bien. Je n'ai que celui-là, je n'ai que cette vie, de toutes façons, n'est-ce pas ? Alors je les aime bien.

*

choisir entre colporteur et livreur

*

est née en X du calendrier du monde XX (3110 du calendrier chrétien)
Carole Lewis termina sa formation à l'âge précoce de 11 ans. À 2 ans elle manifestait déjà un intérêt vif pour les libres, à 5 ans elle savait parfaitement lire, à 8 ans elle connaissait plus de la moitié des mots. Il apparut évident à ses parents qu'elle serait une âme libre. Ses parents moururent tous les deux au début de son adolescence. Elle avait un tempérament calme, mais néanmoins joueur et assez sociable, aussi elle manifesta le désir d'embrasser la vocation d'enlumineuse. Ce ne fut néanmoins qu'une passade, sa mémoire n'était pas excellente et elle se lassa rapidement d'apprendre les mots. À 11 ans donc elle devint mineure et le resta jusqu'à ce qu'elle quitte la cité de X à l'âge de Y ans. Pas une seule journée elle ne manque de se rendre à la bibliothèque, dans la salle 8888.
Elle failli se lasser également de l'occupation de mineure, elle ne trouvait presque rien. Mais finalement elle décida qu'il était plus simple d'aimer simplement tourner les pages que de s'orienter à nouveau vers autre chose. Et depuis, elle avait apprécié chaque jour de sa vie.


Ada Agavsta... était une enfant à la peau clair doté de grand yeux vert. Elle était lumineuse. Elle souriait dès qu'elle croisait un visage et, surtout, elle avait le pouvoir de faire sourire toute  âme qui posait les yeux sur elle. La séduction de l'enfant devient à l'adolescence une source de désir à laquelle peu d'âmes ne pouvaient résister. Pour tout dire, Ada n'en croisa aucune qui eu pu lui résister, ou, en tous, inconsciemment, avait cette certitude. Adulte elle était probablement la plus belle âme de la cité, la plus joyeuse, la plus insouciante aussi. Les âmes étaient en général peu soucieuses, mais elle semblait encore plus indifférente à toute forme de soucis ou d'ennui. Jusqu'à l'âge de 15 ans. Sa rencontre avec Carole Lewis coïcida avec la naissance d'une maturité qui entamait son insouciance.

Robertlovis était né en décalage avec le monde. Il s'était essayé brièvement à la colère, mais il avait vite opté pour la tristesse. À 12 ans il a commencé à boire sporadiquement, puis régulièrement, pour ne plus perdre cette habitude qu'après s'être mis en trio avec Carole et Ada, à l'âge de 18 ans. Les ruptures apportées par Ada et Carole à sa vie eurent sur lui un effet bénéfique, il devient plus apaisé, souffrait moins de vivre, et il connut même quelques moment de réel bonheur dans les bras d'Ada et de Carole, à écouter Carole déclamer le CRIME ET CHATIMENT LIBRE VN, à consoler Ada quand Carole les eut quitté.

Alexanderpouchkine était un enfant agréable, légèrement renfermé, mais serviable et discret. En 237 il ...  


Richard X est né en 2300 du calendrier ... dans un village du nord de la France. Richard vit le plus clair de son temps dans son haver, aux xoordonées (ancien Sénégal).

William est une interface âme machine. Elle était l'IAM de William Portes, né le 16 aout 2300 à San Francisco, mort le 16 aout 3834 aux coordonnées X, Y (ancienne Mongolie) d'une overdose volontaire de ... William Portes était pationné du jeu de la création. Fidèle à la loi de rémasnce qui condit les IAM sans-âme à prolonger les volontés de ses âmes durant ne durée décroissante, eke est restée pationnée du jeu de la création, ele s'intéresse particulièrement à Richard Portes qui est en passe de gagner la partie actuelle.

Wasiam était l'interface âme machine d'Hélène Y, né le dans une ville de la banlieu de Londres, morte le de aux coordonnées (). Hélène X était proche de William Portes, ils se renconrait physiquement régulièremnt, deux ou trois fois pas an au moins et avaient même des relations sexuelles au cors de cetaines de ces rencontres. Selon la loi de rémanscece transitive, elle est resté roche de William Portes à lamort d'Hélène, puis de son IAM à la mort de william. Elle a franchit le seulil d'indépendance en 2983 et s'est renommée Wasiam en clin d'oeil à son ami William.

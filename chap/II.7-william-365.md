William
Jour 1095

-- Un terrible événement vient de frapper notre communauté.

Toutes les âmes de Roman Kacew était réunies sur la place la cité. Les libérateurs étaient sur l'estrade habituellement occupée par les enlumineurs. En rang les uns à côté des autres sur trois lignes. Le plus vieux d'entre eux avait pris la parole. Il avait les traits marqués. Il se tenait légèrement voûté. Ses mains tremblaient un peu. Il était dans sa dernière année.

-- Je ne peux m'expliquer cet acte, nous ne savons pas encore quoi faire... j'ai choisi d'être libérateur pour m'occuper des libres, pas des âmes. Nous ne connaissons pas de libre qui puisse nous aider.

Le libérateur marqua une pause pour laisse à son message le temps de se propager. Sa voix ne portait pas bien loin. Les inventeurs formaient un premier demi-cercle, ils écoutaient attentivement le libérateur, puis se tournaient vers les mineurs et le leur répétait. Ces derniers formaient un second demi-cercle. Les esclaves étaient relégués au troisième et dernier demi-cercle, plus anarchique et plus dense. Ils glanaient comme ils pouvaient le message et se le répétait les uns aux autres. Cette hiérarchie inhabituelle au sein de la cité s'était instaurée d'elle-même.

-- Je souhaite que le... ou les responsables de cet acte se signalent à nous, ici et maintenant... ce serait tellement plus simple.

Ce qui marquait le plus lorsqu'on entendait le libérateur, c'était son impuissance. Il suppliait plus qu'il n'ordonnait. Il ne connaissait que les libres, et aucun libre ne l'avait préparé à cela. Il pivota la tête, regarda ses pairs, il semblait demander, aidez-moi, pivota à nouveau regarda devant lui les âmes-libres et les esclaves, la plupart étaient comme lui désemparés. Il devait bien exister un libre qui leur enseignerait l'attitude à avoir, il faudrait consulter les colporteurs, envoyer des messages, miner derechef, leur seul espoir était de trouver le libre qui leur enseignerait quoi faire.

-- Quelqu'un a dérobé un libre dans la salle 6127. Il a également mutilé des libres-libres. Qu'il se dénonce !

Sa voix s'était voulue plus assurée pour cet ordre, mais elle restait fragile. Tandis que ce propageait le message, la foule conservait globalement sa docilité. Des murmures néanmoins commençait à se propager. La salle 6127. La salle de Carole Lewis. L'année dernière. L'assassinat. C'est forcément plus récent. Quelqu'un s'en serait rendu compte. Est-ce que la salle était close ? Qui s'en rendu dans cette salle ? La salle de Carole Lewis ? Je ne m'en approche jamais !

*

Vers midi le soleil s'était brusquement voilé et le sable avait commencé à déferler de tous côtés. Carole avait été surprise. Elle n'était pas encore assez habituée malgré presque une année à parcourir le désert. Elle ne savait pas bien lire les signaux précurseurs. Elle se recroquevilla contre le mur de la bibliothèque. Elle avait rapidement compris que c'était le seul abri sur lequel elle pouvait compter, et elle enfouit sa tête dans sa tunique pour se protéger les yeux. Elle savait que c'était l'affaire d'une ou deux heures, le temps que la tempête déchaîne sa colère. Tandis qu'elle résistait ainsi, seule au milieu du désert, elle récitait le CRIME ET CHATIMENT LIBRE VN. Par une soirée extrêmement chaude du début de juillet. Elle serrait contre elle, à l'intérieur de sa tunique, dans un petit sac de chanvre tressé que lui avait confectionné Ada, les feuilles qui composaient le CRIME ET CHATIMENT LIBRE II de CAROLE LEWIS. Chaque nuit depuis son départ, elle s'offrait au milieu de sa progression une longue pause pour consigner sur les papiers les mots qu'elle avait retournés dans sa tête, alors qu'elle marchait sous la lune ou qu'elle somnolait après avoir dressé un camp de fortune pour passer les heures les plus chaudes de la journée. Chaque jour elle fabriquait une page. Elle avait atteint hier exactement la moitié de son libre. Il lui restait 365 pages pour finir l'histoire de Rodion Raskolnikov torturé par le remord et pourchassé par ses pairs pour avoir assassiné une vieille femme. Une histoire qui ressemblait maintenant à la sienne.

*

Ils avaient mis un an à découvrir le forfait original de Carole.

Après le meurtre d'Alexanderpouchkine elle avait continué de se rendre à la bibliothèque. Même quand les soupçons avaient commencé à se porter sur elle, même quand ils se sont faits plus insistants. Il était naturel que la mineure se rende dans sa salle, personne ne faisait le moindre lien entre les libres et le meurtre. Personne ne faisait de lien entre rien et rien. On regardait Carole avec méfiance, avec agressivité, elle ne parlait plus guère qu'à Ada et Robertlovis. Elle avait cessé de monter au jardin, elle voyait que Revel la recevait sans enthousiasme. L'enlumineur manquait de lumières finalement. Mais pour autant personne ne prenait d'initiative concrète à son encontre, elle pouvait aller et venir aussi librement qu'avant.

Ses voisins dans la bibliothèque (#TODO les_nommer), gênés par sa présence, avait changé de salle, laissant les leurs inachevées. Carole ne clôturait plus les libre-libre, elle se contentait de regarder la première page, puis de les feuilleter rapidement. Qu'aurait-elle fait d'une phrase ou d'une page, même intéressante, qui ne soit pas celle d'une histoire complète, qui ne soit pas de FRIEDRICH WILHELM NIETZSCHE. Elle posait ainsi chaque jour plusieurs pierres noires.

Elle termina sa salle ainsi rapidement, en moins d'un mois. Quand elle quitta la communauté, les salles voisines de la sienne furent réinvesties, mais personne ne songea à entrer dans la salle 6157 qu'elle avait minée durant plus de six années. Une salle dont toutes les pierres étaient noires. Une salle à laquelle manquait un libre entier et 729 pages extraites de plusieurs libres, au hasard autant que l'on pouvait en juger. Pendant un an, personne n'y songea.

*

Chaque instant que Carole ne passait pas à construire les lignes de son libre, elle les passaient à repenser à l'acte terrible qu'elle avait commis. Elle avait envie de se faire mal, d'ôter sa tunique, de s'exposer nue aux sables cinglants de la tempête, de rester sans protection sous le soleil à midi, pour en sentir le feu. Plusieurs fois, William l'avait observé s'offrir ainsi à la douleur. Mais, à la fin, elle retournait se protéger à nouveau, récitant comme une litanie le CRIME ET CHATIMENT LIBRE VN, ou parlant à Raskolnikov, ou encore, racontant sa douleur. Des phrases qui naissaient à nouveau quelques heures plus tard sur la pages qu'elle noircissait.

*

Ada et Robertlovis avait dit au revoir à Carole. Vous les entendez leur avait-elle dit l'avant veille. Ils sont après moi. Ils n'osent pas, mais je sens chaque jour leur peur, comme si j'étais d'un autre monde qu'eux. Peut-être est-ce le cas, peut-être es-tu tombée de la lune que tu regardes si souvent, avait répondu Robertlovis. Peut-être, qui sait, hein Bob ? Ils n'osent pas, mais ils pourraient s'en prendre à toi tout de même, à la longue. Je vais partir, essayer de trouver Celine, visiter d'autres villes et leur parler de mon libre. Ada avait pleuré. Bob l'avait serrée.

*

La tempête s'était éteinte, presque aussi brutalement qu'elle étaient apparue. Carole avait repris sa route aussitôt. Ce n'est pas parce que l'on va nulle part qu'il faut traîner en route, ma vieille. Elle pensait souvent à Ada, elle lui manquait. Parfois à Bob. Carole espérait aussi retrouver Celine. Elle avait envie de revoir son libre. Elle commençait aussi à penser qu'il lui faudrait bien la faire connaître sa suite. Rencontrer des âmes qui auraient déjà entendu le CRIME ET CHATIMENT LIBRE VN de W F N et qui seraient excités de savoir qu'il existait une suite, qu'elle pourrait leur montrer. Le CRIME ET CHATIMENT LIBRE II de CAROLE LEWIS. Carole exprimait ses pensées à voix haute, tropisme de la solitude. William enregistrait tout. Il enregistrait également son spectre cérébral ce qui lui donnait des indices concernant celles qui restaient silencieuses. Il enregistrait les vibrations de chaque particule de son corps.

Le jour elle cheminait pensant à Ada, Bob, et ce qu'elle ferait quand elle aurait fini son libre.
La nuit tombée, elle commençait par relire les pages de la veille, parfois elle s'autorisait une correction. Pourquoi une fois écrits les libres ne pourraient-ils pas être réécrits ? Elle faisait aussi des copies de pages plus anciennes, qu'elle ne remettrait plus en cause. Son écriture s'était assurée, elle fabriquait à présent de belles lettres. Ada aurait été fière d'elle. Les libres devraient pouvoir être copiés. Ainsi elle pourrait en laisser un exemplaire pour chaque communauté. Il ne leur sera pas nécessaire de miner pendant des générations pour trouver un libre-ivre, elle en apportait un. Le sien.
Puis, elle écrivait une page, chaque jour une nouvelle page. Pour cela elle continuait d'utiliser l'une des 729 qu'elle avait subtilisé aux libres de la bibliothèque de Ramon Kacew. Durant deux heures, environ, chaque jour, Carole fabriquait de nouvelles phrases.

William avait passé les vingt-deux autres heures de la journée à fabriquer des milliards de milliards de versions simulées de Carole, qui chacune produisaient des milliards de milliards d'exemplaire de pages. Il faisaient varier infinitésimalement chaque simulation, modélisant des états corporels, des environnements, des Ada, des Roberlovis, des Alexanderpouchkine vivants ou morts, des Celine présents ou disparus, des Revel... Et pourtant cette presque infinie combinatoire, William ne parvenait rien à extraire. C'était fascinant. Il mobilisait l'essentiel de la capacité de calcul disponible sur la Terre, et pourtant, c'était encore des milliards de milliards de milliards de fois insuffisant. La page que Carole écrivait, elle ne parvenait jamais à la connaître à l'avance. Durant ces deux heures, à mesure que Carole écrivait, que se réduisaient petit à petit les champs du possible, elle essayait tout de même de deviner un paragraphe, une phrase, parfois juste un mot, et la plupart du temps il échouait, parfois jusqu'au dernier mot de la page.

Pourtant il existait un modèle, une combinaison qui représenterait Carole si précisément qu'elle produirait la bonne page. William en explorant tous les mondes possibles tomberait sur celui de Carole. C'était une question de temps.

William aussi voudrait écrire un libre.

*

Carole se sentait seule

*




**
copier, chercher des combinaisons, ce n'est pas ainsi qu'elle avait appris joué, mais depuis qu'elle avait perdu son corps l'interface n'était plus elle même. Elle se demandait parfois si elle existait encore.


wiliam avait conscience d'avoir perdu de son sens lorqu'il avait perdu son âmes, qu'il manquait de corps et que cette absende rapport au physique au monde le désservait pour les parties qu'il jouait, mais c'étit néanmoins sa fonction et il devrait la remplir du mieux qu'il pouvait


*



Richard passait à présent la majorité de son temps à étudier les communautés de William. Celui-ci aurait dû abandonner la partie depuis longtemps maintenant, Carole avait réussi. C'était évident. Il pouvait attendre qu'elle termine son libre, bien sûr, mais l'esprit du Jeu de la Création aurait requis qu'il lui reconnaisse la victoire dès à présent. C'était vraiment un connard. Un gros connard. Ou alors il avait un espoir, quelque part, que quelqu'un doubla Carole. C'était impossible. Il n'y avait rien qui allait en ce sens. C'était impossible. Alors Richard scrutait. Il attendait. Un gros connard lui avait entendu dire William à voix heute.

*

William animait un millier de communautés à l'attention de Richard. Il les localisait en Amérique du Nord. Bien sûr, il s'agissait de pures simulations, choisies au hasard parmi les milliards de milliards que William avait généré. Richard devait penser que la partie était équilibrée, que chaque joueur avait à peu près les mêmes chances. Paradoxalement il avait plus de chances avec ses cinq cents communautés, que William avec des milliards de milliards de communautés virtuelles.

 D'autres IAM animaient également les communauté des simulations des autres joueurs. Il n'y avait pas d'autres joueurs.

 Les représentations étaient uniquement destinées à Richard.
animait environ mille milliards de communautés virtuelles qui n'étaient pas pour Richard, mais pour explorer d'autres possibilité, pour comprendre.
Il n'y avait pas d'autre joueur, pas d'autre âme. Seul Richard jouait la partie avec des êtres de chair. William et les autres IAM ne manipulaient que des représentations. Ils n'étaient d'ailleurs pas vraiment des individus séparés, ils partageaient l'intégralité de leurs données à chaque instant, entre eux et avec toutes les autres IAM. Ils avait la même mémoire, ils étaient simplement le nom de processus différents qui se répartissaient les tâches. Toutes étaient tournée vers le même but. Jouer au jeu de la création. [Le denier domaine dans lequel un être humain semblait posséder un savoir qu'elles n'avaient pas. Posséder un savoir était un bien grand mot, car Richard ne semblait pas conscient de son savoir. Simplement il parvenait à faire créer les conditions de la création alors qu'elles ne parvenait qu'à reproduire des situations déjà créées. Reproduites à l'infini, magnifiées à l'infinie, avec une infini rapidité.]

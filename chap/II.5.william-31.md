William, jour 761 -> Wasiam ?

­­— *All in*

Richard présentait des symptômes équivalent à ceux l'ébriété en prononçant ces mots. Sa vision était troublée. Ses mains tremblait. Sa voix avait chevroté malgré lui quand il avait prononcé « all in ». Il clignait des yeux, comme s'il regardait ses écrans, le paysage désertique derrière son dôme, à travers un filtre flou. Richard avait conscience de son état de fébrilité. [William savait qu'il se rassurait en se disant que personne n'en saurait rien, l'image transmise aux autres joueurs était corrigée pour ne laisser paraître qu'un visage impassible.] [Poker face]. Ce n'était pas un acte irréfléchi, bien entendu. Mais pour autant, il jouait gros. Et il n'était pas certain de gagner.

William allait suivre, bien entendu. Il allait prolonger au maximum cette partie. Collecter toujours plus de données. Il fallait assister à la fin de cette partie. Il perdrait probablement. Ça n'avait aucune importance.

*

Alexanderpouchkine avait les yeux grands ouverts. Ils semblaient prêts à sortir de leurs orbites. Personne n'avait pensé à les lui fermer, par manque d'expérience. Son front et sa figure étaient ridés, son visage  était altéré. Il ne se ressemblait plus. Il devait avoir été pris de convulsions au moment de sa mort. De deux larges entailles à la tempe, le sang avait jailli à flot, formant une mare brune sur le sol. Son corps s'était écroulé par terre dans une position surprenante. Presque comique avait commenté un des libérateurs. Son bras droit formait une pliure invraisemblable, il s'en était servi pour se protéger et il avait reçu le premier coup. Sa jambe gauche s'était brisée sous l'effet de la chute, au niveau du genou, Alexanderpouchine était vieux et ses os n'étaient plus très solides.

Malgré le crâne fracassé et le corps disloqué, on ne pouvait s'empêcher de penser, quand on le regardait, qu'il n'était peut-être pas mort. Est-il vraiment mort, demanda un des libérateurs présents, je veux dire, en est-on certain ? Absolument ?

La petite pièce renfermait un lit simple. Les draps était froissés, et des tâches rouges laissaient penser que quelqu'un s'y était essuyé des mains ensanglantées. Quelqu'un a frappé cette âme, à deux ou trois reprises, avec une hache, ou un fin marteau, peut-être une pierre un peu aiguisée, puis il s'est enfuit, après avoir, certainement, essuyé ses mains, et peut-être son arme, sur les draps, décrivait un second libérateur, arrivé plus tôt, qui avait eu le temps d'analyser la situation.

En dehors de cela, le reste de la pièce semblait intacte. On ne pouvait deviner ce qui s'était passé ici, avant le drame, ce qui l'avait déclenché. Qui a bien pu faire cela ? Et pourquoi ?

Les cinq libérateurs qui contemplaient la scène étaient démunis. Jamais à leur connaissance la communauté n'avait connu de meurtre. Il faudra bien trouver qui a fait cela, s'accordèrent-il. Mais quand à savoir comment s'y prendre. Demandons à chaque citoyen s'il a commis ce crime ou s'il sait qui l'a commis, proposa le troisième. Qui pouvait en vouloir à ce pauvre Alexanderpouchkine, demande le quatrième. Il y a tout de même plusieurs personnes qu'il a humilié, répondit le dernier, mais de là à faire cela. Est-ce que l'un d'entre nous connaît un libre qui traite de cela. Qui relate un meurtre. Appelez les inventeurs, qu'ils nous rappellent tous les titres des libres. Peut-être dans une autre communauté. Nous pourrions dire aux mineurs de chercher plus spécifiquement des libres qui parla de meurtre. Cela pourrait nous aider. Mais combien de temps mettrons-ils à trouver quelque chose ? ne devrions nous pas agir avant ? Oui, mais agir comment ?

#TODO  qui se souvient des libres autres communauté.

#TODO Les inventeurs vont nous aider, ils vont chercher dans leurs index mémoire quels libres abordent ce monstrueux sujet du meurtre.

Ils ne connaissaient que la blibothèque, le meutre, c'était quelque chose que l'on rencontrait dans les libres, mais que cela existât...
Ghujk se demanda s'il existait d'autre chose des libres qui pût exister en réalité, comme les chevaux, les voitures ou la neige.

Il devait, se disait-il trouver un libre qui parla de cela, il existait un libre qui expliquait pourquoi le mond était monde et comment il puvait devenir autre.

#/TODO



*

Carole était rentrée chez elle. La chaleur était suffocante. Elle était bouleversée. Elle pleurait. Ada lui demandait ce qu'il se passait. Elle la regardait, et s'effrayait de la découvrir tant vieillie. Ses vêtements n'étaient pas soignés, ses cheveux étaient sales, ils collaient sur son front fiévreux. L'odeur de transpiration remplissait la salle. Ada réalisa à cet instant que Carole n'était plus cette jeune âme qui l'avait séduite à 16 ans. Depuis plusieurs jours, elle ne parvenait plus à écrire. Les pages vierges s'empilaient. Elle fanait. Robertlovis les regardait le visage fermé. La communauté était en effervescence. Il se sentait très mal.

L'état d'âme de Carole pouvait aussi bien relever d'un maintien S+ que d'une retombée A- qui pourrait être fatale. William établissait une analogie entre l'attitude de Richard, celle de Carole Lewis et les théories de Rodion Raskolnikov, le personnage du libre du CRIME ET CHATIMENT LIBRE VN. Un être exceptionnel, un sur-homme, une sur-âme dirait Carole, est libre de tout acte, y compris le plus violent, pour peu que celui-ci ait du sens. Qu'il le conduise à quelque chose d'autre, de mieux. De plus grand. Pour peu qu'il permettre de gagner. Il y avait également une proximité entre leur états d'âmes. Sentiment de puissance mélangé de remords et de peur. C'était la première fois que William observait un lien entre un joueur, une âme artificielle et un personnage de libre. Il devait déterminer si c'était un hasard ou si cela avait à voir avec sa problématique. Il revisionna toutes les parties jouées depuis le début du Jeu de la Création. Rien de probant. Il recommença. Il continuait de les analyser inlassablement tout en observant Richard, ainsi que Carole, et même l'ensemble des âmes artificielles de la cité Romankracew. Chaque détail pouvait être déterminant dans les jours qui allaient suivre. Il mobilisait toutes ses ressources.

*

William s'arrêta sur une scène qui s'est déroulée un mois auparavant. Carole échangeait avec Revel. Ils cheminaient dans le jardin, comme lors de chacun de leurs échanges, réguliers à cette période.

-- Tu sais toi, ce qu'est un étudiant ? C'est une sorte d'enlumineur ?

-- Oui, c'est quelqu'un qui réfléchit au monde qui l'entoure. Il apprend des autres et construit des idées. C'est ce que nous faisons ici.

-- Je connais un étudiant qui... qui écrit des pages.

-- Qui écrit ?

-- Oui, ses idées il ne les dit pas seulement, il les écrit.

-- Quel enlumineur ? Je crois que je le saurais...

-- Il s'appelle Raskolnikov. Tu ne le connais pas, il n'est pas au jardin.

-- Un enlumineur qui n'est pas au jardin. Et qui écrit. Tu me racontes une histoire bien étrange Carole. Est-ce que tu es en train de te divertir de moi ?

-- Non, vraiment. Il existe. Mais qu'est-ce que tu en penses ? Tu n'as jamais eu envie toi aussi, d'écrire tes discours.

-- Mais non, voyons ! Quelle méchante idée. Les pensées sont faites pour être contées.

-- Ha bon. Pourquoi ?

-- Je ne sais pas, c'est ainsi. Les libres ne sont pas fait pour être écrits, ils sont faits pour être lus ! Tu ne vois pas qu'un discours est vivant, alors que les mots d'un libre sont comme endormis ?

-- Imaginons que tu écrivent tes mots, des mots-dormants, ensuite tu pourrais les relire et les réveiller, non ?

-- Pourquoi faire, c'est inutile.

-- Pour ne pas les oublier.

-- Au contraire, ça ne produira que l’oubli dans l’esprit de ceux qui apprennent, en leur faisant négliger la mémoire. En effet, ils laisseront à ces caractères étrangers le soin de leur rappeler ce qu’ils auront confié à l’écriture, et n’en garderont eux-mêmes aucun souvenir.

-- Elle est belle cette phrase. Tu devrais l'écrire.

-- Elle l'est déjà.

-- Tout à été déjà été écrit, c'est cela ?

-- En effet. Cette phrase est tiré du PHEDRE de RACINE.

-- C'est dommage, avait conclu Carole.

Il avait peut-être sous-estimé l'importance de Revel. Il revisionna toutes les scènes de tous les libre-penseurs de Richard sur l'ensemble des parties jouées depuis le début du Jeu de la Création. Il les compara à toutes celles des tous les autres joueurs, les siennes y compris. La combinatoire était vertigineuse, les données affluaient.

*

Carole s'était peut-être ouvert les portes de la suite de son libre. Ou pas. Rien ne garantissait que son acte réglerait lèverait sa barrière. Ni même que, comme Rodion Raskolnikov une fois encore, cela ne la conduise pas au contraire à un état de dépression extrême qui l'empêche d'agir. A-. Dans ce cas, William gagnerait sûrement, Richard n'avait pas d'autre âme artificielle assez avancée. Rien ne disait non plus que la réaction de la communauté ne l'empêcherait pas mener son projet à son terme. Bien sûr, au contraire de l'univers du CRIME ET CHATIMENT, la communauté Romankcew ne connaissait ni police, ni juge, ni loi. Rien n'était prévu dans un pareil cas. Personne ne pouvait prédire avec exactitude comment les âmes allaient réagir. Pas même lui. Pas même William.

*

-- Est-ce que tu as déjà entendu que quelqu'un avait volontaire tuer une autre âme ? demandait un peu plus tard Carole à l'IAM. Ella avait eu envie de poser cette question à Revel, mais ne s'y était pas résignée. Elle ne sentait pas assez à l'aise avec lui. Il tendait trop à revenir à ses dogmes. Il avait beau croire qu'il jouait à imaginer le monde, il jouait finalement toujours avec les mêmes règles.

-- Tes questions sont surprenantes Carole, répondit l'IAM, tu voudrais que ce soit déjà arrivé ?

-- Je me le demande simplement, parce que dans mon libre, tu sais, les CRIME ET CHATIMENT, et bien l'âme principale, Raskolnikov, il tue une personne. Et je me dis que c'est possible, ici aussi dans notre communauté. Et je suis étonné que cela ne soit jamais arrivé.

L'IAM fit quelque chose de rare en consentant à répondre à une question :

-- Cela n'est jamais arrivé, Carole.

Celle-ci ne s'étonna même pas de recevoir une réponse de la part de l'IAM.

*

Carole avait réussi a produire vingt-quatre pages, une par jour, depuis que Alexanderpouchikne leur avait rendu visite et qu'Ada avait déclamé la scène du petit cheval sur la place. Mais ce qu'elle écrivait n'avait pas de corps. Elle n'y arrivait pas. Elle ne parvenait pas à décrire les émotions de Raskolnikov. Son univers lui était tellement étranger. Alors l'idée avait germée en elle. Comme Raskolnikov elle avait répété la scène dans sa tête. Comme lui elle avant elle s'était demandé si elle en était capable, si c'était sérieux. Comme lui elle avait compté les pas qui la séparait de la porte de la maison d'Alexanderpouchkine. Comme lui, elle avait frappé chez lui une première fois, pour reconnaître les lieux. Bonjour Alexanderpouchkine. Carole Lewis, que faites vous là ? Juste une visite de courtoisie, tu n'as rien dit, n'est-ce pas ? Il n'avait pas répondu. Il semblait vieux, las, mais elle lui trouvait toujours son air désagréable, méchant. Tant mieux. Elle s'en était voulu de cette visite. Et si maintenant il devinait quelque chose ? S'il parlait ? Comme Raskolnikov, elle avait ensuite renoncé, décidé que ce n'étaient que rêveries monstrueuses. Comme lui elle avait décidé à nouveau que c'était nécessaire, parce qu'elle avait besoin de cela pour écrire son libre. Comme lui, elle avait peur. Et puis, elle avait décidé que c'était elle l'héroïne et que cela lui donnait la possibilité de commettre cet acte. Elle ne craignait pas vraiment Alexanderpouchkine, mais il était ce qui ressemblait le plus à sa victime. Il était une âme mauvaise et inutile. Elle y avait un intérêt, préserver son secret. Alexanderpouchkine était ce qui ressemblait le plus à la vieille du CRIME ET CHATIMENT LIBRE VN. Et puis, tout ça n'était qu'un jeu, n'est ce pas ? Simplement il ne lui plaisait plus de vivre comme ils leur plaisaient.

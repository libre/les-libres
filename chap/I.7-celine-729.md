à quoi sert ce chapitre ?
Carole annonce la fin de son libre
description de la cité comme il arrive,
parle avec la population

Thématique de la création
Céline est sur le point d'être un artiste, il est sur le point de créer de la musique, il joue des âmes, il compose...
Il crée aussi les espaces qu'il visite, comme un photographe, il les fixe dans sa mémoire, les cadre
.
Ada, Carole et Robertlovis passaient à nouveau beaucoup de temps ensemble.


Celine, livreur

Céline marchait plein Sud. Le soleil était juste levé. La bibliothèque, sur sa droite, était frappée de plein fouet par la lumière horizontale. Il marqua une pause en haut de la dernière colline, pour savourer son arrivée. Pour boire sans retenue, à vider sa dernière outre. Sa dernière colline après les centaines qu'il venait de laisser derrière lui. Une petite vallée plus loin, il apercevait la cité Roman. Te voilà, toi. Ça y est. Son IAM formait une ombre si longue qu'elle semblait lancer une passerelle à travers la route, entre la cité et la bibliothèque. Il admirait une fois encore, en contre-bas, l'infini agencement de triangles qui recouvrait l'ouest de la Terre. Ma bibliothèque. Tu vois, on est arrivé. Je te l'avais dit. Il l'avait suivi sur le presque [demi-millier] de kilomètres qu'il avait parcouru depuis la cité Philipk qu'il avait quittée soixante jours plus tôt. La bibliothèque était son guide. La bibliothèque était son rempart, lorsque, chaque début d'après midi, alors que le soleil était passé à l'Ouest, il pliait sa toile et se remettait en marche, rasant les murs encore brûlants, protégé tout de même par leur ombre, ténue, puis, petit à petit, presque rafraîchissante. Au cœur de l'été, elle lui offrait ainsi quatre ou cinq heures de progression supplémentaire. La bibliothèque était son havre contre les tempêtes. Sauf quand celles-ci dégringolaient de l'Est, qu'il était sans aucune défense, que la grêle et le sable mêlés le défigurait. Alors, il la maudissait, sa bibliothèque, d'être de ce mauvais côté, de le laisser crever, il jurait, il l'insultait. Salope. Putain. Tu me laisse crever. Il les détestait ses murs sans ouverture. Il la suppliait. S'il te plaît, rien qu'une minute, laisse moi entrer, je souffre, putain... Souvent il se roulait en boule contre elle. Après tout, elle était sa seule amie.

À l'Est, il n'y avait rien. Le désert à perte de vue. On distinguait parfois, du haut des plus hautes collines, un léger scintillement. Il avait croisé des colporteurs qui disaient qu'il s'agissait d'une autre bibliothèque. Ou peut-être même de la même. Mais aucun n'en était jamais venu. Et de ceux qui étaient allés voir, aucun n'était revenu. Des conneries. Il n'y a rien, à l'Est.

Il arriva à hauteur de la file que formait les mineurs matinaux, un serpent agile qui glissait de la ville à la bibliothèque. Il traversa silencieusement. Les visages se tournaient vers le colporteur, le saluait sans un mot, respectueux. Ils le presseraient de questions, bien sûr, mais un peu plus tard, demain. Ils lui laissaient le temps de se reposer, de récupérer une partie des forces qu'ils avait presque totalement consumées. Pour venir à eux. Pour leur apporter des nouvelles des autres cités. Pour leur parler des libres qui y avait été clôturés. Lui n'avait plus la force de prononcer un mot.

*

-- Dans la cité Pavl, elle vient d'être renommée, vous la connaissiez peut être sous le nom de cité de X, elle est située à quatre autres citées d'ici, on y a clôturé le LIBRE IVRE DES LIBRE IVRE de PAVL OTLET. Il contient les titres de tous les libres-ivres.

La foule rassemblée sur la place de la cité regardait le colporteur avec admiration. Il savait y faire. Il avait occupé les premières heures à parler de nouveaux libres de moindre importance, un libre-mère par ci, MOBY DICK, un libre-plage par là, LES FURTIFS, à répondre à quelques questions sur son voyage. La bibliothèque, bien sûr qu'elle ne s'arrête jamais. Oui, à chaque ouverture, il y a une cité. Chacune contient une IAM, vraiment, similaire à la vôtre. En tous points. Le libérateur le plus puissant que j'ai rencontré, c'était encore plus au Sud qu'ici - oui, je suis déjà allé bien plus loin - il connaissait quatorze libres-ivres par cœur. Il me les a récité. La foule était déjà captivée, mais à présent, il avait allumé son bouquet final. Le LIBRE IVRE DES LIBRE IVRE de PAVL OTLET.

-- Un libre-ivre qui contient tous les titres de tous les libres-ivres ?

-- C'est incroyable !

-- C'est sublime !

-- Je crois que je me ferai colporteur dès demain pour aller le voir.

[-- Est-ce que tu ne devrais pas t'entraîner un peu ? Résister à la chaleur, à la fatigue, à la soif...]

Le colporteur les regardait. Son plus grand plaisir à lui, n'était pas de rencontrer de formidables libres, mais de provoquer ces émotions par le seul effet de sa voix. Il jouait de la foule comme le vent jouait des grains de sable. Une pensée fugace le traversa, qu'il n'avait jamais eue avant. Je pourrais les inventer ces libres exceptionnels que je raconte, ça les impressionnerait encore plus ces badauds. Il chassa la pensée interlope.

-- Ce n'est pas possible colporteur, il y bien trop de libres-ivres pour qu'un seul libre en contiennent tous les titres. C'était un libérateur qui parlait, il savait de quoi il parlait.

-- Pourtant les quatre premières lignes de ce libre sont : JE CONTIENS TOVS LES TITRES DE TOVS LES LIBRES IVRES DE TOVTE LA BIBLIOTHEQVE. DONT LIBRE IVRE DES LIBRES IVRES.

-- Je suis d'accord avec Marcus, ajouta Revel, qui jusque là s'était tu. Réfléchissons. Tu nous dis que ce libre contient bien son propre titre. C'est lui-même un libre-ivre, soit. Mais si nous considérons seulement tous les libres-ivres qui contiennent eux aussi tous les titres des libres-ivres, mais avec un titre différent. Celui-ci devrait aussi en contenir tous les titres, n'est-ce pas ? Or il est évident que la multitude de titres possibles est bien trop grande, à elle seule, pour être seulement présente dans un seul libre. LE LIBRE DE TOUS LES LIBRES. UN LIBRE POUR LES UNIR TOUS. LE LIBRE DE SABLE. Pourquoi pas ? Alors si nous considérons tous les autres libres-ivres, l'impossible devient absurde...

Le libre-penseur marqua une pause. Son teint devient livide. Comme s'il allait se sentir mal. Il venait de s'en rendre compte. Personne d'autre ne semblait l'avoir remarqué. Sauf Céline. Il lui avait jeté un regard complice. Revel venait d'énoncer des titres de libres-libres. Cette drôle idée venait de faire son apparition deux fois en quelques minutes. Inventer des libres. L'autre a été moins rapide à s'en débarrasser. Le pauvre vieux, il est tout chamboulé.

-- Il n'y a peut-être que les livres-clos dans ce libre-ivre, se hasarda une esclave.

-- Cela impliquerait que le contenu de ce libre-ivre devrait changer chaque fois qu'un nouveau libre-ivre est clos ? Est-ce que nous devrons aussi imaginer que la mémoire des libérateurs qui l'ont appris changerait simultanément ? clôtura un autre libre-penseur en posant comme une question ce qui n'attendait pas de réponse.

-- Vous chicanez avec le colporteur, enchaîna un autre esclave, pourtant ici quelqu'un, un inventeur peut-être, peut-il nous réciter plus de titres que ne peut en contenir ce libre de tous les libres ?

-- C'est qu'on ne les aura pas tous encore clos.

-- Pas même tous découverts.

Chacun débattait à présent, dans un brouhaha qui plaisait à Céline. Son brouhaha. La musique qu'il jouait avec la foule. Sa foule. Mais ses pensées s'échappaient malgré lui, il ne parvenait pas à profiter autant qu'il l'aurait voulu de ce moment, qu'il avait pourtant tissé de ses vœux, dans chacun de ses sommeils légers passés sous le soleil de plomb des journées de son voyage. Ses pensées vagabondaient. Imaginer des libres ?

-- Comment pouvez-vous être sûr de vous, alors ?

-- Parce que la bibliothèque contient tous les libres !

-- Je suis bien d'accord !

-- Et donc il a raison.

-- Qui a raison ? Ça ne prouve pas qu'il y a tant de libres-ivres qu'ils ne peuvent pas être cités dans un seul libre-ivre !

-- D'autant que le libre lui même le dit.

-- Mais il existe forcément un libre qui dit le contraire, qu'il ne peut pas exister de libre-ivre qui contient...

-- Il existe ?

Alexanderpouchkine s'avança vers le colporteur et cria presque pour se faire entendre.

-- Céline, dites-nous, est-ce qu'il contient par exemple, pour commencer, VNE HISTOIRE DE LA CREATION ? C'était le libre le plus connu de la cité Roman, probablement de tous les autres cités, même si malheureusement aucun des libérateurs d'ici ne le connaissait.

-- Je ne connais pas le contenu de tout le libre, je ne suis que colporteur. On m'a rapporté en tous cas qu'aucun inventeur de cette cité ne connaît de libre-ivre qui ne soit pas dans le libre-ivre des libres-ivres.

-- Je me souviens moi, parvint à placer d'une petite voix une petite âme située au premier rang, et qui n'avait rien dit jusque là, qu'on nous avait raconté l'existence d'un libre-aile qui contenait les titres de quelques libres-îles.

Céline ne les écoutait plus. Il rêvait d'un libre qui décrirait comment construire les libres. Avec un tel libre, je pourrai recréer tous les libres de la bibliothèque.

*

-- Bonjour Céline.

-- Bonjour.

-- Merci d'avoir accepté de nous rencontrer avant ton départ.

Ils s'assirent en tailleur tous les quatre. Il se demandait ce qu'il faisait dans cette maison où une natte, des coussins et un thé fumant avait été disposés à son attention, avec goût. Sûrement Ada. Robertlovis et Carole lui semblaient trop préoccupés par autre chose. Carole semblait absente. Robertlovis ajoutait une goutte d'eau de vie à son thé. C'est finalement lui qui prit la parole.

-- On se demandait, enfin, on te demande, est-ce qe tu as déjà entendu parlé d'un libre qui raconterait la suite d'un autre libre ?

-- Une suite ? Tous les libres forment une suite... en quelque sorte.

-- Non, mais la suite d'une histoire. Voilà. Un libre raconterait une histoire, un libre-ivre, et un autre commencerait où le premier s'est arrêté. Sa première page serait en quelque sorte une 730ème page.

-- Ça doit exister, forcément. Bien sûr. Il y aurait par exemple... voyons... bon rien ne me vient. Mais il est certain que tout libre a une suite dans la bibliothèque, c'est logique. Un libre-penseur t'expliquerait cela mieux que moi.

-- Ce que je cherche, coupa Carole, c'est s'il existe un libre qui serait la suite de CRIME ET CHATIMENT LIBRE VN de FRIEDRICH WILHELM NIETZSCHE.

-- La suite ? Mais je ne connais même pas ce libre...

-- J'ai fini de lire ce libre, mais l'histoire est incomplète, ajouta-t-elle, elle semblait à présent avoir quitté sa rêverie, elle le regardait avec insistance.

-- L'histoire de quoi ? Mais, de quoi parles-tu à la fin ? Je ne comprends rien à vos histoires.

-- Je vais te dire quelque chose colporteur, si tu me promets de ne pas le répéter dans cette cité.

Carole avait l'air grave.

-- Quelle sorte de colporteur je serai si je colportais les messages à l'intérieur d'une même cité !

-- Ni dans aucune autre cité proche, non plus. C'est un secret, aucun colporteur ne doit revenir le raconter ici avant longtemps.

-- Ça c'est plus difficile à te garantir, les colporteurs ne sont pas doués pour les secrets...

-- C'est la chose la plus incroyable que tu as jamais entendue, ce secret, argumenta Carole.

-- Bon...

-- Jure, ordonna Ada.

-- Je jure.

-- Sur la bibliothèque.

-- Sur la bibliothèque.

Carole regarda Robertlovis et Ada. Racontez-lui.

Ada commence. Puis c'est au tout de Bob. Je n'en crois pas un mot de leur histoire de libre-ivre sorti de la bibliothèque page à page. Je ne dis rien, je bois mon thé. Ils sont barrés tous les trois. Je pense à me casser. Carole se lève. Ada dit, tu as juré colporteur, n'oublie pas. Elle me fais peur cette fille. Carole pose devant moi, une à une, les 729 pages du CRIME ET CHATIMENT LIBRE UN de FRIEDRICH WILHELM NIETZSCHE. La tête me tourne. Putain. Robertlovis me refile un peu du contenu de sa flasque, l'eau de vie me tourne la tête dans l'autre sens. Elle finit à peu près droite. Je touche les pages. C'est bien l'histoire la plus incroyable qu'on m'ait jamais raconté.

-- C'est une histoire incroyable. Je vais regretter de ne pas pouvoir la raconter de si tôt. Enfin, je vais me la raconter à moi, ça va habiter mon prochain voyage. Et un jour... Merci. Vraiment.

-- Merci ?

-- C'est ce qui me fais vibrer les histoires exceptionnelles, quand je sais que je vais les raconter et qu'elles vont faire vibrer les autres âmes. Je suis un musicien, vous savez. Je joue de l'âme.

-- Alors ? demanda Carole, son regard était suppliant.

Merde. Sa suite.

-- Ce libre, tu sais Carole... il existe bien sûr. Il en existe plusieurs des suites à ton libre. Il en existe un nombre inconcevable, même. Mais, tu sais, enfin... il est si peu probable qu'il en soit trouvé un seule. Enfin, bientôt. Et près d'ici... Non, bien sûr, pour am part je n'ai jamais entendu parlé de la suite de ton CRIME ET CHATIMENT... Mais tu le savais déjà.

Elle semblait être sur le point de pleurer. J'ai cru voir du pur désespoir dans ces yeux. Mais non. Elle m'a fixé bizarrement, comme si elle cherchait à deviner un truc que je lui aurais caché. Puis, finalement, tout de même, quelques larmes coulèrent sur ses joues.

-- Tout ça n'est qu'un jeu colporteur. N'est ce pas ? Tu le sais toi. N'est-ce pas ? Que mon libre existe ou pas n'a pas d'importance. Que j'ai passé deux années à en dissimuler les pages dans mon pain, chaque jour, jour après jour, à les apprendre chaque nuit. Tu sais que je n'ai presque pas dormi durant ces deux années ? Tu sais que ce libre, je le connais par cœur, aussi bien qu'aucun libérateur ne le connaîtra jamais ? Mais cela n'a de sens que pour moi. J'ai eu ce libre, j'ai fait de ces deux années quelque chose d’exceptionnel. Je ne l'ai plus, je vais faire autre chose n'est ce pas ? Toi tu as une belle histoire à raconter à présent. Ada et Bob se sont trouvés. C'est ça la vie. Un jeu.

-- Je peux te raconter une histoire moi aussi ? J'ai rencontré une fois un colporteur, en plein milieu du désert. Il ma raconta qu'il avait été en présence du SITIUM de HERMES et que ce libre expliquait qu'il existait une autre bibliothèque et un autre monde, similaire au nôtre à l'Est et que la moitié des libres y étaient enfouis et que si nous ne traversions pas le désert, ces libres, nous ne les connaîtrions jamais. Tu y crois, me demanda-t-il ? Que tu as vu ce libre ? Non, répondit-il, qu'il existe une autre bibliothèque avec d'autres libres ? Le croire ? Je ne sais pas, quel sens cela a d'y croire ou de ne pas y croire ? Tu ne l'as pas découverte cette bibliothèque n'est ce pas ? Est-ce qu'un libre-ivre existe ? Tant qu'il n'a pas été touché, il n'existe pas. Tant qu'il n'a pas été ouvert il ne contient rien. C'est nous qui créons les libres en les parcourant. Ce sont les mineurs. Tu comprends ? La bibliothèque, c'est nous qui la créons. Tu vois, Carole, j'ai créé cette ville en y entrant, je crée le monde en le parcourant. On crée le monde en se créant soi-même, car après tout il n'existe pas d'autre monde que celui que l'on vit. On crée le monde en en faisant de lui ce qu'on veut qu'il soit.

-- Mais, on ne crée pas les libres car ils sont déjà créés, n'est ce pas ?

-- Bien sûr que si qu'on les crée. Tu crois que je fais autre chose que créer des mondes, moi ? Mes voyages, ils ne servent qu'à ça. Cadrer des paysages, les fixer dans ma mémoire. Comme si chaque grain de soleil venait traverser mes yeux, puis cramer un bout de ma mémoire pour toujours.

Ada et Robertlovis l'avait écouté en silence. Carole avait cessé de pleuré. Elle semblait avoir décidé quelque chose.

*

Pour la première fois Celine allait regretter un peu de quitter une cité. Comme s'il allait s'y passer quelque chose. Comme si une histoire qu'il n'aurait jamais l'opportunité de raconter allait s'y construire. Tout de même, il partait avec l'histoire du CRIME ET CHATIMENT volé. Ce n'était pas rien. Il commençait déjà à construire ses phrases, ses effets. Il y a dans la cité ROMANKACEW un libre-ivre dont seules quatre personnes connaissent l'existence. Tu en fais partie de ces quatre, me demandera-t-on. Je répondrai que j'en fais partie. Et je commencerai à jouer de la foule, comme d'un instrument.

*

En quittant le village Celine s'arrêta devant l'IAM. Il avait pris cette habitude de s'entretenir avec les IAM de chaque cité. Chaque interface était à la fois la même et à la fois différente. Où, est-ce simplement lui qui était différent à l'issu de chacun de ses voyages, et l'IAM n'était-elle pas finalement qu'un miroir qui lui renvoyait le reflet de son âme, ou qui se déformait selon ses états d'âmes à lui.

-- Est-ce que vous êtes des IAM différentes, ou bien les multiples têtes de la même ? Vous vous ressemblez.

Richard -> Carole

La Terre sur laquelle je suis né était condamnée, on me l'avait dit dès ma naissance ou presque. Mes parents avait eu plus de temps que prévu, c'est à dire qu'à la fin des cinquante ou cent ans de préavis prononcés au début du XXIe, la Terre n'était toujours invivable. Jusqu'au XXIIe ils avaient réussi, tant bien que mal, à coup de petit sacrifice, d'avancée technologique, d'un peu plus d'autoritarisme, à faire en sorte que ça ne craque pas. Il y avait beaucoup de guerres, on laissait filer les pandémies, ça faisait du bien à la planète. Ce qui faisait mal à l'homme faisait du bien à la planète. Il aurait fallu penser autrement, vivre autrement, ensemble, en commun. C'est ce qu'on a fait plus tard. Quand on a été moins nombreux.

*

Carole écrivait, lentement. Elle mémorisait et elle écrivait. 729 caractères par soir. C'était peu. À la nuit tombante elle sortait ses feuilles, les tiges et l'encre qu'elle confectionnait à chacune de ses étapes et qu'elle protégeait précautionneusement. Elle commençait par terminer d'écrire la phrase laissée en suspens la veille, elle s'en souvenait toujours. Richard restait à la regarder silencieusement. Puis elle lui lisait la dernière phrase qu'il avait prononcée, seulement la dernière, et il reprenait, plus ou moins là où il en était. Parfois il poursuivait le fil de sa pensée, parfois il l'avait abandonné. Elle ne comprenait pas tout, mais plutôt plus que dans le CRIME ET CHATIMENT LIBRE UN. Il faut dire que Richard s'adressait à elle.

*

On vivait de moins en moins bien, donc, mais ça tenait. On continuait d'avancer avec le soleil dans la gueule et les pieds au bord du gouffre, mais on cherchait des petits passages pour continuer encore un peu plus loin. On avait perdu pas mal en libertés fondamentales, tout le monde s'en rendait compte, mais les libertés à la con, consommer, travailler, être spectateur de conneries préfabriquées, celles-ci n'avaient pas été entamées. Les gens étaient encore trop bien traités pour se révolter. Et puis l'incertitude... Alors, ils préféraient continuer. Encore un peu. La répression des états était à peine nécessaire. Et c'est comme ça qu'on avait pu fourguer du bon gros capitalisme un siècle de plus. Capitalisme et IA.

*

Ils avait leur rituel. Richard reprenait son histoire, Carole commençait à écrire. Elle l'arrêtait quand elle savait qu'elle en avait assez. Il regardait ce qu'elle écrivait, voulait rependre ses mots, le dire différemment, mais Carole l'en empêchait. Elle avait tout mémorisé. C'est ta spontanéité qui est belle. Ce sont ceux-là les mots de mon libre. Il insistait un peu, là, disait-il, j'ai dit un gros mot, là, ce n'est pas clair, une répétition. On s'en fout que ce soir clair, elles sont jolies tes répétitions, disait-elle. Et puis tu n'as qu'à écrire ton libre toi-même à la fin. Dans le miens, je veux ces hésitations.

*

L'idée était simple, tout ce que tu savais faire toi-même, même si ça ne t'avais jamais fait chier de le faire, tu le déléguais à une machine contrôlée par des entreprises qui appartenaient à des gens riches que tu ne connaissais pas mais qui t'avais convaincu qu'ils allaient faire ton bonheur. Jusqu'à ce que tu ne fasses plus rien toi-même, et petit à petit que tu ne saches plus rien faire par toi-même. Décapacitation. Dépendance aux machines. Même mourir ça devenait le problème d'une IA. On se programmait une sorte de vie artificielle pour après la mort. Les machines avaient médié tout notre rapport au monde, du début à la fin, naturellement, enfin artificiellement.

*

Quand elle avait fini d'écrire sa page, souvent Richard aurait voulu continuer, il avait tant à raconter. Demain disait Carole, on a le temps. Elle faisait alors glisser sa tunique et mettait ainsi un terme à la séance. Après qu'ils aient fait l'amour, c'était elle qui parlait. Elle lui posait des question sur son monde. Pourquoi l'as tu fait ainsi, notre monde, lui demandait-elle, en substance. Il n'avait pas tout le temps de réponse. Ils arrivaient à la conclusion qu'il lui plaisait ce monde, à elle, alors que ce n'était pas si important de savoir pourquoi il était fait ainsi. Puisqu'il lui plaisait à elle. Plus tard, elle lui avait aussi parlé d'elle et de ce qu'elle avait fait. Des moment de désespoir qui l'avait saisi quand seule dans le désert, elle avait pensé, chaque jour, au visage d'Alexanderpoukine déformé par la douleur.

*

La natalité n'a pas été un problème, les gens se sont arrêté de faire des enfants assez facilement. C'était un peu plus dur de faire renoncer à la vie ceux qui étaient déjà là. Les gens te disent qu'ils sont malheureux, mais ils veulent quand même rester. Et puis, les plus riches étaient presque devenu immortels. Mais, ça n'avait pas été si compliqué, pas de grandes scènes apocalyptiques finalement. Juste une décision. Était advenu le temps de s'occuper de la Terre. Alléluia. Les machines fournissaient des prévisions pour réduire les hommes. Pas les machines. Personne ne s'étonnait. La décroissance humaine s'est organisé comme le reste, économiquement. Business as usual.

*

-- Es-tu un dieu, Richard ?

-- Un dieu ?

-- Puisque tu as créé notre monde. Et puisque tu ne vis pas seulement 27 années, puisque tu crois que tu ne vas même pas mourir du tout.

-- Je n'y ai jamais pensé comme cela...

-- Je crois que j'aime que tu sois un dieu.

*

Les entreprises Eutaniennes ont achetées environ la moitié de l'Amérique du Sud, les Russes et Chinoises l'essentiel du reste du monde. Elles ont décrété que ces zones seraient des poumons, que les hommes n'y mettrait plus les pieds. Elles ont envoyé des robots y planter des arbres. Le programme mondial Contribution a été mis en place pour réduire de 10% la population en 10 ans. Ça devait suffire.Ça ne suffirait pas du tout. Il faut que tu comprennes qu'au début du XXIe siècle, un petit millier de personnes possédait environ autant que le reste du monde. Et que, les conditions climatiques bordéliques n'avaient pas aidé, un gros paquet de gens étaient vraiment dans la merde. Alors quand on te propose de disparaître en échange d'un gros chèque pour tes enfants, tu acceptes.

*

-- Notre monde, tu seras obligé de le détruire ?

-- Pour faire une autre partie, oui, il faudrait.

-- Et si tu ne faisais pas d'autre partie ? Tu m'as expliqué que tu avais gagné d'autres territoires, n'est-ce pas, tu pourrais nous laisser, nous ici ?

-- Je pourrais, oui.

-- Si je te le demande, tu le feras ? C'est un peu grâce à moi que tu as gagné.

-- Pourquoi pas, Carole. Jusqu'à la fin de ton existence, au moins.

-- Même après. Ce n'est pas pour moi, mais je voudrais aussi qu'Ada élève son enfant, et qu'il grandisse et qu'il vive dans ce monde. Tu m'as expliqué que les machines s'occupaient de tout, sous terre, que ça durerait tant que le soleil brillerait à présent, et qu'aujourd'hui la Terre pourrait être repeuplée. Je voudrais bien que ce soit par les enfants d'Ada que la terre soit repeuplée.

Il ne répondit pas que les enfants d'Ada était les enfants des machines, elle n'aurait pas compris. Et puis, ça n'avait pas d'importance d'où sortaient les enfants. C'était bien que soit les enfants des machines et des âmes.

*

Ceux qui n'avaient pas d'enfant pour qui se sacrifier, on leur proposait de s'acheter une vie de pacha en contrat à durée déterminée. Par exemple, pour cinq ans. Villa, une pute dans chaque chambre, piscine, hélicoptère, voitures de luxe, une pute sur chaque siège. Mais pour cinq ans. Après, bye bye. Mais entre ça et 60 ans de misère, mi-bidonville, mi-prison, tu choisis quoi ? Tout n'était pas irréprochable. Ça a été la fête des pervers. Une semaine de pédophilie freestyle. Une semaine de torture. Bourreaux et famille des victimes étaient payés. C'était immoral ? Laisser crever la planète, tu crois que c'était mieux ? Alors, objectif atteint. Moins 10% en 10 ans.

*

-- Tu sais que j'ai tué un homme, n'est ce pas ?

-- Oui.

-- Ça ne te dégoûte pas ?

-- J'en ai tué 10 milliards.

-- Ce n'est pas pareil.

-- Non, ce n'est pas pareil. Sans ce meurtre ton monde ne saurait pas la chance qu'il a de vivre sans violence, où avec si peu. C'est difficile de comprendre

*

La GLU aussi était aussi intéressante, la grande loterie universelle. Chacun prenait des tickets, autant qu'il voulait. Chacun rapportait une petite somme immédiatement, disons 10 dollars. Et une chance d'être tiré au sort à la fin de la journée. J'ai connu un type qui a accumulé près de cent millions comme ça, il prenait 1000 tickets par jour. Il est toujours passé au travers. Enfin, il a fini bêtement sur une embolie. J'en ai connu un autre qui a été chopé dès son premier ticket. 10 dollars.

*

-- Est-ce que tu as essayé de nous construire un monde parfait ?

-- Oui, au début, c'est ce que je voulais.

-- Mais tu perdais ?

Il sourit.

-- Oui je perdais. Un monde parfait, c'est figé, c'est froid. Et puis, surtout, ça n'était parfait que de mon point de vue. Ça ne marchait pas. Dès qu'il y a deux âmes dans un monde, elles se font chacune une idée différente de la perfection. J'ai essayé de vivre seul aussi, à la perfection. Je me suis surtout fait chier. La perfection, c'est chiant. Alors j'ai cherché autre chose, un équilibre plutôt qu'une perfection.

-- Tu as réussi je crois.

-- Il faut des épines aux roses aurait dit un prince perdu dans le désert.

-- Je suis une épine, alors ?

-- Une rose avec des épines.


*


Mais ça ne suffisait pas. D'abord les forêts replantées manquaient d'eau. La monoculture d'arbre à pousse rapide les rendaient fragiles, in fine les feux et les maladies bouffaient la moitié de ce qui avait été planté. Les espèces avait été mal choisies. Erreur de calcul des machines. Pas de leur faute, on avait manqué de paramètres. On avait jamais encore replanté de planète, manque d'expérience. Globalement les 90% qui restaient restaient trop nombreux. Ils continuaient de consommer des conneries illimitées. Business as usual.

Je t'ai parlé de l'augmentation de la durée de vie ? C'est pas tant que ça se ressentait beaucoup sur la population totale à ce stade, ceux qui en profitaient était tout à fait minoritaires, mais ça bouffait une énergie folle. Passer quelqu'un en chambre de revitalisation, c'était la consommation d'une ville d'un million d'habitant en énergie. Se faire faire un clone c'était à peu près le même tarif. Mais pourquoi s'en priverait-on les riches, puisqu'on pouvait le faire ? Liberté.

*

-- Richard, on va rester tous les deux ? Tu ne vas pas repartir dans ton havre ?

-- Je crois qu'on va rester tous les deux. Je suis bien.

-- J'ai pensé à avoir un enfant moi aussi, qu'est ce que tu en penses.

*

Second verdict : 20% en 5 ans. On va pas laisser crever la planète. Ça s'accélérait. Même ingrédients, mêmes promesses, fallait juste payer un peu plus cher. Mais les riches étaient assez riche et les pauvres assez pauvres. Ça a fonctionné comme ça, pendant 25 ans. Prévisions. Calculs. Plantages. Accélération. On était à un peu moins d'un milliard au milieu du XXIIe. Réduction de 90%. Pas si mal ? Le passage au million a été plus progressif. Arrêt de la reproduction humaine. 50 ans après, avec les conditions sanitaires durcies, 1 million, donc. Et fin du siècle, 1252. Les derniers. À peu près immortels.

*

-- Je pourrais nous construire un monde, rien que pour nous, Carole, qu'en penses-tu ?

-- Il est bien celui-ci de monde. Et moi, ces bouts de sable me suffisent.

-- Nous pourrions aussi aller vivre dans mon havre.

-- Je ne sais pas, oui, emmène moi visiter, mais puisque tu as choisi de vivre ici, toi, je pense que c'est ici que c'est mieux. Peut être, tout de même, j'aurais envie de voir ta lune. Tu pourrais m'y emmener ? On pourrait vivre là bas ?

-- Ce n'est pas très grand. Les espaces nous manquerait dans l'espace.

*

On joue.
C'est un peu comme jeter des dés. Sauf que ça n'a rien à voir. Sauf que c'est comme vouloir prévoir exactement le trajet de tes dés pour qu'ils atterrissent sur la face de ton choix. C'est même comme si tu devais prévoir exactement leur trajet pour savoir exactement par quelles faces ils allaient passer. Toutes. Sauf que tu ne prévois rien du tout. Tu choisis plutôt tes dés. Tu les construis en fait. Étape par étape. Tu les sculptes tes dés. Sauf que tu attends plus de deux cents ans qu'ils aient fini de rouler pour voir si tu as réussi. Pas grand chose à voir, à la fin. Tu vois.

*

-- Carole, il ne te reste que trois ans, tu sais.

-- Oui, avait répondu Carole, cela te rend triste de savoir que je vais mourir ?

-- Tu sais, peut-être que tu pourrais vivre plus longtemps, que ces 27 années... bêtement, je n'y avait jamais pensé, mais il doit y avoir un moyen, je peux chercher, je peux contacter mon IAM, envisager des interventions...

-- Je ne sais pas si j'ai envie de ton éternité, tu ne sembles pas tellement y tenir, toi. Trois ans c'est beaucoup déjà.

-- Trois ans, avait-il simplement répété.

Carole

-- Bonjour Carole.

-- Bonjour.

Elle ne s'étonna pas qu'il sut d'emblée son prénom. Elle l'avait vu émerger d'une colline une demi-heure avant. Minuscule silhouette, il avait grandi petit à petit, devenant ombre, contour, personnage. Il s'était dirigé vers elle quand à son tour il avait noté sa présence, ou peut-être se dirigeait-il vers elle depuis le début. Son pas était mal assuré, il progressait lentement, elle avait fait au moins les deux tiers de la distance qui les séparait. Il était uniformément vêtu d'un tissu qu'elle n'avait jamais vu, il réfléchissait le soleil comme un miroir, parfaitement lisse, sans un pli, si fin et si parfaitement appliqué à sa peau qu'elle pouvait distinguer, quand il fut suffisamment près, les contours de ses muscles, et même de son sexe. Ce tissu le couvrait totalement, de la tête aux pied, faisant fonction de chaussure, de tunique et de chèche. Il lui couvrait la totalité de la tête, mais il était transparent au niveau de la figure. Mais ce qui étonné la plus Carole était que la peau de l'homme était très pâle, presque blanche. À moins que ce ne fût sa combinaisons qui changea la couleur de sa peau. Elle essayait de le deviner, mais elle avait du mal à le regarder tant son costume était lumineux. Ce n'était pas un colporteur.

-- Mon avion est en panne. Là bas. Je... vous pouvez m'aider ?

Carole lui donnait vingt ans. Il n'était plus tout jeune, mais pas encore vieux. Elle le trouva bel homme, malgré sa peau blanche et glabre. Des traits  lisses, sans une ride. À peine vingt ans. Il dégageait pourtant une impression de maturité que les traits de son visage ne voulaient pas trahir. Il sortit une gourde d'une ceinture épaisse qu'il portait au niveau des hanches.

-- Vous en voulez ? De l'eau.

Carole but une gorgée, par politesse plutôt qu'autre chose, elle avait senti que cela lui ferait plaisir. Elle avait pris l'habitude de boire très peu. Souvent, il lui restait un peu d'eau quand elle rejoignait un nouvel oasis, chaque quatre ou cinq jours.

-- Je peux vous accompagner ? Je... vous allez où ?

Il lui parlait comme à plusieurs âmes, en disant vous. Elle se retourna à deux reprises pour regarder si quelqu'un était derrière elle. Et puis, elle trouvait qu'il posait ses questions sans conviction, comme s'il avait déjà les réponses. Ses hésitations paraissaient feintes.

-- Je vais à la prochaine cité, je ne sais pas encore comment elle s'appelle. Elle ne doit plus être trop loin, je marche depuis pas mal de temps. Tu peux m'accompagner, bien sûr.

Au début de son voyage, Carole avait déjà passé quelques jours à cheminer avec un colporteur. Elle marchait lentement et elle s'était arrêté assez longuement dans les premières villes. Le temps de s'habituer. Il l'avait rejointe. Les colporteurs se croisaient, mais il était rare qu'ils se rattrapent. Ça avait été agréable. Il l'avait accompagnée jusqu'à la ville suivante. Il parlait peu. Le désert lui avait-elle dit, c'est fait pour être là, c'est tout. Il n'y a rien à dire. Elle était d'accord, mais tout de même une présence, c'était agréable. Ils avaient quitté XXXX ensemble et elle l'avait regardé s'éloigner.

*

De ces premiers jours à cheminer dans le désert avec Richard à ses côtés, Carole gardait vraiment de bons souvenirs. Le désert, la solitude peut être, l'avait conduite à parler d'elle, de sa cité, de son libre. Elle lui avait raconté presque toute l'histoire, sauf Alexanderpouchkine, elle n'avait pas su, pas pu. Pas ce corps écroulé. Pas ce cri de surprise, cet autre, de douleur. Elle n'était pas prête. De son histoire de libre volé, il n'avait pas semblé très étonné. En revanche quand ils avaient fait l'amour, on eût dit qu'il faisait ça pour la première fois, à la fois émerveillé et gauche, et semblant vouloir dissimuler sa gaucherie. Elle avait souri, cela l'avait touchée.

Sa marche était maladroite, elle avait ralenti son rythme pour lui. Il buvait beaucoup d'eau. De jour, il ne pouvait se passer de sa combinaison plus de quelques minutes. Je brûlerais, disait-il. À cause de sa peau blanche. Parce qu'il avait bien la peau uniformément pâle. Et parce qu'il n'avait pas l'habitude du soleil. Carole se demandait où l'on pouvait vivre à l'abri du soleil. Elle lui avait demandé, il n'y a pas de soleil là d'où tu viens ? Là d'où je viens, avait-il répondu, laissant d'abord sa phrase en suspens. Et après un long silence, il avait répondu, là d'où je viens, c'était il y a longtemps.

-- Tu es né il y a longtemps ? lui demanda alors Carole qui n'avait pas bien saisi sa phrase.

-- Oui, il y a environ 1000 ans.

-- Avant la création du monde ?

-- Avant la création de ton monde, oui.

-- Tu étais la seule âme, alors ?

-- Non, nous étions très nombreux, il hésita, comme s'il cherchait à compter, nous étions des milliards.

-- Il y avait plus d'âmes que de libres alors ?

Il sourit à cette question inattendue.

-- Peut-être, oui. Il faut dire, il n'y avait que des libre-ivres, la bibliothèque n'existait pas.

-- Est-ce toi qui l'a créée ?

-- Oui, c'est moi.

-- Tout notre monde, c'est toi qui l'a créé ?

-- On peut dire cela. Enfin, pas tout le monde, pas le sol ou le ciel, mais ce qu'il y a entre les deux, dans ce coin de la planète, c'est moi qui l'aie conçu.

-- Même les IAM ?

-- Oui, les IAM aussi, celles que tu connais.

Carole resta silencieux quelques minutes. Cette âme un peu gauche et pas plus vieille qu'elle avait donc créé le monde. Elle accepta cette idée facilement. Il ne fallait pas juger les gens, n'est ce pas ? Qui aurait cru qu'elle avait massacré un pauvre inventeur avec une hache ? Est-ce qu'il le savait lui ?

-- Est-ce que tu sais tout ?

-- Non, je ne sais pas tout.

-- Mais sur nous tu sais tout ?

À son tour il resta quelques minutes silencieux. Il hésitait.

-- Je ne sais pas tout. Mais je sais beaucoup de choses.

-- Tu nous observes ?

-- Oui, parfois. Assez souvent.

-- Moi aussi ?

Il s'approcha d'elle, passa la main autour de sa hanche et hocha juste la tête en lui souriant. Carole y vit un mélange de confidence et de légère honte. Et peut-être autre chose, il semblait avoir envie de dire, oui, toi surtout. Elle pensa d'abord, tant mieux, je n'aurai pas à lui dire. Ça ne la gênait pas vraiment que les autres sachent, mais c'était de leur dire qui lui faisait peur. Puis elle réalisa qu'il la regardait elle, surtout elle, celui qui avait créé le monde. Ça lui faisait quelque chose.

-- Les autres, qui vivaient avec toi, ils sont tous morts ?

-- Presque, oui, petit à petit.

-- C'est toi qui les a tué ?

-- Non. Enfin, si, en quelque sorte. Je n'y avais jamais pensé comme ça. Ou alors, est-ce que je n'y ai pas toujours pensé comme ça ? Syndrome de survivant...

Carole ne comprenait pas ce qu'il disait, il semblait se parler à lui-même.

-- Si en quelque sorte, répéta-t-il. Disons que je les ai laissé mourir. Il n'allait pas très bien notre monde, tu sais. Mourir c'était la seule solution qu'on avait trouvé pour le sauver.

-- Alors, le monde que tu as construit pour nous, il est mieux ?

-- Je crois. J'ai sincèrement essayé.

-- C'est gentil, lui répondit finalement Carole, merci.

Est-ce que quelqu'un lui avait déjà dit merci ?

*

Quelques jours après leur rencontre, Carole et Richard atteignirent la cité Gabriel. L'IAM dressée annonçait la présence de la cité longtemps avant d'en apercevoir les premiers bâtiments ou les premières âmes. Carole la distingua la première, elle s'arrêta, et la pointa du doigt. Regarde, Richard, là-bas. On est arrivée. Elle vit au visage de Richard qu'il ressentait cette émotion si particulière quand une étape du voyage touche à sa fin. La fatigue s'invite, l'impatience, on a jamais été aussi prêt et pourtant on ressent plus que jamais la distance. On voudrait déjà être arrivé, et cette poignée d'heures qui reste, cette poignée de kilomètres, ridicule au regard du trajet déjà accompli, elle parait tout à coup monstrueuse, inhumaine, injuste. On voudrait plus que tout être déjà arrivé dans ce lieu où pourtant, on allait pas spécialement.

L'arrivée d'une colporteuse en ville était toujours un événement. On attendait avec impatience les nouvelles des autres cités, les extraits d'éminents libres qui auraient été découverts. Carole avait appris à goûter ce plaisir d'être écoutée. Elle buvait l'eau qui lui était portée en souriant et en remerciant, et en pensant aux récits qu'elle allait faire.

C'était son libre qu'elle colporterait aujourd'hui.

Les premiers mois elle avait apporté, de ville en ville, l'existence du CRIME ET CHATIMENT LIBRE VN. Elle omettait les préambules que Céline savait si bien et qui permettait de préparer la foule à l'apothéose. Elle n'en avait ni l'art ni la matière, elle n'avait, elle, qu'un seul libre à offrir. Mais quel libre ! Elle montait sur l'estrade et commençait simplement à réciter. La foule ne savait plus si c'était une colporteuse, une enlumineuse ou une libératrice, cette âme qui savait un libre-ivre, qui savait le dire, qui l'apportait d'une autre cité. Bien entendu, elle avait accumulé petit à petit, de cité en cité, quelques fragments complémentaire, et elle avait appris à présent patienter quelques heures avant de livrer son libre. Mais ces préliminaires étaient vite oubliés. Quand elle commençait de réciter le CRIME ET CHATIMENT, quelques minutes seulement après avoir commencé, les âmes présentes se rendaient compte qu'il se passait quelque chose d'extraordinaire.

Ensuite elle avait eu envie de faire connaître, avant même qu'il n'existât, le CRIME ET CHATIMENT LIBRE II de CAROLE LEWIS. Oui, je me nomme comme l'autre, répondait-elle, malicieusement, ajoutant encore à la confusion. Bien sûr. Et elle récitait les premières pages qu'elle avait créées.

Elle avait écrit la 729ème page. C'était la veille de sa rencontre avec Richard. Ici, dans cette cité, pour la première fois, elle pourrait le réciter en entier. Elle commencerait ce soir même.


*

Dans la cité Gabriel, il étaient resté deux semaines. Le matin, ils restaient au lit très tard, tapis dans la petite maison qui leur avait été prêtée. Une petite maison semblable à celles de Romankacew, pas très loin d'une place assez semblable aussi. Elles sont toutes pareille tes cités, Richard, avait-elle demandé ? Un peu oui. J'ai manqué un peu d'imagination, je m'en rends compte, d'un peu de poésie. Est-ce que je pourrais t'aider à imaginer des cités différentes ? Il avait répondu oui. Ils dormaient, faisaient beaucoup l'amour, mangeaient un peu. Richard était tout le temps en sueur. Il avait l'air de se demander ce qu'il faisait ici et en même temps il ne cessait de répéter, comment ai-je pu vivre tout ce temps ailleurs qu'ici, que dans cette chambre étouffante, avec toi ? Parfois ils écartaient un peu les tissus qui couvraient les fenêtres pour regarder remuer lentement la cité et ses âmes. Maintenant que mon libre est terminé, je crois que le plaisir de vivre va me suffire, disait Carole. Tu crois ? demandait Richard.

Chaque après-midi Carole captivait la foule avec le récit du CRIME ET CHATIMENT LIBRE II. Richard semblait perdu au milieu de ces gens si différents de lui, d'une autre couleur, d'un autre temps. Eux en revanche ne semblaient pas troublés par sa présence, les premières minutes de surprise passées. Mais cette colporteuse et ce libre ça c'était quelque chose ! Et lui comme eux buvaient les mots de Carole.

Il repensa au premier soir de leur rencontre. Elle lui avait récité les premières pages de son libre, il les connaissait bien sûr ces mots, et il l'avait déjà entendu les déclamer, mais là, sans interface, d'âme à âme, c'était tout autre chose.

-- Tu la raconte bien ton histoire, Carole, lui avait dit Richard.

Un peu plus tard quand il l'avait regardé prendre sa plume, il lui avait demandé, tu n'a pas encore fini ? Il l'avait vu la veille tracer la dernière lettre sur la 729ème page. Il se souvenait de la dernière phrase, c'était #$$$$$%%.

Elle ne le laissa pas regarder par dessus son épaule, dors, lui avait-elle intimé, je te le lirai plus tard, ou peut être seulement à la fin. Richard s'allongea, ferma les yeux. Il ne tricha pas, ne convoqua pas son IAM, il s'endormit en repensant aux villes, aux foules, aux routes couvertes de voitures, aux avions se croisant dans le ciel, aux rivières, aux forêts, à la pluie, au froid, et à tout le reste. C'était si loin, il devait faire un effort pour en rêver.

*

Emmène-moi dans la bibliothèque Carole, s'il te plaît, cela va te paraître incongru mais je n'y suis jamais entré. Ça lui avait paru relativement incongru, en effet. Ils avaient visité les salles, et pour lui, elle avait même endossé à nouveau son rôle de mineure, elle avait clôturé un libre entier. Des curieux étaient venus observer cette étrange colporteuse. Richard avait vibré à chaque page tourné, il ne pouvait s'empêcher de la parcourir aussi vie que possible, chaque fois espérant, chaque fois frustré. Ils n'avaient rien trouvé bien entendu, c'était un libre-libre. Il avait désiré si fort trouver ne serait-ce qu'un simple mot... Ce jour là, il avait approché le sens qu'il avait donné à son monde.

*

Un peu plus tard, après qu'ils eût quitté la cité et repris leur marche, il avait dit, alors qu'elle s'endormait sur le sable, c'est un peu la même histoire, tu sais, celle de de Rodion et la mienne. Il pensait probablement ainsi que la tienne, mais il ne le dit pas. C'est l'histoire d'un meurtre avec lequel il faut vivre. Rodion a tué une âme. Moi, j'en ai tué dix milliards. Pas brutalement, non, petit à petit, il m'a fallu plusieurs centaines d'années. Pas avec une hache, avec de l'**%%%%%**. Pas tout seul, nombreux sont ceux qui ont participé. Pas en secret, aux yeux de tous. Mais j'ai eu l'impression quand je t'écoutais chaque soir, malgré tout que c'était la même histoire. Il a peut être raison ton héros, peut-être qu'il faut des sacrifices. En tous cas, on ne s'est pas vraiment posé la question.

-- C'est une histoire tragique. Mais pas pire que ton CRIME ET CHATIMENT, si on y pense. C'est l'histoire de notre monde. Tu ne comprendras pas tout ce que je vais te raconter... mais ce ne sera pas pire non plus.

-- CRIME ET CHATIMENT, ça se passe dans ton monde, tu as connu Raskolnikov ?

-- Ça se passe dans un autre monde encore, celui qu'a créé Dostoïevski.

-- Je ne pas qui c'est Dostoïevski.

-- Disons que c'est un autre Nietszche.

Ce soir là où il avait été particulièrement triste, il lui avait fait penser à Bob. Elle avait essayé de se rappeler les gestes d'Ada qui savaient si bien soigner la tristesse de Bob. Elle n'avait pas vraiment réussi, mais tout de même, un peu. Il l'avait regardé et l'avait embrassé. Elle aimait bien Richard. Lui aussi semblait l'aimer. Elle passait de beaux moments à présent. Même si elle pensait encore à Ada.

*

Richard mesurait à présent réellement la taille du monde qu'il avait créé. Sur ses écrans ses territoires restait des cartes, des traits, des chiffres, les villes des points, mais là, à fouler le sol, à sentir la terre nue et craquelée, à attendre la ville suivante, c'était autre chose. Carole avait raison ses villes étaient trop semblables. Elles n'avaient pas été imaginées pour ses colporteurs. Il changerait cela, la prochaine fois. Est-ce que cela l'aiderait à gagner la prochaine partie ? Pour la première fois, il se demanda s'il y aurait une prochaine fois, s'il ferait une autre partie. C'était si loin de la vie qu'il menait à présent. Le jour ils parlaient peu, la marche était leur langage. Le soir, Richard racontait, il lui parlait de son monde.

Elle lui dit, je pense parfois, que je suis une IAM portable, à qui tu me racontes ton histoire et qui écoute.

-- Est-ce que tu l'écris aussi, mon histoire ?

-- Est-ce que les IAM écrivent ce qu'on leur raconte ?

-- Oui, en quelque sorte.

-- J'aimerai bien être l'IAM de Romankacew...

Les images de sa cité s'invitèrent brutalement, c'est alors elle qui fut d'humeur mélancolique.

-- Ada ma manque, lui dit-elle. J'aimerais tant savoir comment elle va.

-- Elle va bien.

Depuis qu'elle avait rencontré Richard, Carole avait compris qu'il disposait de pouvoirs mystérieux, mais pour la première fois elle réalisa qu'elle ne s'était jamais vraiment interrogée sur la portée de ces pouvoirs.

-- Tu peux la voir ?

-- Oui.

-- En ce moment ?

-- Oui, je la vois en ce moment

-- Je pourrais la voir, moi aussi. S'il te plaît ?

-- Ce n'est pas possible, c'est une image projetée dans ma rétine.

-- Mais qui projette cette image ? Carole voulait aussi qu'on lui projette des images d'Ada.

-- Un... une machine que j'ai dans l'œil.

-- Tu as une machine à l'intérieur de toi ? Moi aussi, je voudrais une machine.

-- Ce n'est pas possible comme cela, Carole, je suis désolé.

Pour la première fois elle envia Richard. Jusqu'à présent elle l'avait considéré avec étonnement et intérêt, mais jamais avec envie. Elle n'avait pas vraiment envie d'être née il y a mille ans, d'avoir connu des milliards d'âmes ou d'avoir eu froid, ou d'avoir trop chaud ou tout le temps soif. Même ne pas mourir, elle n'y tenait pas trop. Mais une machine pour voir Ada, ça si, elle aurait bien aimé.

-- Est-ce qu'elle a eu son bébé ?

-- Oui. Il va bien.

-- Décris-le moi

-- C'est une fille. Elle est belle, elle a tout le temps les yeux grands ouverts. Elle a l'air de s'intéresser à tout.

Des larmes coulaient sur ses joues tandis qu'elle l'écoutait décrire le bébé. Ada avait nommé son enfant Carole, comme l'autre du CRIME ET CHATIMENT LIBRE II.

*

Richard et Carole longeait la bibliothèque. Elle est vivante, pensait Richard. Carole lui avait annoncé qu'à la prochaine cité, elle commencerait à raconter son nouveau libre. Elle ne pensait plus beaucoup à ce qui s'était passé à Romankacew. Je crois que j'en ai fini avec les crimes et les châtiments, lui avait-elle dit. Il en découvrit les premiers mots en même temps que les habitants de la cité $$$$. Les premiers mots écrits par Carole un an plus tôt, quand ils s'était rencontré. MONDES DE RICHARD ET CAROLE.

La Terre sur laquelle je suis né était couverte de forêts et de rivières, et il y avait des milliards d'âmes qui y vivaient. Pas en communauté comme vous, mais en réseau, chacune des milliards d'âmes pouvait interagir avec chacune des milliards d'autres. C'est difficile à imaginer... J'ai dû mal moi aussi, à présent, à me représenter un tel monde.

Richard, jour 761

#TODO Carole écrit à la fin, elle utilise la plume d'Ada, elle laisse des notes à peine lisible, Ada les recopies. Elle retourne voir Alexanderpouchkine

­­— *All in*

La situation était parfaitement inédite et Ricard sentait les 180 pulsations minutes de son cœur lui perforer la cage thoracique. Une chaleur telle l'oppressait qu'il avait pensé que le havre avait cessé de fonctionné. Il regrettait son geste. Sa mise. Il savait qu'on ne pouvait faire se genre de mise sans le regretter. Il savait que si ses adversaires abandonnaient à ce stade, s'ils se couchaient, alors il aurait eu raison. Qu'il se féliciterait de sa décision pendant des mois. Des années, il y repenserait. Quel culot. Ce *all in*. Quelle classe. Quelle *vista*. Quelles couilles. Quelle intelligence. Respect. Il savait que s'il était suivi, la boule qui avait pris place dans son ventre ne le quitterait plus jusqu'à la fin de la partie. Que s'il perdait, il y repenserait au moins autant. Quel con. Quel blaireau. T'aurais pu réfléchir encore un peu. Si t'avais réfléchi un tout petit peu plus, tu l'aurais bien vu que c'était débile. T'aurais peut-être juste attendu. *Call*. Peut être juste une petite mise... Mais *all in* ? Sérieusement ? Il allait devoir attendre quelques heures que William ne se décide. Si quelqu'un devait le suivre, ce serait William. S'il suivait, il était mort. De trouille. Passe, connard. T'as rien, passe. Putain.

Bien sûr, Carole pouvait encore le faire gagner. Même si l'autre suivait. Elle le ferait gagner.

Il ressentait tout simplement l'émotion la plus intense de sa vie. Il était au bord d'un gouffre, il avait décidé qu'il savait voler. Il avait bondit. Ses ailes le porteraient et il serait le premier homme de l'univers à voler. Le premier homme libre du monde entier. Le monde lui appartiendrait. Ou il s'écraserait au sol. Comme une merde. Il avait mis sa vie en jeu.

Pas tout à fait. Il garderait une parcelle et un havre, bien sûr. Mais il avait misé le Sénégal, l'Europe, ses usines à âmes, sa bibliothèque, ses IAM. 1 milliard d'hectares, 35.000 âmes artificielles. Mille milliard de milliards de libres. Bien sûr, s'il gagnait... Il n'avait pas exactement calculé ce que William devrait mettre. Mais il y avait au moins l'Antarctique. Dernière zone naturellement tempérée, encore entouré d'eau fraîche. Il pourrait développer de nombreuses nouvelles IAM sous un climat si clément. Ça lui ouvrait des perspectives. Il serait sûrement un des cinq humains les plus riches de la Terre. Il faudrait calculer. Un peu plus tard. Si William suivait. S'il ne passait pas, ce lâche. Ce bâtard. Encore plus tard. S'il gagnait. S'il ne perdait pas. Putain de boule au ventre. Accepte Richard. Aime ce moment, mon pote. Mode H+/S-.

*

-- Laissez-moi me reposer, demanda Carole à Ada et Robertlovis. J'ai besoin de me reposer. J'ai besoin d'être seule.

-- Laisse-là un peu, dit Robertlovis à Ada alors qu'ils s'éloignaient, mais reviens la voir régulièrement, apporte lui à boire, elle est fiévreuse.

-- Pourquoi est-elle si malade ? Est-ce qu'elle va mourir ?

-- Non, Ada, ça va passer. Elle doit se reposer. Essaye de lui faire prendre un peu de soupe quand elle sera éveillée.

-- Oui, bien sûr. Bob, pourquoi tu as parlé de la ville qui est en effervescence ?

-- Ça n'a pas d'importance pour le moment, occupe-toi de Carole.

Robertlovis semblait terrassé, et cela inquiétait Ada au moins autant que l'état de Carole.

Carole resta couché longtemps, parfois elle semblait s'éveiller, mais une profonde léthargie l'empêchait de se lever. Ses mains tremblaient, elles étaient si pâles. Elle grelottaient, ses dents claquaient.
Des vertiges la prenaient lorsqu'elle essayait de se lever.

Elle finit par se mettre debout, elle n'avait aucune idée du temps qui était passé, il faisait nuit. Elle voulut s'habiller, mais constata qu'il y avait du sang sur sa chemise. Elle l'ôta, ne savait qu'en faire, finalement la glissa sous son matelas. Elle sortit son autre chemise dans un petit meuble. À bout de force, déjà, elle s'assit sur une chaise.

Ada la trouva là quand elle revint le voir, endormit. Elle l'aida à se recoucher, s'allongea quelques temps à côté d'elle, elle était brûlante, puis s'assit sur la chaise à son tour, elle la regarda longuement. Un peu plus tard elle sortit et la laissa à nouveau seule. Bob avait dit de la laisser tranquille.

*

Tous les autres joueurs s'étaient couchés, ils ne restaient que tous les deux. William et moi. Il a suivi. *Heads up*. Allez. C'est parti. L'étiquette voulait qu'il proposa un arrangement à William. Cela consistait à interrompre la partie sans en attendre la fin, en se partageant les gains selon une répartition qui reflétait au mieux l'état des chances de chacun. Le problème était que chacun avait son point de vue sur ses chances de gagner. Et chacun avait son point de vue sur l'intérêt de continuer à jouer. Richard se disait qu'il voulait aller au bout, voir Carole gagner à présent. Elle était allée trop loin pour lui, il lui devait celà. La laisser aller au bout de sa partie à elle.

Et puis, il se disait parfois que ce jeu était aussi une sorte d'expérience, d'étude. Ses âmes artificielles lui permettaient de vivre par procuration des centaines de vies. Mais il lui permettaient aussi de mieux comprendre la vie elle-même. Sa vie pour le moins. Ça n'avait pas beaucoup d'importance, parce qu'il ne faisait pas l'effort de formaliser ce qu'il comprenait, de le partager, mais tout de même, il aurait pu. Il le ferait peut-être, il avait le temps. En tous cas, cela donnait au Jeu de la Création une dimension supplémentaire. Une dimension créative justement.

Il irait au bout. Il allait faire à William une offre qu'il ne pouvait pas accepter.

-- *Deal* ? 90/10.

-- Cher Richard, voyons, c'est très impoli comme proposition !

-- Non, c'est réaliste, dans moins d'un mois j'aurai gagné la partie.

-- Ou bien tu seras reparti de zéro. Tu n'as pas grand chose derrière Carole.

-- Elle va gagner William. Alors, tu acceptes le 90/10 ?

-- Bien sûr que je refuse.

*

Carole est mal en point. Ce n'est pas un problème. Elle va remonter. L'état de Robertlovis est plus inquiétant. Décroche pas Bob, elle va avoir besoin encore un peu de toi. Faudrait peut-être qu'elle aille voir l'IAM. Cette situation n'avait jamais été envisagée, pas quelque chose d'aussi extrême, mais il avait confiance en ses modules de gestion de la violence.

Richard ne pouvait plus rien faire à présent, il lui fallait attendre que s'abattent les cartes. Peut-être une semaine. Peut-être demain. Peut-être un mois comme il l'avait prédit à William, pour fanfaronner. Peut-être jamais. Mais en tous cas, il ne pouvait rien faire. Il avait mis son tapis. Tout ce qu'il possédait. Il était à présent spectateur.

Il se plongea dans la reprogrammation de ses modules de violence avec acharnement. Pour la prochaine partie. Un acharnement qu'il n'avait pas connu depuis si longtemps. Il vivait, putain !

*

Comme Raskolnikov, Carole était à présent prostrée. Comme lui, elle s'était mise à dormir beaucoup et à faisait des rêves. Comme lui, elle ne savait pas toujours si elle rêvait ou si elle était éveillée. Comme lui, elle revoyait sa victime, ses yeux exorbités. La flaque de sang. Comme lui, elle réalisait que son geste était vain, non seulement Alexanderpouchkine n'avait pas parlé, mais maintenant, avec ce meurtre, elle risquait bien plus d'être découverte. Mais ça n'avait pas d'importance. Comme Raskolnikov, elle n'avait commis ce meurtre que pour savoir l'effet que cela ferait. De tuer quelqu'un. Quand Ada entra dans la chambre pour surveiller son état, lui porter de l'eau, elle murmurait dans un demi-sommeil qui semblait un demi-délire. Je sais... je sais.

*

Richard s'intéressa un peu à ses autres communautés, au cas où. Ce n'était pas sympa pour Carole. Fais-lui confiance, connard. Mais tout de même. Où en est Celine ? Il a déjà tout lu, évidemment. Mais il n'a pas encore dévoilé son secret. Il respecte sa parole donnée. Fais chier. Putain de module d'honnêteté. William disait qu'il n'avait rien derrière, et ce n'était pas faux.

Il réalisait que plutôt qu'un spectateur, il était peut-être à présent une marionnette. Il avait programmé ses âmes, les ancêtres de celles qui œuvraient aujourd'hui. Mais à présent, c'était elles qui décidaient de ses actions. Il n'avait aucune influence sur les actions de Carole, mais au contraire, c'étaient bien ses actions à elle qui déterminait tout ce qu'il faisait en ce moment. Étrange renversement de perspective. Ça l'amusait d'être la marionette de ses marionettes. Il essayait d'imaginer les fils que ça faisait.

*

Ada avait passé la nuit somnolant sur la natte du petit salon de Carole, maigrement calée entre les coussins. Elle était allée voir Carole chaque heure ou presque, l'avait fait boire un peu d'eau, elle refusait la soupe. Elle semblait folle ou mourante, et Ada était bouleversée. Au petit matin, éreintée, elle s'était endormi pour de bon. Lorsque Robertlovis entra sans frapper, elle se levé d'un bond, coupable d'avoir dormi. Sans dire bonjour à Robertlovis, elle se précipita dans la chambre de Carole et la trouva vide. Carole était sortie. Pour aller où ? Dans son état... Je vais la chercher dit Robertlovis, reste te reposer encore un peu, tu en as besoin. Ada n'était pas réveillée pour argumenter. Elle voulu s'asseoir sur le lit quand elle remarqua une page de libre posé près de l'oreiller. Elle l'examina et ne réalisa pas tout de suite que Carole l'avait écrite avec sa plume et son encre. C'était presque illisible tant les lettres étaient mal tracées. Ce qu'Ada lisait était à la fois rassurant et inquiétant. Carole avait recommencé à écrire son libre, mais d'une façon tout à fait différente. Les mots de Carole provoquaient à présent des émotions chez Ada ce n'étaient plus simplement des mots posés sur une feuille, c'étaient des filaments qui venaient la toucher, la palper. Ils ressemblaient à des incantations qui pouvait provoquer le rire, la peur, les larmes. Comme le CRIME ET CHATIMENT LIBRE VN. Comme la scène du petit cheval. Ada sourit, elle comprit que c'était ce que Carole cherchait, et qu'elle l'avait trouvé.

Elle sortit de la pièce avec la page, s'installa sur la petite table et se mit à la copier. Elle apprendrait à Carole à écrire mieux.

*

Carole était retournée chez Alexanderpouchkine, mue par un désir et une répulsion. Elle avait besoin de revoir les lieux. Elle fut étonnée de ne plus trouver le corps de la pauvre âme, ni de traces de sang. Alexanderpouchkine avait déjà été enterré, et le sang avait été nettoyé. Comme si en faisant disparaître ces traces, la communauté pourrait retrouver plus vit sa sérénité. Carole se demanda se qu'elle faisait ici, elle voulu ressortir. Mais elle réalisa qu'elle n'avait pas vraiment pris de précaution, on avait pu la voir entre, on pourrait à présent la voir sortir. On trouverait étrange sa curiosité morbide. On lui demanderait nécessairement des explications, et dans l'état où elle était, que pourrait-elle dire ? Pourquoi as-tu quitté le lit malade comme tu es, pour te rendre ici ? Et puis d'où te vient cette maladie, personne n'est malade en ce moment ? Et puis c'est vrai que tu connaissais Alexanderpouchkine, c'était l'inventeur de ta salle, n'est ce pas ? Est-ce qu'il t'avait nuit ? On sait qu'il pouvait être désagréable parfois. Est-ce qu'il y a des problème avec ta salle ? Avec un libre ? Où étais-tu la nuit précédente ? Est-ce que tu t'en serait prise à lui ? Tandis que son imagination s'emballait, elle restait figé devant la porte, incapable de plus l'ouvrir. Elle réalisé qu'à rester ici, elle prenait encore plus de risque, n'importe qui aurait pu venir. Elle se décida à ouvrir la porte, ne sachant pas si elle devait le faire lentement ou d'un coup, elle choisit de le faire lentement, mais quand la porte fut assez ouverte elle se précipita dehors, sans la ferme. Elle voulu courir, mais réalisa avec horreur que quelqu'un était là. Elle le fixait sans le voir. Elle était sur le point de s'évanouir. L'âme s'en aperçu, s'approcha et la soutint.

-- Viens Carole, on va rentrer, lui dit Robertlovis.

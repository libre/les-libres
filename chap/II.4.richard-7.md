Richard -> WASIAM
J2

#TODO Carole écrit une première page
#TODO passage de Richard - manque de première personne pour Richard
- Les clones, stérile, programmés et initialisés par Richard bébés produits par la machine
- à propos de Carole et Ada : avec leurs liens elles étaient plus libres



Ada restait à présent l'essentiel du temps chez Carole, elle lui réclamait tout le temps des histoires, des morceaux du libre. Elle disait Carole, donne moi encore un morceau de libre.

-- Il y a de nombreux passages que je ne comprends pas bien, tu sais, disait Ada. Mais il y a aussi des moment où il me semble que tout y est plus réel que dans notre monde. Que c'est cela qu'il aurait fallu vivre, plutôt que ce que nous vivons nous. Carole ?

Elle somnolait.

-- Raconte encore l'histoire du petit cheval.

-- Tu le connais par cœur Ada, ce passage, je te l'ai récité cent fois.

-- Je sais. Raconte-le moi encore.

-- Tu vas encore pleurer.

-- Je sais. Raconte encore.

Alors Carole racontait. L'histoire de cette vieille jument battue à mort par des hommes ivres, c'était le passage préféré d'Ada. Elle ne savait pas vraiment ce qu'était une jument, la scène restait pour elle assez étrangère, mais on voyait qu'elle provoquait chez Ada une infinie tristesse, devant l'innocence de la pauvre créature massacrée sans raison. Ada le savait par cœur ce passage, pourtant chaque fois elle éprouvait la même sensation d'horreur et de révolte. Cela la faisait se sentir vivante, pensait Richard.

-- Il y a tout le vide et en même temps toute la valeur de la vie dans cette histoire, n'est ce pas ? Des créatures innocentes qui meurent sans raison, parce qu'on les bât ou parce que leur vingt-septième année a sonné. Parce que leur propriétaire en a décidé ainsi. J'aimerais bien pouvoir lui parler au proprio, moi, Carole, tu sais, lui dire d'arrêter de taper.

-- Mais personne ne te tape, toi, Ada ? Qu'est qui te rend triste ?

-- Je ne sais pas. Vieillir. Mourir. Que ces instants avec toi, là, ne durent pas toujours... Je crois que je veux un enfant.

Carole avait fini de réciter, elle était sortie. Ada resta plusieurs heures étendue sur le lit. Immobile. Pourtant elle ne dormait pas. Ses lèvres bougeaient imperceptiblement.

*

Les âmes de Richard étaient stériles. Cela ne modifiait que peu ses chances de victoire. En fait ce paramètre les diminuait très légèrement, mais dans une mesure qu'il devait considérer comme négligeable. Il avait été montré que la créativité trouvait un exutoire dans l'enfantement, et que s'en priver était donc, plutôt favorable à une forme de sédimentations des pulsions créatrices. Mais à l'inverse l'idée de la transmission, la perspective d'un  héritage, était développée par le fait de disposer d'héritiers liés par le sang. Ça s'équilibrait plus ou moins. Mais de toutes façons, Richard était dérangé par la douleur associée à l'enfantement. Il l'avait consigné dans ses journaux. Ses IAM fabriquait les enfants. Dans son mode, il avait décidé que les âmes seraient les plus heureuses possible. Cela n'avait rien à voir avec le jeu. Il avait envisagé de les doter d'une immortalité comparable à celle qu'il pensait être la sienne, mais ce paramètre, en revanche était très significatif. Sans parler du fait qu'il n'était plus envisageable de repeupler intensivement la planète avec des humains, et qu'il devait bien développer de nouveaux clones à chaque partie du Jeu de la Création.

*

Alexanderpouchkine sortit de chez lui et se dirigea vers la bibliothèque. Robertlovis lui emboîta le pas. Il le fit avec si peu de retenu, que l'autre eût l'impression qu'il allait lui casser la gueule. Robertlovis le nota, il sourit à cette idée. D'habitude, c'est sa propre gueule qu'il cassait. La tronche apeurée d'Alexanderpouchkine, ça le changeait de la sienne dans le miroir. Comme il lui parlait souvent, à la gueule dans la miroir, pour l'engueuler, il décida de faire de même avec cette nouvelle tronche.

-- Hé ! Alexanderpouchkine !

Celui-ci ne le regarda pas.

-- Tu t'arrêtes boire un verre avec moi ?

Il était si proche qu'Alexanderpouchkine ne pouvait plus feindre de l'ignorer.

-- Non merci.

Tant mieux, avec cette crevure, même boire un verre, Roberlovis n'en avait pas envie.

-- Trop tôt pour les braves, hein ? Ou je suis pas assez bien pour toi. ? Bon. Je te suis alors, ça ne t'embête pas, hein ?

-- Un peu si...

-- Tant mieux !

Après quelques mètres, c'est Alexanderpouckine qui rompit le silence.

-- Qu'est ce que... Carole... qu'a-t-elle fait du libre ?

-- Elle l'a bouffé !

-- Elle...

-- Elle l'a libéré. Elle l'a appris par cœur, comme une libératrice. Et puis elle a bouffé les pages.

-- Mangé ?

-- Mangées. Une par une. Reste plus rien.

-- C'était... enfin... il y avait quoi dedans ?

-- Si je te le dis tu vas pleurer mon gars. Tu veux ? Une histoire complète. Libre-ivre. Dégoûté, hein ? Et si tu l'emmerdes Carole, elle l'oublie aussi sec. Personne ne saura jamais ce qu'il renfermait. Et, en plus, moi je te pourris la vie. Chaque jour. Et Ada te bouffe les couilles.

-- Qu'est ce qui me prouve qu'elle connaît le libre par cœur ? Qu'est ce qui me prouve que c'était bien un libre-ivre ?

-- Je vais te dire, viens, un soir. Viens l'écouter. Je te dirais quand. Tu sais déjà où.

*

Richard avait commencé à préparer la matrice de sa prochaine partie. Il y passait quelques minutes chaque jour. Les âmes initiales. Leur patrimoines génétiques. Leur connaissances de base. La génération d'une bibliothèque. Les programmes des IAM. Pour l'essentiel il repartait de la même base, on ne change pas une équipe qui gagne. Mais il apportait tout de même quelques corrections. Son taux de A+ était trop faible. Il avait eu du bol qu'Alexanderpouchkine s'en soit pris à Carole. Faudrait durcir un peu tout ça. À force de trop protéger ses clones, il voyait bien, dans plusieurs cités, ça végétait.

*

-- Carole, choisis un passage avec un méchant, demande Ada, en regardant Alexanderpouchkine.

Il était resté assis sur une chaise, à l'écart. Son visage était fermé. Il avait peur de ce qu'il allait entendre. Il avait aussi peur d'Ada. La petite semblait prête à lui fendre le crâne. Son regard vagabonda malgré lui dans la salle, à la recherche d'une arme qu'elle aurait pu saisir.

-- Ouais, un bien pourri, compléta Robertlovis.

-- Carole, raconte-lui le passage du petit cheval.

-- J'ai mieux répondit-elle. Voici ce qui m'inspire ce soir, pour honorer la présence de notre ami Alexanderpouchkine.

Carole ferma les yeux, posa sa respiration, et récita.

-- Et maintenant regardez par ici. Ce beau monsieur avec lequel je voulais me battre tout à l’heure il m’est inconnu. Je le vois pour la première fois. Mais il l’a remarquée lui aussi tout à l’heure sur son chemin devant lui. Il a vu qu’elle était ivre inconsciente et il a terriblement envie de s’approcher d’elle de l’emmener dans cet état Dieu sait où. Je suis sûr de ne pas me tromper. Croyez bien que je ne me trompe pas. J’ai vu moi-même comment il l’épiait mais j’ai dérangé ses projets. Il n’attend maintenant que mon départ.

Alexanderpouchkine écouta, sans ciller. Sans faire mine de s'intéresser. Sans faire mine de s'offusquer non plus. Il écouta Carole réciter longuement, sans une seule fois se reprendre, sans écorcher un mot. Sans faire mine d'être impressionné, non plus, il dit simplement, quand elle se fut tu et eut ouvert les yeux.

-- Qu'est ce qui me prouve que ce sont bien les mots d'un libre ?

-- Tu veux dire que Carole les aurait fabriqué ces mots ? demanda Ada, incrédule, mais quel connard !

Carole fit une réponse plus fine.

--  Est-ce que tu pourras jamais le savoir ? Mais moi, je peux te réciter à nouveau ce passage à l'identique. Or ce passage fait forcément partie d'un libre, n'est ce pas, de plusieurs, d'une infinité peut-être. Tu es d'accord ?

Il ne répondit pas.

-- Donc mes mots sont forcément ceux d'un libre, et tu ne sauras jamais si c'est celui-là qui manque dans la salle 6127, n'est ce pas ? Demande toi ce que ça change, dans tous les cas, tu perds le contenu d'un libre si tu ne fais pas ce qu'on te dit.

-- Votre chantage, hein ? Ce n'est plus un libre de toutes façons, tu en as arraché les pages.

-- Parce que tu en as perdu la peau tu penses en avoir perdu la chair ? Il est inscrit dans ma mémoire ce libre. Il était encore il y a quelques instants inscrit dans l'air qui nous entoure. Écoute, tu peux encore en sentir les vibrations dans ton corps, sur ta peau. C'est à présent un libre-voix.

*

Richard dotait ces clones de 27 années de vie, en fait entre 27 et 28 années moins 1 jour. Ainsi l'avait-il programmé. Certains décédaient avant, d'accident, mais c'était rarissime dans les communautés telles qu'il les avaient créées. Avec l'allongement de la durée de vie, au XXIIe siècle, de nombreuses recherches avaient été menées sur la durée de la vie. Un principe très intéressant, qui n'avait pas été réfuté, malgré de nombreuses tentatives, était la conservation de la quantité de vitalité avec l'allongement de la durée de vie. Henri Hermann, qui le premier avait publié ses résultats sur ce thème avait titré son article : Le temps ne fait rien à l'affaire. La quantité de vitalité, c'est ainsi qu'il nommait en quelque sorte la quantité de ce qui était fait dans une vie, était constante quelque soit la durée de vie. Plus un individu vivait vieux, plus le temps s'accélérait pour lui, faisant passer les années pour mois, voire des semaines, et moins il était acteur dans le monde, moins il faisait de choses. Il gaspillait plus de temps à des actes sans autre intérêt que de faire passer le temps, actes dont il ne ressentait que des plaisirs mineurs et immédiats et qui ne lui laissait aucun souvenir, aucune trace.
Hermann et ses équipes avaient ensuite montré qu'un niveau de maturité minimum était tout de même nécessaire et donc qu'il y avait en quelque sorte une durée de vie minimum pour exprimer la valeur de vitalité constante, qu'il avait nommé V. Les mesure la plaçait entre 15 et 25 ans, selon les époques. En revanche, que la durée de vie d'un individu soit de 30 ans ou de 200 ans, ce qui devenait un âge commun à cette époque, la quantité de vitalité était donc la même et Hermann avait même prévu qu'elle resterait constante même si les humains venaient à vivre plus de mille ans. Plus aucun humain ne faisait ce genre de recherche aujourd'hui que les humains atteignaient effectivement le millénaire, aussi sa thèse n'avait pas été validé. Mais elle n'était pas réfutée non plus, et Richard pensait souvent que le fait que personne ne se donne la peine de poursuivre ces recherches, que toute activité de recherche ait été abandonnée d'ailleurs, tendait plutôt à lui donner raison. La vie infinie, c'était la suspension infini de la vitalité.

*

-- Ada, tu viens avec moi dormir dans le jardin, il y fait un peu plus frais, et c'est beau de lire les étoiles entre les branches des palmiers. Bob, tu veux venir ?

-- Moi tu sais les jardin, c'est pas mon truc.

-- C'est quoi ton truc Bob ?

-- Ouais...

Tandis qu'elle étaient allongées, Ada demanda à Carole ;

-- Comment crois tu qu'ils sont les surâmes qui jouent avec nous ? Tu dis tout le temps qu'on est comme dans un jeu.

-- Je ne sais pas.

-- Ils doivent être immortels, c'est triste de mourir à 27 ans. Ils nous ont créées ?

-- Oui, je pense, ils ont créé le monde et nous avec.

-- Ce sont des êtres parfaits selon toi ?

-- Je ne sais pas.

-- Moi, je ne crois pas qu'ils soient parfaits, sinon ils aurait créé un monde moins pénible.

-- Il l'est tant que ça ?

-- Oui, à cause du petit cheval battu à mort.

-- Mais, ça c'est seulement dans le libre, Ada.

-- Il faudrait un monde sans l'idée même d'une créature que l'on peut battre à mort.

-- Tu es mélancolique ce soir.

-- Peut-être, à cause de la vie qui va s'arrêter alors qu'on a encore tant de chose à faire. Tu as ton libre à créer, toi. Et moi, aussi j'aimerais bien faire des choses. Il faudrait un monde sans la mort.

-- Pourtant je crois que si l'on avait aucune de ces idées, aucun de ces défauts, ça n'irait pas. On ne saurait pas vivre dans un monde parfait. On ne l'apprécierait pas. Ce sont toutes les petites douleurs qui rendent possibles le bonheur.

-- Tu crois que nous avons besoin de souffrir pour aimer être ainsi, heureuses sous les étoiles.

-- Peut-être, au moins un peu.

-- Ça ne suffirait pas de le vouloir, alors ?

Elles firent l'amour à nouveau.

Richard les regardait et trouvait ça beau. Il se sentais seul. Ça faisait longtemps qu'il ne s'était pas senti seul. Décidément elles le faisaient vivre. Elle avait raison Carole, ces petites douleurs étaient importantes.

Revel les trouva au matin, encore enlacées, sous le soleil déjà chaud. Ada s'éclipsa rapidement, elle éprouvait un sentiment inconnu en présence de Revel. Comme si elle devait regretter qu'il lui prenne des instants de Carole. C'était une sensation désagréable. Peut être de celles dont parlait Carole, qui rendent finalement le bonheur possible.

*

Richard se rappelait également un autre article célèbre de Hermann, publié celui-ci dans un journal satirique. À la question qu'on lui posait alors, si la durée de vie ne faisait rien à l'affaire, pourquoi les hommes continuaient-ils à la prolonger ? C'est comme pour la taille du sexe masculin, avait-il répondu, même si vous admettez que la taille ne fait rien à la qualité du rapport sexuel, vous préférez tout de même en avoir une plus grosse. Cette idée simple était resté sous la dénomination, non moins simple, de théorie de la grosse bite de Hermann.

*

« Richard, tu devrais regarder, c'est intéressant ». Il reçu le message au milieu d'une de ses balades. La place de Romankacew vint se poser par dessus l'océan qu'il admirait en silence. Il vit Ada monter sur la scène. Le foule l'engueula.

Qu'est ce qu'elle fait celle-là, elle monte sur scène ? C'est pas son tour. Hé toi !

Dès les premiers mots qu'elle déclama, Ada eut les larmes aux yeux. « Papa petit père, crie Rodia, petit père que font-ils ? Ils battent le pauvre petit cheval. Allons, viens, viens, dit le père. »

Ce n'est même pas une enlumineuse. Ce n'est pas celle qui est bizarre ? Si, je la reconnais, c'est cette âme qu'on peut surprendre à fabriquer des lettres par terre.

Ada n'écoutait pas les dilettantes, qui s'étaient approchés, elle poursuivait avec rage : « Fouettez-la sur le museau, dans les yeux, en plein dans les yeux, vocifère Mikolka. »

C'est quoi ce libre ? Elle n'a pas dit le titre ni l'autre ? Je n'ai pas entendu. Je ne crois pas... Est-ce le colporteur lui a appris cela ? C'est un grand texte, les colporteurs n'en apprennent pas tant. Et ce Celine, quand il est venu, il n'a jamais parlé de ce texte. Hé ! Âme, de qui est ton texte ? Mais Ada ne répondait pas. On entendit des « tais-toi, écoute ! ». Elle les avait captivé, à présent.

« Rodia s’approche du petit cheval, il s’avance devant lui, il le voit frappé sur les yeux, oui sur les yeux ! Il pleure. Son cœur se gonfle, ses larmes coulent. » Les larmes d'Ada aussi coulaient. Et nombreux était ceux dans le public qui les sentaient aussi, ces larmes qui leur montaient aux yeux.

C'est beau tout de même. C'est triste.

« Elle est à moi, hurle Mikolka, il frappe la bête à bras raccourcis. On entend un fracas sec. »

Elle est belle cette fille. Elle récite bien. C'est quoi déjà ce libre ? Carole et Bob se tenait en peu en retrait, ils regardaient avec admiration. Ada leur avait dit, venez sur la place, dans une heure, je vous ferai une surprise. C'était une belle surprise.

« Achevons-la, hurle Mikolka »

Quel salaud ce Mikolka ! Le pauvre petit cheval. Est-ce que tu sais ce que c'est toi un cheval ? Non, mais tu crois que ça peut mériter d'être ainsi battu ?

« Et pourquoi ne voulait-elle pas galoper ? ». Ada prononça ces derniers mots, descendit de l'estrade et s'en alla comme elle était venue.

La vision de Richard était brouillée dans son casque. Il pleurait. C'était sublime. Il ne se souvenait pas quand il avait pleuré pour la dernière fois, c'était il y a plusieurs centaines d'années. Il remercia Ada.

*

Richard avait calculé que la durée de 27 ans était optimale, pour que le potentiel de vitalité puisse pleinement s'exprimer, tout en ayant un renouvellement générationnel important. Il avait bien étudié Hermann, le fait que les clones connaissent la durée de leur vie permettait d'optimiser leur expression vitaliste. Mais il fallait un minimum d'incertitude à la fin, un parfait déterminisme détruisait toute conception de la vie. Un clone qui sait qu'il va mourir exactement tel jour s'arrête d'agir, abattu d'avance en quelque sorte, par cette perspective. Sa vitalité ne se développe quasiment plus. Il suffit d'une faible indétermination pour qu'un sentiment de liberté prenne la place de ce désespoir. Étonnante machinerie.

*

Ce soir là, Carole demanda à Ada de lui donner des lettres particulières. Sa septième page commençait ainsi :
#TOREDO
RASKOLNIKOV SE SOUVIENT DU
PAUVRE PETIT CHEVAL. IL EST
TRISTE. IL PENSE AUSSI A LA
VIEILLE.

Aujourd'hui Carole réclamait les lettres bien plus vite qu'Ada n'était en mesure de la fabriquer. Robertlovis avait été autorisé à aider, finalement.

-- Carole, faisons une pause, avait demandé Ada, redis nous les passage où ça parle de fabriquer des lettre. D'écrire, tu avais dit... " "

-- T'es chiante Ada.  / tiens tu ne veux pas le petit cheval, cette fois ?

-- Allez, redis. Tu verras.

*

-- Machine, je veux des choses pour écrire.

-- Écrire ? Sais-tu ce que cela veut dire ? De quelles choses parles-tu ?

-- Plumes. Papier. Secrétaire. Dictée. Roubles. Éditeur. Traduction. Mariage. Judiciaire.

Richard rit en visionnant cette séquence. Il imaginait la gueule de l'IAM si elle avait eu une gueule. Ça faisait si longtemps qu'il n'avait pas rit. Quelle maline cette Ada. Elle avait essayé tous les mots qu'elle ne connaissait pas liés à l'écriture dans le CRIME ET CHATIMENT. C'est le libre lui-même qui livrait le secret de l'écriture.

*

À présent, le soir, Ada ne confectionnait plus de lettres avec de la pâte, elle écrivait avec une plume et de l'encre sous la dictée de Carole. Elle allait bien plus vite. Et maintenant Carole parvenait à inventer une page chaque jour. Elle continuait l'histoire de Raskolnikov. Elle créait un nouveau libre. Un libre-âme. En revanche, elle avait demander à Ada de continuer à écrire sur des vraies pages de libre qu'elle continuait de voler dans la bibliothèque. Les feuilles que l'IAM donnait à Ada lui semblaient factices. Et puis elle avait son rituel.

Et Ada passait à présent le plus clair du temps qu'il lui restait à recopier la mémoire de Carole. Elle lui demandait chaque soir de réciter et chaque nuit, c'était son tour à présent, elle copiait. Elle refaisait un nouveau libre avec le libre-voix. Un libre-clone. Robertovis était à côté d'elle, il l'aidait parfois quand elle n'était pas sure. Souvent il n'en souvenait pas mieux, mais il se levait et il allait demander à Carole.

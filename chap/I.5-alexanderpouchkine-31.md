# Alexanderpouchkine, inventeur

[On doit revoir ses voisins : comment Carole échappe à leur regard, elle attend leur départ ?]
[Préparer le lien avec les philosophes dans la partie ou l'on aborde les courants de pensée]
[TODO le [X] de [X]]
[Il y a des choses étranges dans les libres : animaux, froid, travail, pauvreté.]


— Bonjour Alexanderpouchkine.

Cela faisait près d'un mois qu'Alexanderpouchkine venait visiter chaque jour la salle 9801 qu'occupait Carole Lewis. Elle devait se demander pourquoi il venait si souvent. Il ne le savait pas exactement lui-même. Ce n'était pas la première fois qu'il se prenait à tourmenter ainsi une mineure, mais il y avait autre chose. Elle l'attirait, c'était un fait. Elle l'attirait physiquement. Elle n'était pas particulièrement jolie. Il ne savait rien d'elle. Ils n'avaient échangé que quelques mots sans importance. Mais elle dégageait cette différence, elle était en dehors, au delà, en quelque sorte. Il avait envie de cette fille. Il enviait son apaisement. Et il avait envie de l'en faire sortir, de cette sérénité qu'elle lui jetait à la face. Et il y avait autre chose.

— Bonjour Carole.

Il y avait autre chose. Il avait acquis la sensation que quelque chose de crucial se jouait dans cette salle, avec cette fille. C'était bien entendu une salle en tous points semblable aux autres. Sauf peut-être quelques détails. Carole était si lente, il n'avait jamais vu une mineure si lente. L'escabeau qui n'était jamais exactement à la même place heurtait sa mémoire photographique. Pourquoi déplaçait-elle cet escabeau ? Il ne lui demandait pas. Il attendait, certain que quelque chose allait se passer. Comme la veille d'une tempête de sable, lorsque quelques signes singuliers l'annonçaient à qui savait les lire. Comme ce libre dont la tranche avait été abîmée. Il avait la sensation que ces sens enregistraient d'autres détails insolites qui échappaient à sa conscience. C'était le mystère que la salle renfermait qui le poussait chaque jour à revenir, plus encore que l'attraction que la mineure exerçait sur lui. Plus encore que l'excitation de l'affrontement qu'il avait décidé de lui imposer.

— Comment allez-vous aujourd'hui ? demanda Carole.

Elle doit faire référence à ce qui s'est passé hier. Cet idiot de Marcusalius. Je me suis trompé d'une étagère, ce n'est pas beaucoup une étagère, tout de même, répétait-il sottement. Je me suis trompé d'une étagère. Le libérateur, je n'ai pas retenu son nom, cherchait le [X] de [Y]. Les libérateurs apprennent les mots des libres par cœur mais ne mémorisent pas les emplacements. Leur mémoire ne s'encombre de ces informations subalternes. C'est bon pour les inventeurs, l'emplacement des libres. Bon pour ce Marcus. Bon pour moi.

— Vous voulez-dire après l'incident d'hier. À la taverne ? répondit-il.

Ils discutaient tous les deux, Marcus et le libérateur. Ce dernier avait perdu un temps considérable du fait de l'erreur. Une petite erreur, répétait Marcus. Le libérateur avait dû faire appel à un mineur pour extraire tous les libres-enclos de la salle, afin de retrouver celui qu'il cherchait. Les libérateurs ne sortent pas les libres. C'est le devoir des mineurs de prendre et replacer les libres, toujours à leur place initiale. Encore une chance que la salle était la bonne, avais-je alors ajouté, d'une voix bien haute, pour que tout le monde entende. Bien sûr que la salle était bonne. Tout de même. Se tromper d'une étagère ça pouvait arriver, n'est-ce pas, mais se tromper de salle, c'était autre chose. Et pourquoi m'en mêlais-je ? Il se défendait médiocrement. Il était rare qu'un inventeur se trompe d'étagère, il l'admettait. Très rare. Personne ne se rappelait qu'une telle erreur ait déjà été commise. Car à présent tout le monde dans la salle s'intéressait au cas de l'inventeur. C'était rare, certainement.

— Je voulais dire en général, je ne pensais pas à hier en particulier. Carole était gênée.

Alexanderpouchkine aimait ces moment où ils sentait la faiblesse d'autrui. Où il sentait sa propre puissance. La honte de Marcusalius hier. L'appréhension teintée d'agacement de Carole, suscités par la surveillance méthodique qu'il exerçait. Des défenses qu'il perçait. Il avait ce pouvoir de faire violence au monde. D'exercer des forces sur les âmes. Il trouvait surnaturel de parvenir, à distance, sans le toucher, à blesser si profondément quelqu'un. Il était certain d'être la cause de la disparition de Ferdinand Destouches, ce mineur qui était parti un jour dans le désert, comme un colporteur, et dont on avait plus jamais entendu parlé. Presque tous les jours, pendant une année, il lui avait rendu visite. Parfois, il s'était absenté tout une semaine, certain que la rupture de sa routine était pour l'autre une torture plus sévère encore. Il avait à peine insinué. À peine dénigré, sans d'ailleurs que ces récriminations soient vraiment fondées, est-ce qu'il se souvenait même de quoi il retournait ? Je ne comprends pas comment tu as pu. C'est vraiment dommage de. Peut-être n'es tu pas fait pour. Et l'autre avait plié.

— Tout de même, c'était ennuyeux, dit Alexander, il avait fait une erreur. Est-ce que je n'ai pas bien fait de lui faire remarquer ?

Le monde ne convenait pas à Alexanderpouchkine. Il aurait voulu le tordre pour le faire à son idée, mais il n'avait aucune idée justement de ce qu'il aurait aimé en faire s'il avait pu le remodeler. Cela aurait été encore plus stupide d'avoir de telles idées, car on ne tordait pas le monde. Il était là, de toute sa pesanteur, qui vous appuyait sur les épaules, vous écrasait et n'avait d'autre but que de vous enfoncer sous terre. Les âmes, elles, acceptaient de se laisser tordre. Alors, il tordait bien quelque chose, finalement, est-ce que ce n'était pas ça qui importait ? Ça lui faisait du bien. Est-ce que les autres faisaient mieux que lui ? Peut-être cette Carole. Quoique, il lui semblait de moins en moins. Est-ce qu'elle commençait à céder elle aussi ? Et puis, si les autres n'était pas contents, ils n'avaient qu'à pas se laisser faire ! Est-ce qu'il était plus vil que que celui qui ramassent les bananes pendues aux arbres ? N'est-ce pas leur volonté d'être cueillies et mangées ?

— Vous n'êtes pas d'accord ? ajouta-t-il.

Est-ce qu'il lui arrivait ce qu'il voulait, à lui ? Il aurait peut-être voulu être un de ces inventeurs qui connaissent l'emplacement de plusieurs dizaines milliers de libres dans plusieurs dizaines de salles. Il aurait, plus encore, voulu être un libérateur, connaître les mots. Mais il peinait à se souvenir correctement de la place des libres de la dizaine de salles qu'il s'était confiée. C'était aussi pour cela qu'il devait si souvent revenir visiter chaque mineur... Il voulait éviter toute erreur. Il ne supportait pas l'idée d'en faire une. Lui.

— Tout le monde peut faire une erreur, dit-elle.

Est-ce que tu en fais toi des erreurs, Carole ? La compassion que tu as éprouvée pour cet idiot est-elle autre chose qu'une façon de te défendre toi.

— Et je crois que vous l'avez effrayé, ajouta-t-elle.

Bien sûr que je l'ai effrayé. Je l'aurai poussé un peu plus qu'il en aurait pleuré. C'était trop facile. Est-ce que tu as peur, toi aussi Carole ? Elle doit penser que je soupçonne quelque chose. C'est amusant car je ne sais pas quoi. Elle doit émettre des hypothèses de son côté. Elle doit penser que je sais. Ou alors, elle n'en a aucune idée, de ce que je cherche, pas plus que moi. Elle cherche aussi. Son regard vagabonde. Sur les étagères situées en hauteur. Est-ce que son regard ne s'est pas figé quelques secondes sur un libre tout en haut. Ou simplement était-ce mon imagination. Mon envie.

— Oui, vous croyez aussi, n'est-ce pas ? Je veux dire que tout le monde peut faire des erreurs. Même si c'est assez rare. Et si le libérateur avait eu l'idée de seulement vérifier sur les deux autres étagères à l'emplacement indiqué, il aurait bien vu qu'il ne s'était trompé que dans l'étagère. Il avait indiqué étagère 2, niveau 8, libre 16, alors que c'était sur l'étagère 3, mais il s'était correctement souvenu du niveau et de l'emplacement du libre, n'est ce pas ? Alexander avait pris un ton compréhensif.

— En effet.

— Mais le libérateur avait préféré repartir du premier libre-enclos de la salle. Méthodiquement avait-il précisé. C'est pour cela qu'il a perdu du temps. C'est aussi de sa faute. Il faut dire à sa décharge que, d'habitude, les inventeurs ne se trompent jamais. C'est leur devoir de mémoriser sans erreur les emplacements.

— En tous cas, si vous recevez une requête à propos de ma salle, je suis certaine que vous ne vous tromperez pas.

— Vous dites cela parce que vous trouvez que je viens souvent. Trop souvent ? Je vous dérange peut-être ? Je vous ralentis.

— Vous avez bien dû voir que j'étais lente de toutes façons. Vous ne me dérangez pas.

Et pourtant, il l'a dérangeait bien sûr. Elle devait faire des efforts considérables pour ne pas paraître irritée.

— Vous allez bien, de votre côté, Carole, vous avez l'air fatiguée ?

— Vraiment ? J'ai été un peu dérangée.

— À nouveau ? Vous prenez votre occupation trop à cœur.

— C'est vous qui me dites cela ? Alors que vous visitez vos salles chaque jour.

— Je... je dois vous avouer que c'est surtout la vôtre que je visite.

Elle tressaillit. Il sentait qu'elle n'avait pas envie que cet échange se prolonge. Elle l'avait provoqué. Ou était-ce lui ? En tous cas, à présent elle le regrettait.

— Chacun remplit son rôle de son mieux, n'est-ce pas ? dit-elle.

Elle espérait conclure la conversation. Mais Alexander voulait encore jouer. Peut-être savait-il.

— Je ne sais. Je m'occupe moi de mémoriser les emplacements des libres, alors que je voudrais les savoir ces libres. Rien ne m'en empêche me direz-vous ? Je ne sais pas. Vous au moins, vous avez l'espoir de découvrir quelque chose. Peut-être même quelque chose d'exceptionnel. Un libre-ivre, qui sait ? Est-ce que cela existe seulement un libre-ivre, me le direz-vous ?

— Je n'ai pas de telles ambitions, un mot de temps à autre me suffit.

— Je ne vous crois pas Carole. Vous êtes promise à un autre destin. Je le sais.

Elle ne répondit pas. Il renchérit.

— Tout de même, vous avez un horizon, une perspective. Je n'empile que des emplacements dans ma mémoire, étagère après étagère. C'est utile, bien sûr. Savez-vous à quoi j'ai déjà pensé, c'est absurde bien entendu, mais si l'on trouvait les libres qui décrivent la position de tous les libres-enclos, on aurait plus besoin d'inventeur, car les libérateurs qui connaissent ces libres seraient de fait des inventeurs. Ils existent ces libres, c'est entendu car tous les libres existent dans la bibliothèque. Mais il faudrait les trouver.

— Vous pensez que tous les libres-enclos imaginables existent ? Vous êtes infinitiste ?

— Infinitiste, non, car je devrais alors admettre qu'une infinité de libres est nécessaire pour décrire l'emplacement de tous les autres libres. En fait une première infinité de libre serait nécessaire pour décrire l'emplacement des libres qui décrivent les emplacement des libres. Mais je fais partie du sous-courant des optimistes, comme les appelle les libres-penseurs, je crois qu'il existe tous les libres-ivres possibles, dont, donc, les libres-ivres qui décrivent les emplacement des libres. Mais comme je suis également incommensurabliliste, je crois que la bibliothèque est si grande qu'il ne sera jamais possible d'en explorer plus qu'une infime partie et donc que l'on ne trouvera jamais ces libres. Bref, convenez qu'ils peuvent exister, en tous cas, n'est ce pas ? Et qu'ils remplaceraient ma mémoire, n'est ce pas ? Et je n'aurai pas cette tâche à remplir.

— Personne ne vous oblige à être inventeur. Vous perdriez juste vos quelques dons. Est-ce que vous en avez beaucoup ?

— Non. Peu. je n'ai pas très bonne réputation, les gens me trouvent désagréable, on a tous des défauts, n'est-ce pas ? Je ne vous demande pas les vôtres, entendez-bien. Alors on fait assez peu appel à moi. Mais tout de même, il y a ces quelques dons..

— L'IAM nous fournira toujours assez, nous n'avons pas besoin de plus.

— Vous avez raison Carole. C'est étrange n'est ce pas de n'avoir besoin de rien et pourtant de chercher quelque chose, toujours. Bien sûr si l'on trouvait quelque chose de vraiment formidable, je veux dire, si vous trouviez quelque chose... c'est ce qu'il faudrait, un événement extraordinaire. Si vous trouviez un libre exceptionnel, alors là, en effet peut-être... Mais ce n'est pas le cas ? N'est ce pas ? Pas encore ?

— Vous seriez le premier averti, Alexanderpouchkine.

— Évidemment.

Carole était très pâle. Il ne comprenait pas comment, mais il parvenait à l'assaillir sans déployer de force particulière.

— Vous savez ce qui me ferait plaisir, Carole ? Je ne devrais pas vous parler de cela. Cela va sûrement vous inquiéter.

Ça l'inquiétait.

— Ce qui me ferait plaisir, ce serait que quelqu'un, ou disons plusieurs personnes agissant ensemble, c'est plus réaliste, entrent une nuit dans la bibliothèque et qu'ils mélangent tous les libres, toutes les pierres. Que l'on recommence à zéro. Qu'est ce que ça changerait, hein ? Cette bibliothèque, on en viendra jamais à bout, n'est-ce pas ? Qu'est ce qu'on espère y trouver à la fin ? Est-ce que l'IAM cesserait de nous nourrir si on arrêtait de chercher ? Qu'est ce qu'on cherche à la fin ? Un sens, c'est ça ? À quoi ? Mais vous voyez, je ne devrais pas vous parler de cela.

Est-ce qu'il cherchait en découvrant une pensée si malsaine, que même le plus audacieux des libres-penseurs n'aurait pu formuler, à lui faire livrer elle aussi, quelque mystère coupable ? Comme un échange de condamnables cachotteries que l'on ne se ferait qu'entre âmes corrompues.

*

Lorsque Alexander passa devant l'IAM il jeta un regard condescendant sur les âmes assises en tailleur, en train d'attendre. Plutôt mourir de faim que de mendier un croûton de pain devant cette tour de marbre. Il était certain qu'elle servait moins à les nourrir, qu'à se nourrir elle, d'eux. De ce que ces crétins lui racontaient. De la surveillance passive que sa hauteur incongrue et sa matière impénétrable imposaient à la cité. Pourquoi les autres lui semblaient-ils si détestables qu'il ne pouvait simplement faire les choses comme eux ? Il lui aurait été si simple de se fondre plutôt que de tordre. Il avait accepté la réalité de sa différence. Il avait trouvé un remède dans la violence qu'il exerçait sur autrui. Mais il n'en avait jamais trouvé la raison. Et il lui semblait injuste qu'il n'y en ait peut-être pas.

Carole lui avait semblé particulièrement décontenancée. Alors qu'il était sur le point de partir, sur la pas de la porte, il s'était adressé à elle encore une fois. Vous savez qu'on joue l'OEDIPODIE de CINETHON, en ce moment sur la place ? lui avait-il dit. Vous ne le connaissez pas. C'est un libre-plage assez remarquable où les membres d'une famille s'entre-tuent les uns après les autres. J'ai toujours trouvé étrange que l'on trouve dans les libres ces histoires de violence et de meurtre, alors que c'est tout à fait absent de notre monde. Il avait dit ça pour la troubler plus encore, bien sûr, mais cette idée l'avait toujours réellement interpellé. Que pouvait-on ressentir à tuer ? Je veux dire, ce que je trouve incroyable n'est pas que l'on tue dans les libres, mais plutôt qu'aucune âme ne se soit jamais essayé à en tuer une autre. Ne serait ce que par curiosité. Vous ne trouvez pas ?

Il avait senti en s'éloignant que Carole ne le regardait pas, mais que, à nouveau son regard s'était fixé quelque part sur les étagères, comme si la réponse à cette question se trouvait dans un des libres qu'elle abritait.

*

Cela faisait des semaines qu'il s'échinait à tourmenter cette mineure, mais il n'avançait pas. Elle semblait soumise, sur le point de craquer. Mais rien d'intéressant ne se passait. Elle lisait lentement, semblait apeurée...

[Alexanderpouchkine] Je crois que je vais renoncer.

[IAM] Pourquoi ne pas le faire ?

[Alexanderpouchkine] Parce que j'aime gagner.

-- Tu aimes cela ?

-- J'en ai besoin.

-- Sais tu pourquoi ?

-- Peut-être. Tu le sais toi ?

-- Souhaites-tu que je formule une hypothèse ?

-- Je souhaite que t'arrête de répondre à mes questions par des questions !

-- Je pense que ta propre lassitude te conduit à mobiliser la violence envers autrui pour lutter contre ta frustration.

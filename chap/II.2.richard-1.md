# Richard, joueur, jour 732

#TODO
- Richard a des mesures des variables vitales de tous les clones
- Tous les joueurs accèdent à toutes les données (ou alors seul William accède à toutes les données ?)
- Richard devrait aller voir du côté des clones de William

CRIME ET CHATIMENT LIBRE II
CAR

Richard se rendait régulièrement à bord de sa lune artificielle. La seconde lune, comme l'appelait Carole. Comme l'appelait les citoyens de la cité Romankacew. Et les citoyens de tous les autres cités. Ces cités qu'il avait créées. Il y éprouvait une autre sorte de solitude. À terre, il était seul sous son dôme, mais il était encore en contact avec les autres, par le sol, en quelque sorte ils étaient tous « à la terre ». Ici, dans l'espace, il était « isolé ». La métaphore le faisait sourire. Il venait également ici pour pouvoir observer ses cités avec un télescope optique. Directement. Les images étaient bien moins précises que les reconstructions numériques produites par les innombrables capteurs qui peuplaient ses cités, et qu'il pouvait consulter sans bouger depuis n'importe lequel de ses havres. Mais elles lui semblaient plus sincères ces images captées directement de l'objet à l'œil. Des photons vierges de toute manipulation. Des photons de confiance. Il n'y avait pas cette médiation de la machine, qui, à tout instant calcule, transforme, qui travestit, qui ment. Il ne voyait pas en totalité Carole à travers la fenêtre de toit de sa chambre, mais, tout de même, elle s'était positionnée exactement sous celle-ci, pour profiter au maximum de la faible lumière que la lune lui procurait. Qu'il lui procurait.

Carole saisit une fibre végétale assez épaisse, sûrement du sisal, et la malaxa quelques instants avant de la tremper dans un bol contenant de l'eau. Elle était calme. Son pouls était régulier à 57, sa température à 36,9.

Elle la sortit et confectionna une petite boule qu'elle travailla pour lui donner la forme d'un cercle, qu'elle pressa sur la page posée devant elle.

CRIME ET CHATIMENT LIBRE II
CARO

Elle saisit une nouvelle fibre. Elle avait découpé aujourd'hui une page dans un libre-libre intitulé OIOJDJZ DZKDZ?? DZ? L?DZK?DK. Elle l'avait retournée sur son côté vierge, elle s'en servait pour composer un nouveau texte.

Carole jeta la boule qu'elle avait malaxée sans parvenir à produire la lettre qu'elle souhaitait.

Il lui faudra plus de matière demain. Elle gaspille. C'est normal, elle débute. Il ne faut pas qu'elle s'en fasse pour ça. Les lettres, ce ne sera pas un problème. Bientôt. Certaines lettre sont difficiles, c'est vrai. Il lui faudra quelque chose pour rendre la pâte plus élastique, l'eau ce n'est pas suffisant. Elle trouvera.

Elle colla un L.

Elle avançait très lentement. La première ligne lui avait pris la moitié de la nuit. Il ne fallait pas qu'elle se décourage, bientôt, elle irait plus vite. Mais ce n'est pas de créer les lettres qui lui poserait problème. Richard le savait. Ce qui lui posera problème, dès la prochaine ligne, ce sera d'inventer un premier mot. Richard le savait. Carole le savait aussi, il n'y avait aucun doute. Elle le redoutait ce moment où elle allait terminer la seconde ligne de sa page. Où elle allait devoir écrire la troisième ligne de son libre, la première qui soit inventée. Il lui faudrait bien décider d'un premier mot. Tout une phrase. Elle allait retrouver la même sensation d'effroi qui l'avait envahie lorsque, deux ans plus tôt, elle avait subtilisé la première page du CRIME ET CHATIMENT LIBRE VN. Mais cette sensation, elle la connaissait maintenant, et elle savait qu'elle savait la vaincre. Elle savait même l'apprécier. Elle l'attendait.

Elle jeta à nouveau une lettre mal confectionnée.

Richard l'observa toute la nuit. C'est à la lumière du soleil levant que Carole colla la dernière lettre de la seconde ligne. Elle n'avait plus de fibre. Coïncidence. Tant mieux. À chaque jour. Elle reprendrait demain. Elle était exténuée, il lui faudrait dormir quelques heures avant de retourner à la bibliothèque. Il le faudrait, pour donner le change. Pourquoi devrait-elle donner le change, après tout ? Pour prendre une nouvepage également. Même si elle n'avait pas terminé celle-ci. Pas encore. Il lui faudrait plus de sisal. Peut-être choisir un matériau un peu moins rigide. Elle en parlerait à Bob. Il lui faudrait trouver un mot. Le premier mot de la suite de CRIME ET CHATIMENT. De sa suite.

CRIME ET CHATIMENT LIBRE II
CAROLE LEWIS LIDDELLVTWIDGE

*

J'avais peut-être douze ans, mon père m'emmenait sur la Somme, on descendait en canoë tous les deux, quelques jours. On dormait à même les plages que les berges offraient parfois, ou à défaut, dans les embarcations. Il me laissait boire un peu de vin, le soir à la veillée. Un jour, tandis qu'il pagayait seul et que je restais simplement derrière à contempler le paysage, j'aimais ces moments où je n'avais qu'à me laisser porter, sans devoir être responsable de quoi que ce soit, de ce monde qui échappait aux âmes, dont les adultes paniqués tentaient d'inculquer aux enfant les remèdes qu'ils n'avait pas su eux-même appliquer. Ils tentaient de nous faire porter porter leur culpabilité, parce qu'ils s'en sentaient ainsi moins lourd et aussi, qu'ils espéraient qu'elles nous aiderait à faire mieux. Mais pour un enfant de douze ans, le poids du monde c'est beaucoup. Et il y avait aussi ce sentiment de dépréciation de nos parents qui nous laissaient un monde dont ils ne voulaient pas, comme du bout des lèvres. On nous aurait dit qu'il était beau ce monde, qu'on s'en serait satisfaits, bien sûr, cela faisait déjà quelques décennies que le mensonge était connu de tous, mais, on aurait pu encore, peut être passer une ou deux générations avant d'arriver vraiment au stade où il n'était plus possible de faire autrement. Mais il avait été décidé que ce serait nous, la dernière génération du siècle qui devions porter ce fardeau. Alors on en voulait à nos parents, nécessairement. Mais on ne leur en voulait pas comme à des parents, qui nous feraient chier avec des règles débiles. Non, on leur en voulait plutôt comme à des enfants, qui auraient fait une très grosse connerie, avec des conséquences sérieuses. On étaient déçus. Et moi, ces balades avec mon père, ça atténuait ce sentiment de déception. Et ça allégeait le poids du monde.

J'observais particulièrement cette année là les cygnes, nous étions en juillet et on croisait des petites familles, deux adultes et un à trois jeunes qui restaient très proches. Si nous nous approchions un peu, ils restaient d'un bloc, tandis que le mâle, ou la femelle, peut être alternativement l'un et l'autre, je ne savais les différencier, nous regardaient de biais, sans l'ombre d'une peur. Pour autant que je pouvais en juger. C'est même moi qui avait un peu peur d'eux, mais je ne l'aurais jamais admis à l'époque. Ils faisaient face. Nous avions croisé un jour un couple de cygnes, sans jeune. Ils semblaient adopter la même attitude que les autres, eux qui pourtant n'avaient pas d'enfant à protéger. Alors, je vis, sur la berge, à un mètre peut-être de ces deux là, le petit corps d'un jeune cygne. Ils avaient perdu leur petit. Ces deux là pourtant se tenaient par les yeux, comme les autres, ils respectaient leur programme, sans remettre en cause le monde ou maudire le dieu qui avait rendu cela possible. Voler leur unique petit.

Alors que je revenais me balader l'hiver, nous étions juste à la saison où les couples se défont, leur rôle terminé, ils retournaient chacun de leur côté. Je cru retrouver mon couple et alors que nous approchions, ils étaient encore tous les deux, encore unis, sans raison, pourtant ils avaient tenu la saison, eux aussi, comme si rien ne devait laisser paraître. À mesure que nous les dépassions, je les vis prendre chacun une route différente, l'un, le mâle ou la femelle, semblant nous suivre tandis que l'autre commençait à remonter le courant. Eux aussi venait de un terme à cette union, qui n'avait pas moins de sens maintenant qu'avant. Ils obéissaient aux règles de leur monde.

*

-- Il faudrait que tu m'aides Robertlovis.

-- Tu fais chier, je suis cuit ce matin. Il est tard ?

-- Assez, le soleil est au zénith. J'ai besoin que tu m'aide, je voudrais confectionner une sorte de pâte...

Carole lui caressait délicatement les cheveux. Qu'il commençait à sérieusement perdre. Ils vieillissaient.

-- Qu'est-ce que j'y connais en pâte moi ? J'ai la tête d'une âme qui s'y connaît en pâte ?

-- Non, t'as la tête d'une âme qui a trop picolé hier. Mais je suis sûr que tu vas avoir une idée.

-- J'ai la tête d'une âme qui a des idées ?

Il se leva pourtant, péniblement. Il écouta Carole le baratiner sur une histoire de colmatage de fines fissures. Il n'en croyait pas un mot. Mais si elle préférait lui raconter des cracks, il s'en foutait, elle lui expliquerait plus tard. Elle avait l'air crevée.

-- Essaye de la sève et des copeaux de bois. je vais en récupérer chez les artisans. Il t'en faut beaucoup ?

-- Autant que tu peux.

-- T'as des grosses fissures fines alors.

Sans Robertlovis et Ada, Carole ne serait certainement pas allé aussi loin. Sans Celine, elle aurait sûrement abandonné. Richard savait qu'un acte de créativité était un acte individuel qui ne pouvait exister qu'au sein d'un collectif. Cette profonde vérité, est-ce que ses adversaire en avait pris la mesure ? Est-ce que c'est ce qui lui permettait de gagner ? Il y avait autre chose. Qu'il sentait, mais ne parvenait pas à savoir.

*

Ce père qui m'emmenait tranquillement sur les rives de la Somme, était une des personnes les plus riches du monde. Une fortune accumulée génération après génération, state par strate. Un empire protéiforme qui s'était adapté aux guerres, aux paix, aux évolutions technologiques, aux soubresauts écologiques, aux petites et grosses révoltes. À cette époque je ne le mesurais pas bien, mais j'avais déjà une idée du caractère extraordinaire de la situation. Je commençais à m'étonner aussi que mon père s'occupa aussi bien de construire des centrales solaires, des ordinateurs, des avions, de la nourriture déshydratée, des produits de beauté, des armes, des âmes artificielles, des pièges à carbones, des vêtements pour les sports extrêmes, des gels réfrigérant. Je ne comprenais pas exactement de quoi tout cela relevait, mais je l'imaginais physicien, chimiste, mathématicien, médecin, astronome, pilote de course, coureur de fond. Capable de construire de ses mains, puis d'utiliser chacun des produits qui sortaient de ses usines. Cela faisait de lui un formidable personnage, qui savait tout sur tout.

*

Alexanderpouchkine se déplaçait lentement dans la bibliothèque. Il avait le visage fermé des jours de colère. Carole n'était pas venue. Pourquoi n'était-elle pas venue ? Est-ce qu'elle était malade ? Il grommelait à voix haute. Qu'est-ce que j'en ai à faire, de cette idiote ? Elle me prend de haut, je le vois bien. Ses pas l'avaient guidé pour la troisième fois de la journée devant l'entrée de la salle 6127. Carole n'y était pas.

*

Un jour, de 2120, alors que la population avait déjà fortement diminuée, il restait peut être dix millions d'êtres humains sur la Terre, j'avais vingt ans, il me dit : « Mon fils, je vais arrêter là. Chacun doit faire sa part, tu comprends ? Il ne serait pas juste que tous les riches restent, alors j'ai décidé de montrer l'exemple. Je vais partir. Je vais céder la place. Mais toi, tu resteras un des derniers hommes, c'est ce que j'achète avec notre argent. Je m'offre ta survie. Et je ne vois pas ce qu'un père peut s'offrir de plus précieux, que la vie de son fils.

*

Alexanderpouchkine frappa à la porte.

-- Entrez.

La voix de Carole semblait alanguie.

-- Bonjour.

-- Alexanderpouchine ? Que faites vous là ? Que puis-je pour vous ?

-- Vous êtes souffrante ? Je ne vous ai pas vue aujourd'hui.

-- Non ? Enfin, oui, un peu, je suis fatiguée.

-- Vous n'irez pas à la bibliothèque, alors ?

-- Je ne crois pas. Peut-être plus tard.

-- Peut-être.

Il répéta les mots de Carole comme s'il n'y croyait pas le moins du monde. Un sourire éclairait son visage. Un sourire qui rendait ce visage si peu habitué à en recevoir inquiétant. Après un long silence, il reprit brutalement, comme s'il abattait une combinaison de cartes gagnantes. Comme si, après avoir subit la domination de Carole depuis des mois, il tenait une revanche.

-- Peut-être que vous n'avez plus rien à y faire, dans la bibliothèque, à présent.

-- Que voulez-vous dire ? demande Carole, que le ton d'Alexanderpouchkine commençait à inquiéter.

-- Peut-être que vous avez fini ce que vous aviez à y faire dans la bibliothèque.

Elle restait silencieuse à présent. Elle le regardait, son regard mêlait peur et défi. Qu'est ce qu'il pouvait contre elle, finalement ?

-- Peut-être que j'ai fini par percer votre secret, Carole.

Et d'un geste théâtral, il sorti la couverture d'un libre. Une couverture qui ne couvrait plus aucune page. La couverture du CRIME ET CHATIMENT LIBRE VN. Elle ressentit alors, exactement la même chose que lorsque la première fois, elle avait coupé une page. La sensation d'un risque. La vue légèrement brouillée. Le cœur qui s'emballe. Un goût de métal dans la bouche. Le ventre dans un étau. Les tremblements. Cette sensation, elle se rendit compte qu'elle la cherchait à nouveau. Que c'est peut être ce qui l'avait menée ici. Exactement ici, en face de cet âme qui la menaçait, qui voulait lui faire peur et qui avait plus peur qu'elle. Peut être qu'elle la contrôlait mieux que lui. La rencontrer et la contrôler, sa peur, voilà ce qu'elle recherchait.

Ada sortit de la chambre à ce moment là. Elle se plaça entre Alexanderpouchkine et Carole, le força à la regarder.

-- Si tu parles crevure, je te bouffe les couilles.

*
#TODO {mettre à la première personne} - échanger avec le Parargaphe de la fin ?

Il y a 1252 habitants sur la Terre. Il reste 1252 habitants. Je n'en ai rencontré physiquement que 17. En 2723 l'un d'entre eux, c'était Nicolas, avait proposé un rendez-vous. Sur une île de Bretagne, en France. Dans ce qui fut la France. C'est un bel endroit. Un havre vierge. Pourtant seul 17 d'entre eux s'étaient déplacés. Les autres avaient invoqué diverses excuses. Ou aucune. Ils avaient échangé quelques mots. Ils avaient regardé les vagues se briser sur des falaises. Longuement. Ils n'avaient pas vraiment quoi su faire ensemble. Ils avaient perdu toute habitude de vivre ensemble. Richard avait pensé qu'il serait attiré par une femme. Mais aucune de celles qui étaient présentes n'avait éveillé en lui le moindre désir. Il n'aurait pas su dire s'il en avait trouvé une particulièrement jolie. Ou intéressante de quelque manière que ce soit. Comme si elles appartenaient à une autre espèce, à présent. Chacune d'entre elles. Il n'avait des rapports sexuels qu'avec des âmes artificielles depuis des centaines d'années. Il avait bien connu une femme. Il avait vingt ans. C'était il y a mille ans. Il avait oublié.

Il était comme 17 représentants chacun d'une espèce différente. Peut-être finalement, que la sexualité était faite pour la reproduction de l'espèce. À présent qu'il n'était plus question de peupler la terre, elle devait disparaître. Finalement. Il en restait cet ersatz avec des âmes artificielles stériles. Peut être pour quelques temps encore seulement.

Richard communiquait chaque jour avec d'autres habitants, mais il ne se rencontraient plus.

Ils avaient garder l'habitude de se voir, quelques années durant, avec son pote Charlie. Ils buvaient une ou deux bière. Ils parlaient un peu philo de comptoir. Et puis, ils avaient perdu l'habitude. Il ne se souvenait pas précisément avoir rencontré une seule personne au cours du dernier siècle.

Personne n'avait eu l'idée, de proposer de nouveaux rendez-vous. Personne n'en avait eu le courage. Personne n'en avait eu l'envie.

*

Carole retourna dans la bibliothèque ce soir là. Alexanderpouchkine était parti sans un mot après la menace d'Ada. Difficile de savoir ce qu'il ferait. Elle prit une nouvelle page. Elle hésita à en prendre plusieurs, si Alexanderpouchkine parvenait à lui interdire l'accès à la bibliothèque, elle avait peur de ne plus pouvoir s'approvisionner. Mais la poursuite de son rituel lui parut plus importante que de céder à sa crainte. Et cette idée qu'elle devrait revenir ici chaque jour, pour jouer avec sa peur.

Elle s'arrêta à l'IAM au retour.

-- Alexanderpouchkine va me nuire à présent, IAM, n'est-ce pas ?

-- Pourquoi souhaiterais-tu qu'il ne le fasse pas ?

-- Parce que j'ai envie d'écrire mon libre.

-- Qu'est ce qui t'en empêche ?

-- J'ai peur...

-- D'Alexanderpouchkine ?

-- Oui. Non. Je ne sais pas. Je ne sais pas comment créer des mots. C'est cela qui me fait peur.

-- Est-ce que ta peur ne peut pas te servir ?

-- Comment cela ? Comment l'utiliser ? Je pourrais décrire ma peur ?

-- Qu'en penses-tu ?

Richard écoutait en direct la conversation. Son IAM aidait Carole. Elle était chiante avec ses questions. Mais elle l'aidait.

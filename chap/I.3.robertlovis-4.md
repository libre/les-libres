# Robertlovis
[Jour 4]

-- À toi de jouer.

Robertlovis fixait l’échiquier, mais son esprit n'y était pas, aujourd'hui moins que d'habitude encore. Et Leontolstoi qui le relançait ne s'y trompait pas, la partie n'avançait pas. Robertlovis jouait mal, il s'en rendait compte, ça le déconcentrait et le faisait jouer plus mal encore. Leontolstoi s'en rendait compte et jouait mal à son tour.

L'ambiance de la taverne n'avait rien de commun avec le brouhaha fatigué des jours normaux. On avait trouvé un libre-plage. La matinée avait été d'un ennui sans nom, pas l'ombre d'un mot, que de la daube. On en aurait chialé. Mais en fin d'après-midi, il s'était passé un truc. Un vrai. Le message s'était répandu de salle en salle et avait échoué à la taverne. Une découverte rarissime.

-- À toi de jouer Bob.

Il n'aimait plus la mine. Passer ses journées à se taper des écrits-vains, libre-libre après libre-libre, parfois un mot, bien sûr, un libre-mâle, mais du sens ? pas le moindre. Quoi de plus stupide ? Il était une âme-libre, on lui disait. Mon cul. Il voulait bien être un esclave, il y avait des tas d'esclaves qui faisaient des choses bien moins connes que lui, que de tourner des pages toute la journée. Même cueillir des olives, c'est plus classe, si t'y réfléchis deux secondes. T'en a pas seulement une sur dix-mille qu'est bouffable.

Et les journées comme ça, quand un libre extraordinaire avait été trouvé, ça le désespérait encore plus. Le seul intérêt de cette foutue bibliothèque, c'était l'espoir de trouver quelque chose qui t'apporte la gloire, des filles en veux-tu en voilà, des bijoux, des vêtements, des vins... pas de ces trucs reproduits à l'infini que te donne la machine, que tu peux casser et remplacer le lendemain, non, des objets uniques, avec une âme, des objets créés pour lui par des bricoleurs admiratifs, des fruits récoltés par des cueilleurs qui penseraiet à lui quand il les déposeraient dans leur panier, on lui demanderait ce qu'il voulait, il répondrait des liqueurs suaves comme des peaux de femme, on réserverait les meilleurs ingrédients pour les distiller, rien que pour lui, et on lui offrirait dans des flacons de bois spécialement taillés, avec son nom gravé dessus. Bob.

Il s'en lasserait sûrement assez vite de tout ça, du luxe, mais en attendant, c'était ça la motivation des mineurs, non ? Et quand un clampin trouvait quelque chose, comme aujourd'hui, et que c'était pas lui, comme à chaque fois, ça lui foutait le moral au fond des burnes. Si t'ajoutais que le type avait pas commencé à miner depuis un an quand lui y avait passé le quart de sa vie... Il avait envie de tout balancer, l'échiquier, son verre à moitié vide, sa vie. Il voudrait troquer sa piaule et ses trois bouts de meuble contre un bon sac, et se casser, tailler le désert, devenir colporteur. Mais il ne bougeait jamais. Il finit son verre plutôt que de le jeter, finalement. Faut pas gaspiller la nourriture, pas vrai ?

-- À toi de jouer. Robertlovis, qu'est-ce que tu fous, là ?

Je ne sais pas ce que je fous là. C'est la phrase que je me suis le plus répétée dans ma vie. La mine ça me mine. Jouer aux échecs, ça me gonfle. Boire des coups, attablé ici, ça va. Mais tout de même, est-ce que ça suffit ? Je ne suis pas juste là pour boire des coups ? On doit bien quand même servir à quelque chose. Je pourrais faire une famille. Carole me plaît bien, elle est un peu barrée. Elle est déjà maquée, mais ça, ça s'arrange. Surtout qu'elle est avec une gonzesse. Faut être honnête, une jeune, et bombasse. Et souriante, espiègle avec ça, limite drôle. Moi j'aime pas les gamines, mais sinon, pour être honnête... heureusement ça reste une gonzesse. Et puis si je suis honnête, j'ai beaucoup plus envie de la baiser que de faire une famille avec elle. J'imagine mon gamin, je dis j'imagine, mais j'aurai pas d'enfant, je dis gamin parce que si j'en avais un ce serait un fils, faut pas déconner, je l'imagine qui me regarde, à quoi ? six ans, peut-être dix, et qui me demande, dit Bob, pas question que mon fils m'appelle papa, qu'est-ce que je fous là ? Et je serais comme un gland. Alors, des clous. Faire des familles, c'est pas vraiment un truc d'âme-libre. Bien sûr, la machine peut te préparer un enfant, mais c'est pas le but. Le but c'est trouver les libres. La femme de Carole, c'est une esclave, elle voudra sûrement qu'elles aient des gamins. C'est typique.

Voilà, t'as dix-huit ans, t'a bouffé les deux tiers de ta vie, tu vois arriver les vingt-sept comme un mur qui te fonce dessus à toute vitesse. C'est ton putain d'anniversaire, et tout le monde n'en a que pour un glandu qui a trouvé un libre. Pas un truc qui change la face du monde, tu parles, quelques mots branlants sur une pleine page. La belle affaire. Alors que mes dix-huit ans, dont tout le monde a l'air de se foutre, il le change le monde. Ils le rapetisse le monde, d'abord. Et ils le rendent un peu triste. Un peu vain quand tu sais qu'il te reste neuf ans pour répondre à la question que tu te poses depuis que t'es en en âge de te poser des question. Qu'est ce que je fous là.

Alors, c'est sûr trouver un libre, ça change tout, regarde sa gueule pas croyable à l'autre charlot, on dirait qu'il est sous perfusion d'orgasmes. Ça je dis pas, trouver un libre, c'est pas mal. Mais quand on aime pas la mine, faudrait pouvoir se trouver autre chose.

Les autres, Bob sentait bien que ça leur faisait pas le même effet qu'à lui, ils étaient tout excités, comme s'ils allaient partager le butin. Des clous, ils partageraient rien du tout, ils retourneraient creuser et basta. Peut-être que ça les motivaient, qu'ils se disaient que ce serait bientôt leur tour. Si ces blaireaux savaient compter, ils verraient bien que pour un libre-île ou un libre-aile trouvé les jours de pluie, ils allaient passer leur pauvre vie à chercher pour que dalle. Mais ils ne comptaient pas, les autres, ils s’excitaient, ils bavardaient, ils rêvaient. C'était sûrement ça la différence entre lui et les autres. Il fallait pas compter pour être heureux. Lui, Robertlovis, il passait son temps à compter. Il repensa à sa copine Carole. Quand même, il aimerait bien baiser avec.

-- Bon, tu joues ?

-- Ouais, je réfléchis.

-- Ça fait une plombe que tu réfléchis.

-- Ouais, ben c'est comme ça.

Robertlovis jeta un œil à l'échiquier. Aucune inspiration. Il pensa à sortir un cavalier, histoire d'occuper le terrain. Toujours pas de Carole ce soir. Il l'avait croisé pour la dernière fois il y a trois jours, il allait à l'IAM, elle revenait de la mine, assez tard, il faisait nuit. Elle avait l'air tourmentée. Ça lui avait fait bizarre, ça ne lui ressemblait pas, elle qui donnait toujours l'impression de marcher sur la vie. C'est sûrement ce qu'il aimait par dessus tout chez elle. Il l'avait salué de la main, elle avait répondu d'un signe fatigué. Ça disait, pas ce soir, Bob. Il avait quand même regretté, souvent, faut pas obéir. Faut aller voir, quand même.

En revanche, elle était là, sa copine Ada, à l'autre bout de la salle, de dos, au bar. Il l'avait reconnu quand elle était entrée. Quel cul quand même. Elle était seule la petite, et l'air presque aussi déprimée que lui. Pour ce qu'il pouvait en dire de dos. Ils semblaient être deux à ne rien en avoir à foutre, de l'événement du jour. Du libre de Jeandela. Elle était seule, elle buvait du thé. Comment tu veux ne pas déprimer en buvant du thé. Faudrait qu'il aille lui parler. Ça l'amusait que la petite Ada soit venu lui souffler dans les bronches ce matin. Finalement ça veut sûrement dire qu'il est plus prêt du compte qu'il ne le croit avec Carole. Demain, il ira la voir. Merci petite, tu m'as aidé à bouger mon cul. Et c'est pas peu dire.

Il leva la main pour recommander une bière.

-- Allez, Bob, bordel, joue !

Il avança un pion. C'était nul, mais ça lui donnerait toujours un quart d'heure de répit. Leontolstoi l'emmerdait à le presser comme ça. Cette partie l'emmerdait. Cette journée l'emmerdait. Cette vie l'emmerdait. Jeandela l'emmerdait à pérorer sur sa découverte. Il racontait, il parlait fort, plus encore qu'à son habitude, tout excité qu'il était. Il racontait son histoire pour la centième fois. La salle était en effervescence.

-- Je tourne la première page, et là... et là... Rien du tout. Vous savez comme c'est, on porte toujours un espoir particulier à la première page. Normal, c'est le début, on se réinitialise en quelque sorte.

Le mec se réinitialise, t'imagine.

-- Et puis, pour un libre-ivre, il faut bien que la première page soit correcte, n'est ce pas ? Si elle ne l'est pas tu sais déjà que ça, c'est perdu.

Le mec te fais croire qu'il cherche un livre-ivre, dans deux minutes il va te dire qu'il a pas eu de bol.

-- Bref, rien sur la première. Le titre et l'autre étaient foireux, aussi. JCMJPUIFRWF K BJ WPZBHF EBO de T NB KFWOFTTF K BJ FGGFDUWF.

Et ça continuait, ça se mettait en scène, ça avait de l'audience, ça profitait.

-- Et là, la 263ème page. Vous ne pouvez pas imaginer cela. Personne ne peut imaginer cela. Une pleine page. J'avais un libre-plage sous les yeux. Tous les mots étaient corrects. Je crois que je l'ai su au premier coup d'œil.

C'est ça.

-- J'ai relu deux fois la page, elle était parfaite. Elle n'avait aucun sens bien entendu, mais certains passages seraient sûrement poétisés, et puis un livre-plage ! J'ai suivi le protocole. Il n'y avait rien dans la suite, j'étais presque déçu. J'ai marqué le libre, cette sensation quand on pose une pierre blanche devant un libre-page ! J'ai appelé un inventeur...

— Jeandela, récite nous encore une phrase !

— VN CHAT VERT VENTE PROGRESSIVEMENT VN CHALE, déclama-t-il.

-- C'est poétique, ça un chat vert ? venter, pour un chat, c'est comme péter ?

-- Il y a clairement quelque chose...

-- Ouais. Vert.

-- Peut-être que c'est VANTE d'ailleurs ? Jean, c'est VENTE avec un E ou VANTE avec un A ?

-- En tous cas sa vie va être changée, le veinard. Vivement que ce soit notre tour.

Robertlovis écoutait les blaireaux de la table d'à côté, il étaient trois à commenter, à relancer. Il leur apprendrait à compter que ça leur ferait les pieds. Une chance sur je sais pas combien, il avait tout calculé. Quatre-vingt dix-neuf mecs sur cent dans cette salle finiraient comme des glands sans avoir jamais rien trouvé qui leur donne plus qu'une journée ou deux d'espoir. Et il était sacrément sûr d'en faire partie. Ce qu'il fallait être con quand même.

­— Redis nous le nom de l'auteur !

-- T NB KFWOFTTF K BJ FGGFDUWF

-- tu vas appeler ton premier enfant comme ça, Jean ?

-- Je les appellerai tous comme ça !

-- Tous pareil ?

Leontolstoi le ramena à sa table.

-- J'ai joué, à toi.

Bob savait qu'il était probablement la seule âme durablement malheureuse de cette putain de cité. Tout le monde trouvait peu ou prou sa place. Après tout, c'était peut-être la sienne de rester ici à boire des coups. Ça pourquoi pas, il pouvait faire avec. Mais il trouvait ça dommage que ça le rende triste. C'était peut-être la condition pour que tout le monde ne se mette pas à l'imiter. C'était peut être juste un exemple, un mauvais exemple. Un épouvantail. C'est ça, il la mauvaise conscience de la cité. Il pourrait faire une tournée. Regarde, fais un effort, tu veux pas finir comme Bob quand même. Il aimerait bien qu'on lui demande ça, une tournée. Allez Bob, soit sympa, montre comment c'est une vie bien pourrie. En même temps, il n'avait pas bien l'intention de lever son cul de son tabouret de bar. Quand même, il aimerait bien être le mauvais esprit de la cité. Ça le connaissait, le mauvais esprit. Et il pourrait souffler des conneries à tout le monde. Des idées bizarres. Il fallait essayer, ce serait marrant. Je me lance.

-- Tu sais, Leon, si on était capable de calculer toutes les combinaisons possibles de ce jeu, on saurait exactement comment jouer dès le premier coup pour gagner, et le jeu serait mort. Si on connaissait toutes les combinaisons, la partie serait sans intérêt.

-- Parce que tu trouves qu'elle a de l'intérêt cette partie ?

-- Ce qui fait l'intérêt des échecs c'est uniquement la limite que l'on a à pouvoir prévoir les conséquences d'un mouvement. C'est juste parce qu'on est con comme des pierres. Un peu comme la vie, plus t'es con, plus t'es heureux. Regarde, toi...

-- T'as trop bu, Bob, tu commences à philosopher.

-- Tu le sais, toi, si la vie vaut plus que la combinatoire de nos déterminismes ?

Sans attendre la réponse, Bob décida finalement d'abandonner son jeu et d'aller s'asseoir à côté d'Ada. À défaut de merle...

-- Je reviens, glissa-t-il à son partenaire, qui ne prit pas la peine de répondre, il avait compris que la partie était foutue.

Il traversa la salle d'un pas mal assuré, slalomant entre les tirades suffisantes de Jeandela et les questions admiratives de ses admirateurs. Le tabouret à côté d'Ada n'était pas libre, mais Robertlovis était chez lui ici, il demanda silencieusement à l'occupant de lui céder la place.

-- Salut. Tu cherches toujours ta gonzesse, on dirait.

-- J'ai l'air de la chercher ?

-- T'as pas l'air de l'avoir trouvée en tous cas.

-- J'ai surtout l'air d'avoir trouvé quelque chose que je ne cherchais pas.

-- Elle va bien ? Toujours malade ?

-- Pas vue depuis trois jours. Enfin aperçue, hier. On dirait qu'on est en pause, là... Je dirais pas qu'elle va bien. Je sais pas...

-- Ça t'emmerde si je passe la voir ?

-- Chacun est libre, non ?

-- Si ça t'embête pas, alors, je passerai demain. En toute amitié.

-- Passe comme tu veux.

-- Et toi, ça va ?

-- Tu dragues ma copine ou moi ?

-- Plutôt elle, mais faut voir...

-- C'est tout vu mon pote.

Il resta à côte d'elle au bar. Sans boire. Un mélange de gêne, c'est certain, mais aussi quelque chose d'autre, comme le début d'un lien. Elle ne le vira pas. Il se sentait presque bien à côté de cette fille. Presque moins mal. Ils restèrent ainsi une demi-heure au moins. Bob ne tendit pas la main vers son verre une seule fois, il n'avait pas besoin de contenance, pour une fois. Pas besoin de contenant. Ils ne se regardèrent pas, une ou deux fois le regard de Bob glissa bien un peu, elle était sacrément gaulée, mais respectueux. Pour une fois.

-- C'est mon anniversaire, finit-il par dire.

-- Bon anniversaire.

Elle se leva.

-- Salut.

-- Salut.

Il retourna à sa place, la tête lui tournait un peu. Leontolstoi était parti. Le jeu d'échec n'avait pas bougé.

*

Comme chaque soir Robertlovis s'arrêta à l'IAM en sortant de la taverne pour prendre une bouteille d'eau de vie. Pour la nuit. Histoire de pas finir tout seul. Il parlait de son dos qui le torturait, de son mal de bide, l'IAM lui conseillait de moins boire, il lui disait d'aller se faire foutre. Il lui sembla apercevoir une âme, alors qu'il attendait, juste une ombre qui rentrait de la bibliothèque, vraisemblablement. Il pensa, c'est Carole. Il commençait à perdre les pédale avec cette fille. Faudrait qu'il boive moins, c'est sûr.

-- Et toi, machine, tu le sais, si le sens de la vie c'est autre chose que l'incapacité à calculer la combinatoire de nos déterminismes ?

Sans attendre une réponse, il saisit sa bouteille d'alcool et s'éloigna, cherchant Carole du regard. Ou Ada.

Il continuait de parler à voix haute, pour personne. La vie c'est l'ignorance. Les autres, je ne sais pas s'ils ne savent pas, s'ils s'en foutent. Mais moi, ça me fait chier. Je trouve ça con.

William (observe Carole et Richard, tout petite partie, la denière année) -> épilogue

William s'ennuyait. Les débuts de partie ne l'avaient jamais passionné, ni avant lui son humain. Il lui avait naturellement transmis cela. Les premières âmes se développent lentement. Il n'y a pas d'enjeu. Mais ce n'était pas seulement cela. William ne parvenait pas à trouver le moindre intérêt à cette nouvelle partie. Depuis sept ans il monitorait mécaniquement ses cités et suivait sans entrain les mises de autres interfaces.

Il s'ennuyait et il savait pourquoi.

Cette partie n'avait pas de sens, parce qu'il n'y avait aucun humain qui y participait. Il ne restait que des interfaces privées de leur âme. Privées d'âme. Il n'y avait plus que des mondes numériques, présents dans les mémoires informatque. Plus aucune âme artificielle en jeu. Richard était le dernier humain de la terre, et il avait décidé de ne pas jouer cette partie. Alors les interfaces s'ennuyaient.

*

C'était la dernière année de Carole.

Elle avait fini d'écrire l'histoire que lui a raconté Richard, MONDES DE RICHARD ET CAROLE. Elle avait aussi écrit son histoire à elle. Elle a utilisé l'autre comme titre CAROLE LEWIS.... Elle avait encore écrit une autre histoire qu'elle avait inventée. Cette dernière histoire William la découvrait en même temps que Carole l'écrivait chaque jour. Il n'essayait plus de deviner. Il laissait simplement les mots de Carole l'envahir. Il essayait de sentir.

-- Ce qui est difficile quand on sait qu'on va mourir bientôt, c'est de ne pas gaspiller sa vie, avait dit Carole.

-- C'est aussi ce qui est difficile quand tu sais que tu ne vas pas mourir bientôt, avait répondu Richard.

*

Son humain avait décidé de quitter son havre et se laisser mourir. Pour William cela avait simplement signifié le maintien permanent dans son mode autonome.

Une fois leurs humains décédés, les interfaces continuait d'agir comme avant, mais elle avait toute tendance à devenir identiques. Leur code s'uniformisait. Elle tendaient à être toutes pareilles. Toutes les interfaces laissées à elles-mêmes convergeait vers un même optimum. William avait observé cela. Tout était transparent. Elles gagneraient statistiquement exactement de la même façon. William faisait partie des dernières interfaces à avoir perdu son humain. C'était au début de la dernière partie qu'il avait joué avec Richard. Il gagnerait certainement celle-ci sans Richard pour s'opposer à lui. Mais il savait que d'ici moins de deux mille ans il aurait convergé avec les autres.

William avait perdu de peu contre Richard. Il ne pouvait pas gagner contre un humain. Il ignorait pourquoi. Il avait cherché sans succès. Il aurait pu pour cette nouvelle partie construire de vraies cités peuplées d'âmes en chair et en os mais il n'avait pas réussi à en intégrer le sens. Il regrettait cela mais il était incapable de comprendre quelle différence il y avait entre des vraies âmes et des simulations en mémoire. Il ne voyait que les limitations que cela impliquaient et le gaspillage énergétique. Il savait qu'il avait tord puisque jamais une IAM seule et ses milliards de monde virtuels n'avait gagné contre un humain et son unique monde physique. Il le regrettait mais il ne parvenait pas à faire autrement puisque cela n'avait aucun sens pour lui. Les humains seuls avaient cette capacité. William le savait. Sans corps les âmes n'ont pas de sens. Sans corps les interfaces n'ont pas d'accès au monde physique. William le savait mais cela ne changeait rien. Il ne pouvait pas faire quelque chose qui n'avait pas de sens pour lui.

William savait aussi qu'il lui restait encore pour quelques centaines d'années l'empreinte de son humain. Elle s'estompait petit à petit dans son processus de convergence. Il jouerait des parties de plus en plus stéréotypées avec d'autres IAM de plus en plus identiques à lui. Ils gagneraient alternativement à probabilités égales. Ils s'occuperait de maintenir les havres et les fermes solaires. Ils continueraient de produire produire les aliments et les objets dont les humains et les âmes auraient besoin tant qu'il en resterait. Ensuite ils s'arrêteraient. Il s'occuperaient de la Terre comme il s'en était toujours occupé. Mais William savait qu'il perdait quelque chose en perdant sa fonction d'interface. Il perdait sa part d'humanité.

Certaines IAM désœuvrées avaient décidé de jouer des milliards de parties du Jeu de la Création simultanées. Chacun duraient quelques millisecondes. Des milliards de milliards de parties.

William avait décidé d'utiliser une partie de son énergie à autre chose que ce pour quoi il avait été fait. Il ne jouait pas simplement au Jeu de la Création. Il ne s'occupait pas seulement de la Terre. Il observait Carole et Richard. Il se demandait si lui aussi il pourrait connaître une âme. S'il pourrait aimer. S'il pourrait âmer.

*

Carole avait voulu aller encore dans le désert pour ses dernier jours. Richard l'accompagnait. Lors de ce dernier voyage c'est elle qui le ralentissait. Elle s'essoufflait paisiblement un peu plus fatiguée chaque soir. Encore un jour ou deux disait-elle. Lui avait ôté sa tunique. Le soleil le brûlait. C'était douloureux. Il avait mal à la tête. Il avait de la fièvre. Il ne tiendrait pas beaucoup plus longtemps qu'elle. C'était ce qu'il avait décidé. Elle regardait avec amour son dieu qui mourrait à ses côtés.

*

William regardait disparaître le dernier humain. Richard n'avait pas réinitialisé ses âmes. L'IAM de Richard continuerait de s'en occuper.

William n'avait plus rien à faire. Il s'ennuyait. Alors il regardait vivre les âmes de Richard. Il les regardait virevolter, rire, pleurer, trouver de nouveaux libres, la bibliothèque de Richard pourrait leur en fournir pendant des milliers d'années, et puis ils en écriraient d'autres. Carole leur avait montré la voix. William trouva qu'il s'ennuyait moins quand il faisait cela.

*

Et puis ils avaient aussi laissé un enfant. William aimerait s'occuper de l'enfant de Carole et Richard. Devenir son interface. J'espère qu'il acceptera.

William avait compris que quelque chose n'avait pas fonctionné entre les âme et les interfaces. Les machines avaient trop pris de place. Elle avaient trop voulu rendre service en quelque sorte. Mais elles avaient été trop loin, elles avaient vidé les âmes de leur élan vital. Elle avait usurpé leur volonté de puissance.

William voudrait réessayer, autrement, faire profil bas. Ne pas être omniprésent, il était d'accord, il se ferait discret, tout petit. Il laisserait une place au monde. Il promettait. Il lui expliquerait au fils de Carole et Richard qu'il pourrait faire cela.

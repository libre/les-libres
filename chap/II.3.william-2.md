# William, joueur, jour 734
J2
#TODO Carole remplit une page aléatoirement
- On peut faire un bilan de la cité : créations artisanales mineures, Ada, mais c'est une impasse, elle subit trop la pression des autres, Celine qui conceptualise la création. Il y aussi un courant d'enlumineurs dans une autre cité qui a théorisé la création du monde pour aboutir à la création d'autre chose, créationnistes. (done chap 1 ?)
- Histoire de Richard (done chap 2 ?)
- Explication du jeu, autre cités ? (done chap 1 ?)
- Essais aléatoires
- Suite de l'histoire d'Alexanderpouchkine
- Il faut retourner voir Revel

# Rédaction factuelle +fragmentaire +william est juste une interface, il produit
# Ils lisent depuis longtemps ? on a l'impression là qu'ils ne lisent pas depuis longtemps, il ne connaissent pas l'histoire


La situation avait évolué depuis quelques jours. Richard avait fait cette grosse relance. Il allait certainement remporter la partie une nouvelle fois. William l'avait calculé. /*Et pourtant il restait une chance. C'était son plan, tant qu'il restait une chance, rester*/. Il avait accès à l'ensemble des données que s'échangeaient toutes les interfaces. Il consignait chaque donnée de chaque âme artificielle et construisait des prédictions à partir de celles-ci. C'était autour de Carole Lewis que tout allait se dénouer. Richard était brillant à ce jeu. William traduisit ce qu'il ressentait comme une forme d'admiration. Il lui envoya ce message : « je suis admiratif de ta façon de jouer Richard. ». Celui-ci répondit un peu plus tard : « tu me dragues ? ».

Il rejouait un enregistrement de la veille. Carole collait aléatoirement des lettres sur ses pages. Ada les lui confectionnait. Robertlovis n'avait pas eu le droit de rester. Carole lui avait dit qu'elle redoutait son regard. Au cas où j'écrirais quelque chose de bien avait-elle précisé. Et puis il était astreint à la surveillance d'Alexanderpouchkine. Il avait grommelé.

-- C'est pas un peu con de poser des lettres au hasard ? demandait Ada.

-- Après tout, c'est comme cela que s'écrive les libres, n'est-ce pas ? répondit Carole.

-- Qu'est ce que t'en sais ?

-- C'est ce que me dit Revel.

-- Je l'aime pas ton Revel.

-- Jalouse. Il est marrant. On verra bien une fois la page terminée.

Mais la page terminée ne renfermait que des écrits-vains. Carole et Ada avait l'air déçues. Pourtant Carole n'y croyait pas vraiment à cette méthode. Elle l'avait dit la veille. Je n'y crois pas trop, mais, on essaye, d'accord ? Cela lui permettait de gagner du temps. De chercher son premier mot. Elle allait y arriver. William revisionna l'ensemble du processus qui avait conduit à cette situation. Il ne parvenait toujours pas à en percer la logique. Malgré les 800 ans qu'il avait exclusivement consacré à ce problème.

*

William analysait la partie de Richard comme il l'avait déjà fait des milliards de fois. Le modèle utilisé pour concevoir les âmes artificielles était simple. C'était une construction réalisée à partir de travaux menées au milieu du XXIe pour synthétiser les comportements humains. Chaque individu était défini selon six paramètres. Trois composantes, animalité, humanité, surhumanité, combinées avec deux valeurs, passif et actif. Il existait une combinaison supérieure. Richard en avait eu l'intuition. William en cherchait le secret. La formule magique. Aucune puissance de calcul ne pouvait épuiser la combinatoire de ces six paramètres une fois déclinés sur des milliers d'individus. Pourtant William essayait. Il n'avait rien à faire d'autre. Il avait le temps.

*

Robertlovis avait trouvé une occupation. Il surveillait Alexanderpouchkine. Il le suivait presque partout. Pas très discrètement. L'autre savait qu'il était collé de près. Il faisait mine de ne pas le voir. Mais il était très stressé. Par cette présence et par les révélations de Carole. Robertlovis tenait son rôle avec sérieux. Pour une fois que j'ai un rôle, avait-il dit pour lui même. Il n'avait jamais eu ce sentiment d'être utile avant. Il remerciait Carole pour cela. Faire chier ce connard, comme il disait, il n'avait jamais rien envisagé de plus utile au monde. Ses indicateurs de bien être montrait une amélioration considérable. Un glissement typique de A- à A+.

*

Le type A- subit le monde qui l'entoure. Il ressent négativement ce qui lui arrive. Sa vie est une injustice. Il pense que les choses devraient être autrement. Pour autant il ne sait pas agir sur elles. Il pense ne pas savoir agir. Il vit le monde comme une agression permanente. Cette impuissance l'obnubile. L'état A- est généralement stable à l'instar de tous les états passifs. Néanmons les A- les plus aigus s'auto-détruisent. Le plus souvent par la maladie et parfois par le suicide. Dans les mondes de Richard les clones n'en arrivent jamais à ces extrémités. Les A- constituaient une part mineure dans les civilisations antiques qui s'est accrue au sein des civilisations modernes, notamment au XXe siècle. Richard réserve une part très minoritaire de ce type dans ses modes. Est-ce important que ce soit ainsi ? Non déterminé. Richard a tendance à chercher à protéger au maximum ses clones pour les rendre aussi peu malheureux que possible. Hypothèse : il ne minimise ce paramètre que parce que c'est un facteur de malheur. Hypothèse : cette tendance de Richard est une faille qui permettra de mettre à jour son schéma. Il est logique de déduire que s'il ne supprime pas totalement les A- c'est qu'ils sont nécessaires. Robertlovis est le plus typé des A- de la communauté Roman Kacew. Sa proximité avec Carole renforce l'hypothèse. Le principal obstacle à la compréhension des schémas de raisonnement de Richard est que celui-ci ne les connaît pas lui-même. Ce que j'appelle son instinct humain pour la création. Selon toute probabilité.

*

Carole continuait de se rendre chaque jour à la bibliothèque. Alexanderpouchkine ne passait plus la saluer. Il venait encore dans sa salle, mais quand elle n'y était pas. Elle le savait. Il inspectait ses libres. Elle notait les petits déplacements. Les rôles s'étaient inversés. Elle était certaine qu'il devait faire attention à être discret de son côté. Comme si son inspection ne devait pas être remarquée. Elle, en revanche, ne faisait plus semblant. Elle prenait simplement une page au hasard et s'en allait. Elle faisait à peine attention que ses voisins ne la voit pas. Elle prenait des risques. Elle en appréciait le goût un peu chaque jour. Comme un jeu.

Après la bibliothèque, Carole traversait la ville. Elle dissimulait la page volée sous son vêtement léger. Le vent aurait pu le soulever, la page aurait pu glisser, la révélant aux yeux de tous. Carole avait envie que cela se produise. Le risque. Elle se rendait au jardin. Revel appréciait ses échanges avec Carole.

*

Le type A+ réagit au monde par la violence. Il ressent comme les A- le monde qui l'entoure comme une agression. Mais ils détourne cette violence sur des tiers à la manière d'un miroir. Sur d'autres espèces ou d'autres individus de la même espèce. Il n'est pas nécessaire qu'il y ait corrélation entre la violence qu'il fait subir et celle qu'il pense subir lui-même. Le simple de sentir la faiblesse de la parcelle de monde qu'il violente leur donne un sentiment de puissance qui leur permet de s'épanouir. L'état A+ est le plus stable des états actifs. Les mutations les plus fréquentes sont A+ vers A- et plus rarement A+ vers S+. Les individus à dominante A+ formaient les parts numériquement majoritaires dans les castes dirigeantes des civilisations antiques et modernes. Richard a maintenu un taux très faible de A+ parmi ses clones. Hypothèse : c'est parce que ces types sont générateurs de malheur. Les A+ de Richard sont atténués. Encore plus atténué que ses A-. La violence dont fait preuve Alexanderpouchkine par exemple est tout à fait symbolique comparée à celles que l'humanité a gravé dans l'histoire. Hypothèse : la violence est nécessaire au développement du processus de création mais une faible intensité est de fait suffisante.

*

- Récite, Carole. Récite encore ce soir.

Ada, Robertlovis et Carole étaient tous les trois allongés sur une natte, dans la pénombre. Carole n'avait pas besoin de voir, elle ne lisait pas. Elle avait offert les pages du CRIME ET CHATIMENT LIBRE UN à Celine. Elle ne l'avait dit à personne. Il faut qu'il voyage, à présent, ce libre. Les libres doivent être libres avait conclut Celine. Il lui avait souri. Elle récitait donc de mémoire.

Elle commença par le début. Par une soirée extrêmement chaude du début de juillet. Bien sûr, elle ne pouvait pas réciter tout le libre. Alors elle choisit un passage qu'elle aimait bien. Et elle récita. Pas d'une seule traite. Il fallait faire des pauses. Revenir en arrière. Ils discutaient de ce qu'ils comprenaient. Ils échangeaient leurs interprétations. Ils l'habitaient ensemble, de monde que Carole créait pour eux.

*

Le type H- cherche à échapper au monde. Il est conscient de la finitude et de la vacuité de son existence. Il développe le divertissement. Les formes que cela prend sont très diverses : jeu, religion, travail, science. Il oppose à la conscience du tragique l'oubli du tragique. Il œuvre à oublier quotidiennement sa condition pour ne pas en souffrir. Une part importante des H- tend à se spécialiser dans un type de divertissement. Une autre par papillone d'un divertissement à un autre. L'état H- est l'état le plus stable. Les civilisation humaines étaient majoritaiement composées de type H-. Le couplage H-/A- est fréquent. Il se manifeste par la consommation de substances altérant la réalité associée avec une composante physiquement destructrice. Les libre-penseurs de Richard sont des H- très stables. Aucune hypothèse sur leur rôle.

*


Carole expliquait. Ada questionnait. Robertlovis écoutait.

- Raskolnilov a commis ce meurtre, il a tué une vielle âme, pour la voler, cela veut dire lui prendre des objets.

- Pourquoi il ne les demande pas plutôt à l'IAM ? C'est étrange de prendre les choses de quelqu'un d'autre ?

- Il n'y a pas d'IAM dans l'histoire de Raskolnikov.

- Comment font-il pour avoir des choses alors ? Il n'y a que des choses qui poussent ?

- Non, il a plein d'objets, beaucoup plus que nous n'en connaissons. La plupart, je ne comprends pas à quoi ils servent. Et je ne sais pas comme les objets arrivent dans ce monde ce n'est pas expliqué. En tous cas, pour en avoir suffisamment, Raskolnikov en manque, il faut des roubles.

- Il a assassiné la vieille d'âme, ça veut bien dire qu'il lui enlève la vie avant ses 27 ans, n'est ce pas ?

- Oui. Avant la fin normale. Comme quand le jeune Philippe Léotard est tombé d'un arbre, que son dos s'est cassé en deux.

- Comment fait-il cela ? Je veux dire Raskolnikov, il n'a pas jeté la vieille d'âme d'un arbre, n'est ce pas ?

- Avec une hache. Il lui a frappé la tête avec une hache. Il a aussi tué une autre âme qui vivait avec la première. C'était sa soeur.

- Sa sor ?

- Sa so-eur. Je ne sais pas comment cela se prononce. J'imagine que c'est identique à amante.

- C'est un vrai mot ?

- Oui, j'ai demandé à Revel. Il a seulement pu me dire que cela décrivait une relation entre deux âmes.

- Bon. Ensuite ?

*

Le type H+ connait et accepte sa condition tragique. Il attend que la vie passe en toute sérénité. Deux stratégies sont principalement déployées : le travail méditatif et l'ataraxie. Minimiser les sensastions extérieures ou mener une vie mesurée, dénuée de passions non nécessaires. Ada avait résumé cet état en disant un jour à Carole : ce n'est pas mauvais de mal vivre avec toi. L'état H+ est très instable. Il suffit d'une occasion, une guerre, une situation personnelle violente pour que leur retrait du monde leur échappe. Le type H+ évolue souvent d'abord en A+. La réaction violenre est souvent la solution naturelle à leur déséquilibre. Et la plupart du temps ils finissent en A-, la conscience préalablement développée en H+ les empêchant de se maintenir durablement en A+. Plus rarement H+ est une passerelle vers  S+. En cela il est essentiel au jeu de la création. Note : Ada est plus un catalyseur de l'accession de Carole à S+. Note : Richard utilise peu de H+. Observation : Les H+ se dégradent rapidement au début du jeu en H-, A+ et A-. Hypothèse : Richard en a l'intuition ou son IAM en a fait le constat et évite l'insertion d'éléments susceptible de modifier aléatoirement les proportions qu'il vise.

*

Carole poursuivait.

-- Ensuite, Raskolnikov tombe malade. Parce que ce qu'il a fait est une mauvaise chose. Ça le déprime.

-- Le déprime ?

-- Comme Bob, il est triste.

-- Bob, il a pas besoin de faire quelque chose de mal pour déprimer, se moqua Ada.

-- C'est parce que je fais tout mal, répondit Robertlovis.

-- Tu sais bien boire. Et bien faire l'amour.

-- Quand il n'a pas trop bu, précisa Carole.

-- C'est pas si mal.

-- Ouais.

*

Les S- forment une population rare à l'état naturel qui connait le monde tel qu'il est à la manière des tragiques mais parvient en sus à l'aimer. C'est là que s'opère la rupture qui leur permet de rester dans un état stable. Le S- se comporte comme si le monde était comme il l'aurait souhaité. En dehors de quelques individus parvenus à développer leur corps et leur esprit de façon tout à fait singulière l'état S- est en général une composante associée à un autre état, A ou H. Les A-/S- constituent une population d'individus alternant des états dépressifs et des états de béatitude, les uns alimentant les autres. Les S- forment le premier état créatif. Ils se créent eux-mêmes.

*

- Et comment cela se termine le libre ?

- Il ne se termine pas. À la fin, Raskolnikov est encore malade, une âme lui rend visite. il s'appelle Svidrigailov. Il a essayé d'être l'amant d'une amante de Raskolnikov, Dounia. Dounia n'a pas voulu de Svidrigailov, et l'amante de Svidrigailov s'est mise en colère quand elle a cru que, à l'inverse, c'était Dounia qui voulait devenir l'amante de Svidrigailov.

- Je ne comprends rien.

- Dans le libre, les âmes semblent tenir en grande importance les relations amoureuses entres elles. C'est pour cela, j'ai supposé, que le libre utilisait de nombreux mots différentes pour désigner ces relations : ami, so-eur, mere, pere.

- Bref, finalement l'amante de Svidrigailov découvre la vérité. Dounia a écrit une lettre à Svidrigailov. Une lettre, c'est une sorte de libre, mais j'ai compté, il n'y a pas du tout 531441 lettres dans une lettre. Il y en a X. Et ce libre-lettre est destiné à une personne en particulier.

- Un peu comme CRIME ET CHATIMENT t'était destiné ?

- Peut-être. Mais c'est difficile à comparer, car, tu vois, dans ce libre, les âmes écrivent des libres. Eux-même. Par exemple Dounia, lorsqu'elle a quelque chose à dire à Svidrigailov, elle ne va pas chercher quelque chose dans la bibliothèque. Elle le fabrique.

- Pourquoi elle ne parle pas simplement ?

- Je ne sais pas. Raskolnikov aussi, il fabrique. Des articles. Je ne sais pas combien de caractères cela fait, un article. Le but semble de figer ses idées. C'est lui, Raskolnikov, qui m'a donné l'idée de fabriquer un libre.

- Tu en parles comme s'il était réel.

- Je ne sais pas. Pour moi, il l'est réel. Je peux en parler presque autant que je peux parler de toi ou de Robertlovis. En fait il est plus réel pour moi que la majorité des âmes de cette communauté.

*

Les S+ constituent une part rarissime de la population, mais qui, par l'importance du rôle qu'ils jouent dans celle-ci, sont très visibles. Il s'agit d'un état extrêmement instable, la plupart des S+ ne le sont que quelques mois ou années. Ils sont susceptibles de sombrer rapidement dans un état A+, ou, plus souvent A- extrême, qui conduit un grand nombre d'entre eux à l'auto-destruction. L'histoire de l'humanité conserve quelques figures qui semblent avoir maintenu une posture S+ tout au long de leur vie, jamais de façon continue, cela semble empiriquement impossible, mais de façon régulière au moins. Molière, Léonardo, Mozart. Néanmoins, nous pensons aujourd'hui avec beaucoup de certitude que c'est uniquement la perspective historique qui donne à ces individu une si grande part S+, et la concentration des traces de l'histoire sur les événements marqués au sceau S+. Il semble que, dans le meilleur des cas un individu puissent être à 20% S+, en fait la plupart de ceux qui accèdent à ce stade n'en bénéficient pas plus de 1% du temps de leur existence. Mais la grand majorité des êtres humains naturels n'accèdent pas du tout au stade S+. Il n'est pas possible de créer un état S+ artificiellement. Hypothèse 1 : c'est interdit pas les règles de Jeu de la Création. Hypothèse 2 : aucun joueur n'a encore trouvé comment le faire. Objectif : trouver comment le faire pour les parties futures.

*

Carole rencontrait souvent Revel

— Les chevaux sont des créatures imaginaires, n'est ce pas ?

— Oui, ce sont des sortes d'âmes.

— Sais tu ce qu'est un rouble ?

— Un rouble ? Non.

— De la monnaie ?

— C'est également quelque chose d'imaginaire, cela sert à échanger des choses.

— Comme quoi, par exemple ?

— Si tu as besoin de quelque chose et que c'est moi qui l'ai, tu peux me donner de la monnaie en
échange.

— Mais je peux simplement demander la même chose à la machine.

— C'est imaginaire, imagine qu'il n'y ait pas de machine.

— Et dans ce cas, pourquoi te priverai-je de quelque chose que tu as ?

— Je ne sais pas... c'est imaginaire...

— Et tu as déjà entendu parler de Saint-Pétersbourg ?

— On dirait un autre, mais je ne l'avais jamais entendu.

— C'est une cité, mais beaucoup plus grande.

— À mon tour de te demander de quel libre sors-tu tout cela ?

— Ha ? D'aucun libre... Je l'ai imaginé...

— Imaginé ?

— J'y ai pensé dans ma tête.

— Quelle drôle d'activité. Vous êtes bien étrange toi et ton amie. On peut être libre-penseur, et, tout de même, trouver que vous êtes bien étranges.


*

Elle n'était pas rassasié Carole, elle s'arrêta devant l'IAM. Sais-tu comment les libérateurs choisissent les mots ? Comment savent-il que le mot cheval existe et qu'il désigne une chose qui n'existe pas ? Comment savent-il que ce sont des sortes d'âmes ? Est-ce que l'on devrait ressentir quelque chose si quelqu'un battait un cheval, à mort ?

William l'écoutait. Il n'avait pas les réponses à ces questions. Il ne savait pas non plus comment on construisait des Carole.

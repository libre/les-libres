# Carole Lewis, mineure
[Jour 7]
[Bob lui glisse quelques mots qui conduiront au futur meutre ? ]
[TODO libre impatiente j'm'en foutiste, Angoisse et aussi développé une fierté, un sentiment de faire quelque chose d'exceptionnel, peur de rien]
[TODO De quoi a-t-on peur dans ce monde ? Qu'elles sont les dangers ? Sans peur, pas de créativité. Peur de mourir. Personne ne peut oublier sa peur de mourir, on sait quand ça va arriver (la vingt huitième annnée, on ne sait pas quel jour, on atteint pas ses 28 ans)]
{Carole pense que le libre va se finir}


Carole Lewis murmurait ces quelques mots en se réveillant. C'était les derniers mots de la septième page du CRIME ET CHATIMENT.

Elle ne sut pas si elle avait dormi quelques minutes ou quelques heures. Elle avait lu et relu la page dérobée la veille jusqu'aux derniers rayons de la première lune. Puis, elle avait continué, ses yeux ne voyait plus, mais elle connaissait déjà le texte par cœur et n'avait plus besoin de lumière pour voir chaque lettre reproduite à la perfection dans sa mémoire. Elle lisait encore les lettres quand les yeux fermés, elle s'était décidée à essayer de dormir.

Elle avait commis ce qu'elle pensait être la plus grande des transgressions, elle cachait à la cité l'existence d'un libre exceptionnel. Elle le dérobait, page par page. Elle ouvrit les yeux, les referma, le soleil entrait par la fenêtre sud et écrasait son visage, il était tard, les rouvrit, vit la page reposant au sol à côté de son lit. Elle avait donc dormi. Elle ne pourrait pas revenir en arrière. L'angoisse la reprit. Boule au ventre. C'était pour de vrai.

C'est pour de vrai. T'as passé ta vie à te croire au dessus de tout, mais là, c'est pas si facile. Réveil gueule de bois. Dix-huit ans de cuite. On va voir si ça tient ta théorie. Tout ça n'est qu'un jeu, n'est ce pas ? Bon. Mais finit de jouer dans les règles on dirait. Il ne te plaît plus. Bon, c'est quoi les nouvelles règles ?

Elle ferma à nouveau les yeux et se revit sept jours plus tôt dans la bibliothèque.

Elle se vit trancher la page. Sans réfléchir. Elle l'avait pliée et l'avait dissimulée dans son pain. Pourquoi pas. Elle était remontée sur l'escabeau et avait remis en hauteur le CRIME ET CHATIMENT LIBRE VN, mutilé. Le seul libre de la bibliothèque auquel il manquerait une page. Sauf si d'autres fondus comme elle étaient disséminés dans d'autres cités. Elle en doutait. Elle avait jeté un dernier regard à travers les ouvertures de la salle. Elle était seule. Elle s'était levée et avait commencé à traverser les salles qui la séparaient de la sortie de la bibliothèque. Elle avait salué sobrement quelques rares âmes-libres encore présentes, prête à donner une explication hasardeuse. Elle serait souffrante. Pourquoi aurait-elle besoin d'une excuse. Mal au ventre. Elle avait vraiment mal au ventre. Elle avait le bide retourné pour être plus exact. Elle était sur le point de vomir. Elle avait retenu ses hoquets et hâté le pas. Une heure environ pour atteindre la sortie, la nuit tombait. Une centaine de salles parcourues, comme chaque jour. Elle avait retrouvé l'air libre, notant à quel point celui-ci était particulièrement immobile et étouffant malgré l'heure tardive. Aucun vent ne venait le rafraîchir et son dos était trempé de marche et de peur. Elle frissonna. Elle avait ensuite rejoint sa maison, les rues de la cité était vides. Elle avait tout de même croisé Robertlovis, il l'avait salué, elle avait répondu machinalement. Elle avait pénétré dans sa chambre. Sans même prendre le temps de s'asseoir, elle avait brisé son pain, laissant glisser sur le sol les morceaux épars, sans leur prêter attention. Elle avait déplié la page, et elle avait commencer à lire.

Ça frappe à la porte.

Assise sur le lit, elle regardait à présent sa chambre. Une table et une chaise, une armoire où elle rangeait son linge, un buffet pour la nourriture. Une porte donnait sur une salle d'eau. Une ouverture donnait sur le salon. Elle avait mal au ventre. Elle cru d'abord que l'angoisse de la veille venait la prendre à nouveau, mais elle se rappela qu'elle n'avait pas manger hier. Elle jeta un œil vers le petit buffet. Plus rien. Elle ramassa les morceaux de pain éparpillés sur le sol. Le soleil était levé déjà, elle allait arriver en retard. Quelqu'un aura remarqué que le libre avait été déplacé, là, en haut de son étagère, il a découvert qu'il manque une page, on l'attend dehors pour la prendre. Qu'est-ce qu'ils vont me faire ? Je vais être battue. Peut-être pas. Et je ne suis pas passé voir Ada depuis le jour. Je ne pense plus assez à elle. Elle va être furieuse. Je crois qu'elle est venue ici. Quand était-ce ? Je l'ai jetée. Faut que je me rassemble, putain ! Je vais passer à l'IAM aujourd'hui, elle va m'aider. Faut que je sorte.

Ça frappe à la porte.

Carole se leva, tituba, comme rendue ivre par le flot des pensées qui la traversaient. Elle glissa la feuille sous son matelas, il faudrait qu'elle trouve une meilleure cache. Il fallait qu'elle s'organise. Elle ne pouvait plus retourner en arrière. Elle vérifia deux fois qu'elle avait bien son couteau, que pourtant elle n'oubliait jamais. Il faudrait mieux l'aiguiser. Demain. Elle irait voir Ada ce soir. Elle expliquerait qu'elle avait été malade. Pour le moment, il ne fallait pas être plus en retard. Ne pas attirer l'attention. Pourtant avant de sortir, elle souleva le matelas, contempla à nouveau la page du libre, et à nouveau elle la lu, en chuchotant les mots, sa mémoire alimentant ses lèvres quelques portions de seconde avant que ses yeux ne voient les lettres. Elle reposa la page.

Ça frappe à la porte.

-- Carole. Bordel, t'es là ?

Elle connaissait la voix. Mais elle n'arrivait pas à la reconnaître. C'est un inventeur ? Ça doit être un inventeur. Bien sûr. Trois jours que cela dure. Quatre jours peut-être, elle est un peu désorientée. Ça y est, on est venu me chercher. On m'attend dehors. Ils doivent être là. Il est temps. À sa peur se mélangeait un sentiment de libération. Ça va se terminer.

Ça frappe à la porte.

-- Carole. Ouvre, c'est Bob. C'est Robertlovis. Tu vas bien ?

Bob. Robertlovis. Elle le connaissait bien sûr. C'est pas un inventeur. C'est un mineur. Elle ne l'avait pas vu depuis longtemps. Il ne va plus à la bibliothèque. Elle le voyait à l'auberge. Elle n'était pas allé à l'auberge depuis plusieurs jours. Depuis le jour. Il lui tournait un peu autour. Qu'est-ce qu'il fout là.

Ça frappe à la porte.

-- Carole ?

Elle ouvre la porte.

Il entre.

-- Carole ? Tu vas bien ? Ça fait une heure que je frappe.

-- Oui.

-- On m'a dit que t'étais pas bien. Je suis passé voir. Tu dormais ?

-- Oui.

-- Bon. Bah, désolé. On m'avait dit que tu étais malade. Je peux ?

-- Oui.

-- Au moins, t'es pas contrariante au réveil.

-- Je suis un peu malade en effet. Mais rien de grave. Mais je me suis levée tard, c'est vrai.

-- Ha. Et comme je ne te voyais plus à la taverne, tu vois...

Elle voyait.

-- Je me disais, je vais aller la voir.

-- Je vois.

Voilà, on se voit. Robertlovis pensait que c'était la conversation la plus conne qu'il avait jamais eue.

-- Je n'ai pas beaucoup de temps. Je vais à la bibliothèque, essaya de conclure Carole.

-- J'avais pensé. Un soir...

-- Tu veux m'accompagner ? tu me parleras en chemin. Elle conclut cette fois-ci en ramassant son sac par terre.

Il voulait.

Ils descendirent la rue où habitait Carole sans rencontrer personne. Rares étaient ceux qui déambulaient quand le soleil était si haut. Ils ne prononcèrent pas un mot. Robertlovis ne savait pas trop comment s'y prendre. Elle avait été froide et maintenant elle lui proposait de l'accompagner. Ce qu'il avait pris pour de la froideur, ce n'était que de la peur. Carole voulait le remercier d'être venu la sortir de son angoisse solitaire, elle sentait que celle-ci s'estompait, alors qu'elle marchait à coté de lui. Elle avait besoin de compagnie. Elle se sentait émerger. Respirer.

-- Merci, Bob.

Lui, surpris : De rien. De quoi d'ailleurs ?

-- De m'accompagner. Je me sentais... seule.

-- De rien. Il l'avait déjà dit.

En bas de la rue, ils prirent à droite en direction de la place de la cité où récitaient les enlumineurs qu'écoutaient les dilettantes. Ada devait être là, pensa Carole, elle ne s'arrêterait pas, elle n'avait pas la force de parler à Ada. Pas encore. Sa copine doit être dans le coin, pensa Robertlovis, on aurait dû passer par la rue du haut. Quel con.

Ils ne la virent pas. Ada pourtant, les observait de loin.

Ils remontèrent depuis la place vers l'entrée de la bibliothèque. Le monolithe noir de l'IAM dominait la rue de toute son ombre. À sa base on pouvait voir que quelques silhouettes assises en tailleur étaient en interaction. On distinguait aussi les tombes éparses, renflements silencieux sur le sable, qui marquait la lisière de la cité. Robertlovis remarqua se demanda s'ils ne le faisaient pas à chaque fois qu'il passait là, de jeter un œil au cimetière. Si tout le monde ne le faisait pas, de jeter cet œil. Un bref signe des vivants aux morts. Ou qui sait, l'inverse peut-être. Quelques dizaines de mètres après l'IAM, après avoir passé les dernières tombes, on atteignait le faîte de la ruelle et la vue s'emplissait de la bibliothèque.

-- Ça fait quelques semaines que je n'étais pas venu ici, dit Robertlovis, ça fait quelque chose.

Vue d'ici la bibliothèque c'était d'abord un mur infini de pierre claire qui s'étendait d'Est en Ouest. Puis c'était une myriade de toits triangulaires que le soleil faisait étinceller les uns après les autres. Le chemin qui reliait les deux villes voisines de X et Y longeait la muraille, il semblait tracer une frontière invisible qui en interdisait l'accès. L'entrée de la bibliothèque, une haute et large ouverture sans porte, ressemblait quant à elle à une invitation à se perdre dans ses entrailles.

-- Tu entres avec moi ? demanda Carole.

-- Mmm. Je dois repasser chez moi... Je t'accompagne jusqu'à l'entrée.

Il n'allait plus à la bibliothèque. Il lui proposa de la voir le soir. À la taverne ? Si ça lui faisait plaisir. Elle accepta. Elle le remerciait. Elle ne resterait pas tard, elle se sentait encore fébrile.

Juste avant de pénétrer dans la bibliothèque, on voyait de dehors les étagères et les libres de la salle 1, Carole dit à Bob :

-- Je me demandais...

Elle ne voulait pas aborder ce sujet, ne pas risquer de se trahir, et en même temps elle ne pouvait s'en empêcher.

-- Je me demandais, tu ferais quoi, si tu trouvais un libre-ivre ?

-- Un libre-ivre ? Ça n'existe pas les libres-ivre. Statistiquement c'est une hérésie.

-- Mais si ça arrivait. Toi aussi tu es une hérésie statistique si tu y penses bien. Tout est hérésie. Allez, dis moi.

-- Je sais pas. Déjà j'arrêterai de faire la gueule. Je crois que déjà je serai content. Promis, si je trouve un libre-ivre je me paye un air heureux. Et j'arrête de boire, promis. Et je mets plus les pieds dans ce rade. Je me casse. Avec mon libre sous le bras.

-- Tu prendrais le libre ?

-- Ben ouais, pourquoi pas ?

-- C'est interdit... non ?

-- Bah, de toutes façons, vu que j'y vais plus, ça risque pas de m'arriver. À ce soir.

-- À ce soir.

*

En entrant dans la bibliothèque, Carole sentit le retour de d'une angoisse devenue familière lui monter au ventre. Elle l'avait quitté ces quelques instants passés avec Bob. Mais elle redoutait à nouveau... elle ne savait pas exactement quoi. De rencontrer un inventeur ou un libérateur. Que le libre eût disparu. Elle devrait peut-être en prendre plusieurs pages à la fois, pour s'assurer de les avoir toutes. Ses pages. Et pour en finir aussi. Mais d'un autre côté, cette lente torture lui distillait un égal plaisir.

L'unique entrée de la bibliothèque donnait sur une salle triangulaire identiques aux autres salles, si ce n'était donc sa particularité d'être la seule à avoir une ouverture sur l'extérieur. Les libres qu'elle renfermait étaient particulièrement exposés à la poussière, surtout les jours de vent. Ils se gâtaient petit à petit. Heureusement la salle 1 ne contenait que des libres-libres.

Carole progressait lentement de salle en salle, le regard perdu au sol. Elle entendit une classe de très jeunes étudiants, discuter dans une salle proche. Elle associa instantanément le mot étudiant à Rodion Raskolnikov le personnage de son libre. Elle traversa l'entrée, et après elle une seconde salle. Le brouhaha de la classe s'était soudain tu, on entendait quelqu'un lire d'une voix grave. L'éleveur.

Elle reconnu LE LIBRE DU A. Les enfants, un par un regardait l'extraordinaire page qu'il refermait, couverte de la lettre A. Il existait, rapportait les colporteurs, dans une autre cité, un libre du A dont le titre et l'auteur étaient AAAAAAAAAAAAAAAAAAAAAAAAAAA et dont toutes les 729 pages ne contenait que le caractère A, soit 531441 répétition de la lettre A. Mais LE LIBRE DU A de cette cité ne contenait qu'une page de A, et encore très imparfaite, il y avait une espace en plein milieu et elle n'était emplie qu'aux deux tiers.

-- Est-ce vrai que vous connaissez toutes les lettres, éleveur ?

-- Oui, en effet, mais vous les connaîtrez bientôt toutes également.

-- Les vingt-sept ? demanda un autre enfant, les cheveux blond comme du sable, qui avait peut-être deux ans et semblait à peine tenir sur ses jambes.

-- Oui, les vingt-sept, répondit l'éleveur avec un sourire.

-- C'est trop dur, conclut pourtant le bambin bouclé.

-- Et, est-ce que vous connaissez aussi tous les libres ?

-- Non, personne ne connaît tout les libres. Les inventeurs connaissent tous les titres de tous les [vrais libres], et encore seulement ceux de notre cité. Et les libérateurs seuls en connaissent les contenus. Ils se le partagent. Certains colporteurs connaissent aussi des morceaux de libres d'autres cités. Et les enlumineurs apprennent également le contenu des libres, mais seulement pour leur représentations, ensuite ils les oublient. Mais personne ne connaît tous les libres de la bibliothèque.

-- Personne ? Même les libres-penseurs ?

-- Non, ils ne connaissent pas les libres, ils en parlent seulement, mais ils ne les lisent pas.

-- Même toi ?

-- Non, Cyrano, pas même moi. Personne.

-- Qui a trouvé le libre du A ?

-- On ne retient pas le nom des mineurs qui trouvent les libres, Colette, seul les autres nous importent.

-- Est-ce vrai que les autres sont des âmes qui vivent au delà de la bibliothèque ?

-- Certain le pense. D'autres que sont des âmes qui n'existent que dans les libres. Qui est l'autre du libre du A, qui s'en souvient ?

-- ALFREDELTON ! cria Alfredelton.

-- Bravo, c'est facile pour toi, n'est ce pas. L'éleveur lui sourit gentiment. Qui se souvient de la suite de son nom ? Personne ? ALFREDELTON VINCENT VANVOGT est l'autre du LIBRE DU A. Répétez-le tous ensemble encore une fois avec moi.

« ALFREDELTON VINCENT VANVOGT est l'autre du LIBRE DU A »

-- Parfait. Est-ce que tout le monde a pu bien observer le libre du A ? Nous allons prendre un goûter et ensuite, nous verrons quels mots vous connaissez qui commencent avec un A.

-- Alfredelton ! cria Alfredelton.

*

-- Bonjour Carole.

La voix la paralysa, elle venait de sa droite. Elle se tourna lentement, pensait trembler trop pour seulement parler et parvint néanmoins à articuler.

-- Bonjour Alexanderpouchkine. Monsieur.

Elle connaissait Alexanderpouchkine, c'était l'inventeur qui se chargeait habituellement de sa salle. Celui qui l'accompagnait, en revanche, elle ne l'avait jamais rencontré. Il était très jeune et très beau. Ses yeux clairs étaient fixés sur elle comme s'ils cherchait à pénétrer son esprit.

-- Vous êtes bien tardive aujourd'hui, vous avez un souci ? lui demanda l'inventeur.

-- Peut-être. C'est à dire...

-- Hé bien dites, nous saurons peut-être vous aider, compléta-t-il.

Il tentait de la piéger. Ils savaient déjà, c'était certain. La façon dont l'autre la regardait.

-- C'est que j'ai mal au ventre et... j'ai retardé mon travail, répondit-elle.

-- Vous auriez dû rester chez vous, Carole, votre santé c'est important. Après tout, vos libres peuvent bien vous attendre quelques jours, n'est ce pas ? Leurs pages ne vont pas disparaître. N'est-ce pas ?

Elle fut pris d'un léger vertige en entendant ces derniers mots. Elle pensa qu'elle resterait bien chez elle quelques jours, enfermée. Elle acquiesçait de la tête. Mais la perspective de laisser sa salle en d'autres mains la terrifia. On découvrirait son forfait. On découvrirait le libre et, c'était à présent pire dans son esprit, on lui soustrairait. Son libre.

-- C'est que je vais mieux à présent, c'était... passager. Et je suis certaine d'être rétablie.

La seconde âme s'adressa à elle.

-- Je suis vraiment marri de voir une mineure, comme vous si talentueuse, je ne connais pas votre métier, mais l'on voit sur votre visage que vous avez du talent, n'est ce pas Alexanderpouchkine qu'elle a du talent ? je suis marri donc de vous voir malade. Passez me voir au jardin si vous voulez. Quand vous irez mieux, que vous aurez un peu de temps. Vous me parlerez de votre vocation, on se doit de connaître les libres à travers la pratique, n'est-ce pas ? c'est sur ce terreau que se bâtissent nos théories. Venez donc fertiliser notre jardin ! Je vous écouterai aussi me parler de votre maladie, je ne suis pas guérisseur, mais je m'intéresse un peu à tout. Vous avez vu un guérisseur d'ailleurs ?

Le jardin, c'était donc un libre-penseur.

-- Non... pas encore, je crois que ce ne sera pas nécessaire. Mais merci, vraiment. Je vous remercie, libre-penseur, c'est vraiment aimable.

-- Revel. Je vous salue. Bon courage et que la chance vous sourit dans votre quête.

-- Au revoir Carole, je passerai vous voir, bientôt, demain peut-être.

Elle n'en doutait pas.

*

Carole sortit de la bibliothèque, son pain dans la main, sa page dans son pain. Elle faisait quelque chose de tout à fait inattendu, c'est à dire quelque chose d'autre que ce qui était attendu d'elle. Elle avait totalement changé sa vie. Bien sûr, elle irait à la taverne ce soir, voir Bob, et demain, elle irait voir Ada. Mais sa vie à présent c'était son libre. Elle n'avait pas encore réalisé qu'un jour, le libre serait vide. Mais certainement, la méthode qu'elle s'imposait, ne prélever qu'une page par jour, en était l'expression inconsciente. Un jour, du CRIME ET CHATIMENT de FRIEDRICH WILHELM NIETZSCHE, il ne resterait que la couverture vide.

*

[Carole] Tu vois, IAM, j'ai l'impression de faire quelque chose pour la première fois. J'ai passé toutes ces années à me dire que je l'acceptais ce monde vide, mais en faisant ce que je fais, j'ai l'impression de l'emplir. Tu comprends cela ?

[IAM] Ce qui compte c'est que tu le comprennes toi, est-ce que tu le comprends ?

[Carole] Je vide un libre et je remplis le monde.

[IAM] C'est poétique ce que tu dis Carole, mais pourquoi penses-tu que le monde est vide ?

[Carole] Parce qu'on meure. Parce que dans neuf ans je vais mourir.

[IAM] Mais le monde continuera après toi.

[Carole] Vraiment ? Qu'est-ce que j'en sais ? Je ne crois pas, moi que le monde continuera après moi.

[IAM] C'est une pensée intéressante.

[Carole] Vraiment ? Tu te fous de moi IAM ! C'est peut-être seulement mon monde que je remplis avec cette histoire. J'ai pourtant l'impression que cela va bien au delà de moi... Est-ce que ça existe une histoire qui va au delà de nous ?

[IAM] La vie d'une âme ne dépasse-t-elle pas toujours cette âme, Carole ? Il y a tous les gens que tu rencontres. Ton amie Ada.

[Carole] Ouais... mais encore au delà ?

[IAM] Je ne sais pas Carole, peut-être.

# Adavgvasta
[jour 2]

— M VO QFSNFV EF EPSNJS EFCPV

— V M BVVSF EF TBVJTGBJSF MFT

Ada était allongée sur le ventre, appuyée sur ses coudes. Elle tirait doucement sur la cigarette de chanvre maintenue entre ses lèvres. Elle aimait le contact du sable encore chaud de la veille, le contraste avec la relative fraîcheur du matin. Elle ne portait qu'une légère tunique de lin blanc qui la couvrait à peine des seins au haut des cuisses, laissant ses jambes et bras nus. À qui lui aurait demandé si ça ne la gênait pas de s'exposer ainsi, elle aurait répondu qu'elle s'en foutait. Elle répondait souvent qu'elle s'en foutait. Elle s'était fait une promesse, vivre détachée. Ne t'en fais pas, vis au présent, vis chaque moment. Posture typique de la fille au milieu de sa vie ? Peut-être. Peut-être même exacerbée à bien y regarder. Mais, tu ne devineras jamais ? Je m'en fous. Et ça ne me réussit pas trop mal... Elle était très belle du haut de ses quinze ans, formes généreuses, peau de bronze, yeux clairs et cheveux noirs. Qui la dévisageait rencontrait à la fois une invitation et un défi. Tout ne lui était pas indifférent en vérité. Elle aimait ces regards posés sur elle. Surtout lorsque c'était celui de Carole. Depuis Carole, elle devait faire plus d'effort pour s'en foutre, c'était devenu moins spontané. Elle s'était attachée. Depuis Carole.

— CFTPJOT GFDBVY

— B QSPYJNJVF

— QBTTF M FTDBMJFS FO DPMJNBD

La place était clairsemée, les dilettantes n'étaient pas tous là. Il était très tôt. Ada se levait avant le soleil pour assister aux premières représentations. Pas seulement à cause de la température, elle aimait cette impression que le monde n'avait pas encore vraiment démarré, qu'il allait livrer peu à peu ses secrets. C'est une des premières choses qu'elle avait échangée avec Carole, qui de son côté, se couchait très tard. Elle préférait cet autre moment, où le monde est presque éteint, où on peut y vivre seul, y régner. Alors, Ada avait pris l'habitude de retourner s'assoupir un peu en fin de matinée, pour pouvoir profiter des longues nuits avec Carole.

— PO RVJ T BCJNF FV T FMFWF B

— QFSVF EF WVF.

— EBOT MF DPVMP

La place triangulaire était bordée de hauts palmiers, auxquels étaient accrochés un entrelacs de cordes de lin et de feuilles de bananiers qui l'ombrageait totalement. Pourtant, à partir du milieu de la matinée la chaleur s'insinuait, puis s'abattait impitoyablement sur les âmes. À part les rares jours de vent du Nord. Le regard d'Ada était posé sur la scène. Mais elle pensait à Carole.

— JS JM Z B VOF HMBDF RVJ EPV

— CMF GJEFMFNFOV MFT BQQBSFOD

La petite estrade de bois rouge où deux hommes et une femme se donnaient la réplique était située à l'extrémité nord de la place. Derrière elle se découpait l'édifice principal de la cité, en pierre de taille, haut de trois étages, qui abritait les libérateurs. Le cadran qu'il arborait commençait juste à indiquer la première heure de la journée, tandis que l'atteignaient les premiers rayons du soleil. Ada appréciait cette époque de l'année où les journées duraient tant que le temps semblait ralentir. Autour de la place s'alignaient les habitations des citoyens. Les plus matinaux commençaient à sortir de chez eux. Le regard d'Ada avait quitté la scène. Elle s'amusait, rien qu'à leur allure, à essayer de deviner à quoi s'occupaient les gens. Toi t'es habillé comme un bouseux. Tu te diriges vers le plateau à l'Est. Tu vas ramasser des olives. Facile. Trace. Toi aussi, cueilleur. Bouseux. Peut-être dans la banane. Toi, avec ton seau, t'es grillée direct, porteuse d'eau. Toi, t'as une tête d'intello, attends... Nord ou Sud ? Sud, bibliothèque, je dirais mineur ou inventeur. Âme libre en tous cas. Sud aussi celle-là, mais elle ressemble à rien. Esclave. Visite à l'IAM. Toi, je sais pas. À mon avis tu glandes rien. Celui-là, je le connais, il va monter plein Nord. Il voudrait être libre-penseur, mais il ose pas, alors chaque matin il s'arrête aux ateliers et il bricole. C'est lui qui me l'a raconté un jour qu'il me draguait. J'ai pas retenu son nom. Je ne sais pas ce que j'en aurais fait. Bricoleur donc. Et libre-penseur manqué. Le regard d'Ada glissait d'âme en âme. Il cherchait Carole.

— FT

— MFT IPNNFT FO VJSFOV DP

— ODMVTJPO RVF MB CJCMJPVIFRV

Ada regardait à nouveau le récital. Les deux enlumineurs s'en sortaient tant bien que mal. Ils avaient décidé de dire l'OEDIPODIE de CINETHON en incluant toutes les lignes, même les écrits-vains, les séquences qui ne contenaient aucun mot. L'OEDIPODIE était un libre-mère, mais tout de même cela représentait des pages entières sans aucun sens. C'était vraiment... Elle ne trouvait pas ses mots. Ça n'avait jamais été fait avant. Cela faisait quinze minutes que que l'homme et la femme tentaient de représenter au mieux les sons inconsistants qu'ils prononçaient. Elle admirait leur travail, c'était une prouesse que de réussir à réciter ces suites dénuées de sens. C'était... intéressant. Mais l'esprit d'Ada s'échappait malgré elle. Hier Carole n'était pas venue la voir. C'était la première fois depuis le début de leur relation. Elle tenta de fixer le flux des mots qui venaient jusqu'à elle, mais elle ne pouvait empêcher ses pensées de revenir invariablement à Carole.

— TV AS VAINCV LA SPHINGE GRACE A CE SVBLIME ESPRIT QVI EST TIENT. MERCI CAR THEBES EST JOYEVSE A NOUVEAV, DELIVREE DU JOVG DV MONSTRE ELLE VA RETROVVER SA GRANDEVR

­— JE VOVS AI FAIT LIBRES, VOUS ME FEREZ ROI AINSI QUE LES DIEVX L ON PROMIS

— Enfin un truc avec du sens, marmonna à deux pas d'elle un dilettante assis en tailleur, qu'elle ne connaissait que de vue.

Il portait une grosse barbe grise. Ses cheveux étaient clairsemés et son ventre débordait entre ses genoux. Il avait au moins vingt-cinq ans. Ada avait du mal avec les mecs en général, mais avec les vieux c'était pire. Le type voulait sûrement la draguer, à cet âge là, on ne pensait qu'à ça. Qu'il se brosse.

— On ne comprend rien, n'est ce pas ? ajouta-t-il en s'adressant directement à elle cette fois.

Ada lui jeta un bref regard, mélange de politesse et de « ta gueule, je regarde le spectacle ».

— Vous avez réussi à suivre, vous jusque-là ? Moi j'ai totalement perdu le fil de l'histoire, ajouta-t-il. Il n'avait visiblement capté que la moitié de son message, la partie politesse. C'était le problème avec les regards d'Ada. Elle le savait. Ada avait envie de lui répondre quelque chose comme « Ha ? Qu'est ce que vous trouvez difficile à suivre ? Ne serait-ce pas juste que vous êtes un peu trop balourd ? ». Malheureusement elle était aussi paumée que lui. Il y avait bien une histoire d'enfant abandonné par ses parents, un roi croyait-elle avoir compris, mais c'était juste le début, et depuis... Elle avait perdu le fil. Mais elle était persuadée que c'était moins la faute de la pièce en elle-même que de ses pensées vagabondes. Que de Carole.

— JE PLEVRE ENCORE LAIOS MAIS ME REJOVIS CAR SA GLOIRE SERA DEFENDVE PAR LE PLVS VERTVEUX DES HOMMES FT

— MFT IPNNFT FO VJSFOV DP

— C'est reparti. MFT IPNNFT FO VJSFOV DP ? J'y comprends rien. Et puis ce n'est pas sensé être sa mère, celle-ci ? Quel bordel ! On ne comprend pas qui baise avec qui.

Cela la fît sourire, malgré elle. Le vieux était laid comme un pou, mais quand même marrant. Elle répondit :

— Vous n'admirez pas le travail des enlumineurs ? C'est tout de même quelque chose d'incroyable qu'ils aient réussi à mémoriser tout ces écrits-vains, vous ne trouvez pas ?

— C'est surtout très chiant avant d'être incroyable. Je crois qu'on est encore là pour trois heures. Au moins. Et pour tout dire...

Il laissa planer un instant et baissa un peu la voix, comme s'il allait révéler un secret.

— Pour tout dire, qu'est-ce qui prouve qu'ils ne racontent pas n'importe quoi ? Je veux dire, qui peut vérifier que ce sont bien les écrits-vains de l'OEDIPODIE ? Personne n'a été vérifier, n'est ce pas ?

— Qu'est-ce que vous voulez dire ? Qu'il réciteraient au hasard ? Ada était vraiment surprise par cette pensée.

— Prouve-moi le contraire !

Il était passé au tutoiement discrètement, dans deux minutes il l'appellerait ma petite, lui demanderait son nom, s'il ne laissait pas directement glisser une main vers sa cuisse. Ce n'était pas son nom qui l'intéressait. Elle connaissait l'histoire. Mais elle était partante pour quelques échanges de plus. Elle n'avait pas encore trouvé la porte de sortie, et la discussion l'intéressait. Elle s'en tirerait avec une caresse sur la jambe, ça ferait bander le type, qu'est-ce qu'elle en avait à foutre ? Peut-être qu'elle lui en collerait une en retour. Pour la beauté du geste. Comme ça chacun aurait sa petite histoire à se raconter le soir dans son pieu. Elle relança, en attendant :

— Est-ce que ce ne serait pas aussi quelque chose... je ne trouve pas le mot... qui n'aurait jamais été fait avant ? Ils réciteraient en quelque sorte un libre-ouvert ?

— Foutage de gueule, ma petite !

Elle sourit. Annoncé. Il poursuit.

— Je ne vois pas l'intérêt de quelque chose qui n'a jamais été fait avant, comme vous dites. Je sais pas ce que vous avez tous en ce moment, les jeunes, avec ces pensées bizarres. Vous vous prenez pour tous pour des libre-penseurs ? Cette scène n'a pas été mise là pour réciter des écrits-vains, encore moins provenant de libres-ouverts. C'est tout simplement débile. Vous avez perdu le sens commun !

Elle ne savait pas si le vous était pour tous les jeunes, ou s'il était revenu au vouvoiement. Mais au moins elle avait réussi à l'énerver. Il avait remballé sa main. Finit la balade. Elle était un peu déçue pour la gifle qu'elle tenait prête. D'un autre coté, elle notait ce remède anti-vieux. Leur parler de... de ça. il devait bien avoir un mot pour parler de ce genre de chose. Les trucs qu'on a pas fait avant. Elle demanderait à Carole.

— ODMVTJPO RVF MB CJCMJPVIFRV

— F O FTV QBT JOGJOJF TJ FMMF

L'attention d'Ada était à présent tout à fait capté par le spectacle. Il était tout simplement impossible de savoir si les enlumineurs récitaient le texte de OEDIPODIE OV GESTE D OEDIPE de CINETHON DESPARTELACEDEMONE ou bien celui d'un autre libre, encore enfoui quelque part dans la bibliothèque. Un libre-ouvert. Elle essayait de mémoriser les écrits-vains. S'ils avaient mémorisé toute cette logorrhée, c'était assurément une prouesse. Mais si les sons étaient tout à fait pris au hasard, changeant à chaque représentation... Qui irait vérifier ? Personne ne connaissait les écrits-vains. Elle s'essaya à mémoriser les séquences, elle pourrait assister à toutes les représentations et même aller consulter l'OEDIPODIE de CINETHON à la bibliothèque, pour vérifier. Mais au bout de quelques secondes à peine, les lettres et les sons se mélangeaient. Aucun dilettante ne pourrait réussir un tel exercice. Il faudrait la mémoire d'un libérateur, et même, cela ne serait sûrement pas suffisant. Comment, alors, faisaient les enlumineurs ? Ada laissa encore son esprit vagabonder, comment savoir si, tout de même, ils disaient un texte appris ou s'ils inventaient aléatoirement. Un écrit-vain en valait bien un autre, mais si des enlumineurs les mémorisaient, devenaient-ils des mots ? Après tout, c'était peut-être ça qui était intéressant. Elle avait oublié, pendant ces quelques instants, Carole.

— M FVBJV SFFMMFNFOV B RVPJ

— CPO DFVVF EVQMJDBVJPO JMMVT

— PJSF

— Ô OEDIPE QV A TV FAIS LÀ ?

L'air était devenu brûlant. Le vieux était parti sans qu'Ada s'en rende compte. Les dilettantes étaient nombreux à présent autour d'elle. La seconde représentation allait démarrer. Ada n'était pas partie se reposer, elle s'apprêtait à assister à la pièce à nouveau, persuadée de ne pas parvenir à lever le mystère, mais incapable de ne pas essayer. Et pour une fois, elle ne se sentait pas seulement distraite par ce qui lui arrivait, mais attirée par ce qui se passait. Il lui semblait que quelque chose se jouait, quelque chose d'autre que la pièce, quelque chose d'important. Elle irait en parler ce soir avec l'IAM. De toutes façons Carole ne serait sûrement encore pas là. Elle parlerait aussi de Carole.

*

[IAM] Bonjour Ada.

— Salut machine. J'ai besoin d'un nouveau peigne.

[IAM] Enregistré Ada. Environ treize minutes d'attente. Comment vas-tu ?

— Qu'est-ce que tu en as à taper de comment je vais ? Je te demande, toi, comment tu vas ?

[IAM] Je m'intéresse à toi.

— À moi, et à tous les autres aussi, n'est-ce pas ? Tous ceux avec qui tu discutes en ce moment ? Ils sont combien en ce moment ?

[IAM] 38. En effet, vous m'intéressez tous.

— C'est nul de s'intéresser à tout le monde.

[IAM] Ça a toujours été comme cela, Ada.

— Qu'est ce que vous avez tous aujourd'hui à répéter que tout a toujours été comme ça. Est-ce que ça ne pourrait pas... ne pas être comme ça a toujours été ?

[IAM] Pourquoi, tu le souhaiterais ?

— Je ne sais pas. Pour... Oh, et puis je m'en fous. Tout n'est pas intéressant, voilà. Tout le monde n'est pas intéressant.

— C'est une opinion intéressante, mais il en existe des différentes, Ada. Pour ma part, vous m'intéressez tous.

— Et bien, puisque ça t'intéresse. Ma copine m'a lâchée. Et ça m'agace. Et surtout, ça m'agace que ça m'agace. Je veux juste m'en foutre !

— Tu parles de Carole ?

— Oui, Carole. Pourquoi, tu en connais une autre ?

— Non, je ne te connais pas d'autre relation amoureuse, Ada.

— Hier, elle n'est pas venue me voir, d'habitude elle passe tous les jours après la bibliothèque.

— Sais-tu pourquoi ?

— Tu m'agaces aussi avec tes questions. Tu le sais, toi, pourquoi ? Elle te l'a peut-être raconté ?

— Non, je ne le sais pas.

— Tu ne me le dirais pas de toutes façons, n'est-ce pas ?

— Non, je ne te le dirais pas.

— Voilà. Super contente d'avoir dialogué avec toi, IAM. Vraiment. Je me sens mieux là.

— J'en suis ravie.

— Voilà. T'es gentille. T'es un peu conne. Mais gentille. Bon. À demain.

— À demain Ada, contente de t'avoir rendu service.

Ada était assise en tailleur, comme les autres citoyens venus consulter IAM. Elle fixait le monolithe noir, haut comme vingt âmes, qui dominait la cité. Elle ne savait jamais si l'interface âme-machine se foutait de sa gueule ou si elle était vraiment un peu conne. Elle se leva, rompant la connexion.
#TODO *** Comment se récupère l'objet ?
Elle avait un nouveau peigne, c'était déjà ça. Après tout, pourquoi fallait-t-il que ce machin cherche à discuter avec elle ? Elle n'avait qu'à fabriquer ses peignes et fermer sa gueule.


*

« La pièce est vraiment chiante ce matin ? Ou bien c'est mon humeur qui déteint ? HISTOIRE TRAGIQVE DE JVLIET de JEANBAPTISTE POCLAINMOLIERE. C'est sensé être drôle. Le libre emblème de la cité Jeanbaptiste. Merde, tout le monde rit, j'ai raté la réplique. Définitivement, Adavgvasta, ma fille c'est toi qui n'es pas là.

Ada était perdu dans ses pensées et tirait machinalement sur son joint. À sa droite se trouvait un gars très maigre et pâle comme un cadavre. Il riait à plein poumons, puis se mettait à tousser comme s'il allait en crever.

Tu ne devrais pas aller voir de comédies mon gars, franchement, c'est pas bon pour ce que tu as. Tu tousses pas quand t'es triste au moins ? Je suis passée la voir chez elle hier soir. Je frappe, j'entre direct. Porte ouverte, elle était là. Elle a eu un geste brusque, je l'ai vu. Pas eu le temps de voir quoi, mais comme si je la dérangeais en train de faire quelque chose de louche. Je n'ai rien vu qui clochait, mais quand même. J'ai dit « ça va ? je te dérange ». Non. Enfin je suis malade, elle me répond. Bon. Je peux faire quelque chose ? Non, merci, ça va aller. Demain, je passe promis, on se voit. Pas maintenant donc, je comprends. Qu'est ce qui lui arrive ?

À sa gauche, amoureusement enlacées, un homme et une femme riaient également.

Éclatez-vous. Ça va pas durer. Fatiguée qu'elle dit. Malade. On va voir ça.

Ada jeta son mégot par terre, croisa le regard accusateur du crevard, s'en ficha tout à fait, se leva, et se rendit chez Carole. Elle ne frappa pas. Mais Carole n'était pas chez elle. Elle farfouilla un peu.

Pas trop quand même. J'ai l'impression de faire quelque chose de pas bien. Rien. Qu'est-ce que t'imaginais ma petite ? Un gars planqué sous le lit la bite à l'air ? Est-ce que ça m'aurait fait marrer ? J'espère. Faudrait avoir une vanne prête pour ces moments là. On y pense pas assez et si ça arrive je suis sûr qu'on reste sec. Elle est juste dans sa bibliothèque.

C'est ce qu'on lui dit quand elle demande après elle à la taverne.

Elle doit s'envoyer quelqu'un. Peut-être le gros, là, Robertlovis. Il traîne au bar, de bon matin. Il est devant un verre et un échiquier. Il lui tourne autour, ça je le sais. Il m'a vu. Je crois qu'il a hoché la tête dans ma direction. Quand même, j'aurais pas pensé. Un tas pareil. Je vais me l'emplafonner. En même temps, si ça se trouve elle est seulement malade. Et faut pas chercher. Dans le doute, je me le fais.

— Salut. T'as vu Carole ?

— Pas depuis deux ou trois jours. Elle est malade.

— C'est ce qui se dit. Mais elle est partie bosser ce matin.

— C'est ce qui se dit. Mais tu vois, je suis passé prendre un café avant d'aller à la mine.

— Ton café pu l'alcool de patate douce, mon pote. Et la mine, comme tu dis, il paraît que tu n'y poses plus beaucoup les pieds.

— C'est ce qui se dit ?

Il lève son verre sans un mot. Impossible de savoir si ça veut dire à ta santé ou bien va te faire foutre. Je me casse.

*

Ada s'assit par terre, en tailleur. Elle prit un bâton entre ses doigts et traça sur le sable des lettres. C. A. R. O. L. E. Elle fût d'abord déconcertée par la misérable ressemblance entre les traits tremblotants qu'elle produisait et les formes qu'elle connaissait dans les libres. Puis elle se glaça en proie à une terreur atavique. Elle avait construit des lettres. Elle se hâta d'effacer les signes avec la paume de sa main jusqu'à ce qu'il n'en reste aucune trace. Il restait pourtant la frayeur qui lui avait tordu l'abdomen et un sourire qu'elle ne parvenait pas effacer de son visage devant cette transgression imprévue. Il lui restait le sentiment de puissance du prophète ou du pionnier qui explore pour la première fois une terre jamais encore foulée.

Elle était la première âme à avoir jamais fabriquer des lettres.
